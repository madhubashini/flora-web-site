/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author HP
 */
@WebServlet(name = "Inact_products", urlPatterns = {"/Inact_products"})
public class Inact_products extends HttpServlet {

    
   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String id=request.getParameter("proID");
        System.out.println(id);
        
        Session my_session1=Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tra=my_session1.beginTransaction();
        
        POJO.Products pro=(POJO.Products)my_session1.load(POJO.Products.class,Integer.parseInt(id));
        
        POJO.DeleteStatus dels=(POJO.DeleteStatus)my_session1.load(POJO.DeleteStatus.class,Integer.parseInt("2"));
        pro.setDeleteStatus(dels);
        my_session1.update(pro);
        
        tra.commit();
        my_session1.close();
        
                
    }

   

}
