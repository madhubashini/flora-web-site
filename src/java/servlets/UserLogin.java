/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import POJO.DbCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "UserLogin", urlPatterns = {"/UserLogin"})
public class UserLogin extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String unames = request.getParameter("lognames");
        String pwd = request.getParameter("logpasss");

//        System.out.println(unames);
//        System.out.println(pwd);
        try {
            Session s = Connections.NewHibernateUtil.getSessionFactory().openSession();
            Transaction ts = s.beginTransaction();

            Criteria c = s.createCriteria(POJO.UserReg.class);
            c.add(Restrictions.eq("userName", unames));
            c.add(Restrictions.eq("password", pwd));

            List<POJO.UserReg> userg = c.list();

            POJO.UserReg ure = null;
            int i = 0;

            if (!userg.isEmpty()) {

                for (POJO.UserReg new_user : userg) {
                    ure = (POJO.UserReg) s.load(POJO.UserReg.class, new_user.getId());
                    i = ure.getId();

                }

                POJO.UserStatus stss = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("1"));
                POJO.UserType utype_1=(POJO.UserType)s.load(POJO.UserType.class,Integer.parseInt("1"));
                POJO.UserType utype_2=(POJO.UserType)s.load(POJO.UserType.class,Integer.parseInt("2"));
                POJO.UserType utype_3=(POJO.UserType)s.load(POJO.UserType.class,Integer.parseInt("3"));

                if (ure != null && ure.getUserStatus().equals(stss) && ure.getUserType().equals(utype_1)) {

                    request.getSession().setAttribute("login", ure);
                    HttpSession htses = request.getSession();
                    //
                    if (htses.getAttribute("shopping_cart") != null) {
                        Keep_cart kpc = (Keep_cart) request.getSession().getAttribute("shopping_cart");
                        List<Added_cart_items> adc = kpc.returnArray();
                        for (Added_cart_items a : adc) {

                            Criteria cr = s.createCriteria(POJO.Products.class);
                            cr.add(Restrictions.eq("id", a.getCart_product_id()));
                            POJO.Products pr = (POJO.Products) cr.uniqueResult();

                            Criteria cr3 = s.createCriteria(POJO.DbCart.class);
                            cr3.add(Restrictions.eq("userReg", ure));
                            cr3.add(Restrictions.eq("products", pr));
                            List<POJO.DbCart> dbliste = cr3.list();

                            POJO.DbCart dcart = null;
                            if (!dbliste.isEmpty()) {
                                    dcart =(POJO.DbCart)cr3.uniqueResult();
                            }

                            if (dcart != null) {
                                dcart.setQty(a.getCart_product_qty() + "");
                                s.update(dcart);
                            } else {
                                POJO.DbCart dca = new POJO.DbCart(pr, ure, a.getCart_product_qty() + "");
                                s.save(dca);
                            }

                        }
                        htses.removeAttribute("shopping_cart");

                        Criteria cr = s.createCriteria(POJO.DbCart.class);
                        cr.add(Restrictions.eq("userReg", ure));
                        List<POJO.DbCart> dbclist = cr.list();
                        HttpSession myse = request.getSession();
                        Keep_cart kp = null;

                        for (POJO.DbCart d : dbclist) {
                            Added_cart_items ad = new Added_cart_items();

                            ad.setCart_product_id(d.getProducts().getId());
                            ad.setCart_product_qty(Integer.parseInt(d.getQty()));

                            if (myse.getAttribute("shopping_cart") == null) {
                                kp = new Keep_cart();
                            } else {
                                kp = (Keep_cart) myse.getAttribute("shopping_cart");
                            }

                            kp.addCart(ad);

                            myse.setAttribute("shopping_cart", kp);
                        }

                    } else {

                        Criteria cr = s.createCriteria(POJO.DbCart.class);
                        cr.add(Restrictions.eq("userReg", ure));
                        List<POJO.DbCart> dbclist = cr.list();
                        HttpSession myse = request.getSession();
                        Keep_cart kp = null;

                        for (POJO.DbCart d : dbclist) {
                            Added_cart_items ad = new Added_cart_items();

                            ad.setCart_product_id(d.getProducts().getId());
                            ad.setCart_product_qty(Integer.parseInt(d.getQty()));

                            if (myse.getAttribute("shopping_cart") == null) {
                                kp = new Keep_cart();
                            } else {
                                kp = (Keep_cart) myse.getAttribute("shopping_cart");
                            }
                            kp.addCart(ad);
                            myse.setAttribute("shopping_cart", kp);

                        }

                    }

                    //
//                   
                    //

                    out.print("1");
                }
                
                if (ure != null && ure.getUserStatus().equals(stss) && (ure.getUserType().equals(utype_2)|| ure.getUserType().equals(utype_3))) {
                    request.getSession().setAttribute("login", ure);
                    out.print("99");
                }

                POJO.UserStatus stus_2 = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("2"));
                if (ure != null && ure.getUserStatus().equals(stus_2)) {
                    out.print("2");
                }
                POJO.UserStatus sts = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("3"));
                if (ure != null && ure.getUserStatus().equals(sts)) {
                    out.print("3");
                }
                POJO.UserStatus st = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("4"));
                if (ure != null && ure.getUserStatus().equals(st)) {

                    POJO.UserReg um = (POJO.UserReg) s.load(POJO.UserReg.class, i);
                    um.setUserStatus(stss);
                    s.update(um);
                    request.getSession().setAttribute("login", ure);
                    out.print("4");
                }

            } else {

                out.print("00");

            }

            ts.commit();

            s.close();
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
