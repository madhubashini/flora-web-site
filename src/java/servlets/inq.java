/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "inq", urlPatterns = {"/inq"})
public class inq extends HttpServlet {


   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        PrintWriter out=response.getWriter();
        
        String id=request.getParameter("id");
        System.out.println(id);
        
        Session  s= Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction t=s.beginTransaction();
        
        Criteria c=s.createCriteria(POJO.Inqu.class);
        c.add(Restrictions.eq("id", Integer.parseInt(id)));
        POJO.Inqu i=(POJO.Inqu)c.uniqueResult();
        if(i!=null){
            System.out.println("kkkk");
            i.setStat("2");
            s.update(i);
            
            t.commit();
        }
        
    }

}
