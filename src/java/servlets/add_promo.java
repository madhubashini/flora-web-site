/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author HP
 */
@WebServlet(name = "add_promo", urlPatterns = {"/add_promo"})
public class add_promo extends HttpServlet {


    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String add=request.getParameter("ad");
        String end=request.getParameter("en");
        String per=request.getParameter("pe");
    
        Session se=Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=se.beginTransaction();
        
        POJO.DeleteStatus de=(POJO.DeleteStatus)se.load(POJO.DeleteStatus.class,Integer.parseInt("1"));
        
        try {
            
            POJO.Promotionn p=new POJO.Promotionn(de, add, end, per, null);
            se.save(p);
            
            tr.commit();
            
        } catch (Exception e) {
        }
        
        
    }


}
