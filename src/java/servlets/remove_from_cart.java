/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "remove_from_cart", urlPatterns = {"/remove_from_cart"})
public class remove_from_cart extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String id = request.getParameter("id");
        System.out.println(id);

        POJO.UserReg user = (POJO.UserReg) request.getSession().getAttribute("login");

        if (user == null) {

            System.out.println("lognot");
            Added_cart_items p = new Added_cart_items();
            p.setCart_product_id(Integer.parseInt(id));

            HttpSession hs = request.getSession();
            Keep_cart c = (Keep_cart) hs.getAttribute("shopping_cart");

            c.removepro(p);

            hs.setAttribute("shopping_cart", c);

        } else {

            System.out.println("log");

            Session sess = Connections.NewHibernateUtil.getSessionFactory().openSession();
            Transaction trs = sess.beginTransaction();

            POJO.UserReg urs = (POJO.UserReg) sess.load(POJO.UserReg.class, user.getId());
            POJO.Products prm = (POJO.Products) sess.load(POJO.Products.class, Integer.parseInt(id));

            Criteria cr3 = sess.createCriteria(POJO.DbCart.class);
            cr3.add(Restrictions.eq("userReg", urs));
            cr3.add(Restrictions.eq("products", prm));
            //POJO.DbCart dc = (POJO.DbCart) cr3.uniqueResult();
            List<POJO.DbCart> dc = cr3.list();
            // System.out.println(dc.size());

            if (!dc.isEmpty()) {
                int item = 0;
                for (POJO.DbCart getdb : dc) {
                    item = getdb.getId();
                }
                POJO.DbCart mydb = (POJO.DbCart) sess.load(POJO.DbCart.class, item);

                sess.delete(mydb);
                System.out.println("aaaaa");
            } else {
                System.out.println("vvvvvvv");
            }

            trs.commit();

            Criteria cr = sess.createCriteria(POJO.DbCart.class);
            cr.add(Restrictions.eq("userReg", urs));
            List<POJO.DbCart> dba = cr.list();

            HttpSession myn = request.getSession();
            Keep_cart kt = null;

            myn.removeAttribute("shopping_cart");

            for (POJO.DbCart da : dba) {
                Added_cart_items adn = new Added_cart_items();
                adn.setCart_product_id(da.getProducts().getId());
                adn.setCart_product_qty(Integer.parseInt(da.getQty()));

                if (myn.getAttribute("shopping_cart") == null) {
                    kt = new Keep_cart();
                } else {
                    kt = (Keep_cart) myn.getAttribute("shopping_cart");
                }

                kt.addCart(adn);

                HttpSession myd = request.getSession();
                myd.setAttribute("shopping_cart", kt);

            }
        }

    }
}
