/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "cart_qty_decre", urlPatterns = {"/cart_qty_decre"})
public class cart_qty_decre extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String id = request.getParameter("id");
        // System.out.println("print dicrease item id=" + id);

        HttpSession ses = request.getSession();

        Keep_cart kc = (Keep_cart) ses.getAttribute("shopping_cart");

        List<Added_cart_items> ac = kc.returnArray();

        Added_cart_items adc1 = new Added_cart_items();
        for (Added_cart_items ad2 : ac) {

            if (ad2.getCart_product_id() == Integer.parseInt(id)) {

                if (ad2.getCart_product_qty() - 1 >= 1) {

                    //System.out.println("ifeka athulen gannawa"+ad2.getCart_product_id());
                    adc1.setCart_product_id(ad2.getCart_product_id());
                    adc1.setCart_product_qty(1);

                    Keep_cart kpc = (Keep_cart) ses.getAttribute("shopping_cart");

                    kpc.decrementCart(adc1);
                    ses.setAttribute("shopping_cart", kpc);

                    break;

                }

            }

        }

        int am = 0;
        if (request.getSession().getAttribute("login") != null) {

            ses.removeAttribute("shopping_cart");

            System.out.println("aaaaa");

            HttpSession sesi = request.getSession();

            Session sei = Connections.NewHibernateUtil.getSessionFactory().openSession();
            Transaction tr = sei.beginTransaction();

            POJO.UserReg u = (POJO.UserReg) request.getSession().getAttribute("login");
            POJO.UserReg ur = (POJO.UserReg) sei.load(POJO.UserReg.class, u.getId());

            Criteria cr = sei.createCriteria(POJO.Products.class);
            cr.add(Restrictions.eq("id", Integer.parseInt(id)));
            POJO.Products pr = (POJO.Products) cr.uniqueResult();

            System.out.println(" product Ideka" + pr.getId());

            Criteria cr3 = sei.createCriteria(POJO.DbCart.class);
            cr3.add(Restrictions.eq("userReg", ur));
            cr3.add(Restrictions.eq("products", pr));
            // POJO.DbCart dcart = (POJO.DbCart) cr3.uniqueResult();

            List<POJO.DbCart> my_db = cr3.list();

            if (!my_db.isEmpty()) {

                System.out.println("tiyanawa DB cart eke_decre");

                String maf = null;
                for (POJO.DbCart getd : my_db) {
                    maf = getd.getId() + "";
                }
                System.out.println(maf + "Db cart eke id eka");

                POJO.DbCart pojo_db = (POJO.DbCart) sei.load(POJO.DbCart.class, Integer.parseInt(maf));

                int ints = Integer.parseInt(pojo_db.getQty());
                am = ints - 1;

                if (ints - 1 >= 1) {
                    pojo_db.setQty(am + "");
                    System.out.println("tiyanawa Db cart eke 1ta wadie" + am);
                    sei.update(pojo_db);

                }
                if (ints - 1 < 1) {
                    pojo_db.setQty("1");
                    System.out.println("tiyanawa Db cart eke 1 ta adue" + am);
                    sei.update(pojo_db);
                }

            } else {
                System.out.println("naaaaa Db cart eke_decre");
            }
            tr.commit();

            // DB cart eka set karala session ekata set karanawa
            Criteria crs = sei.createCriteria(POJO.DbCart.class);
            crs.add(Restrictions.eq("userReg", ur));
            List<POJO.DbCart> db = crs.list();

            HttpSession my = request.getSession();
            Keep_cart k = null;

            for (POJO.DbCart dd : db) {

                Added_cart_items add = new Added_cart_items();

                add.setCart_product_id(dd.getProducts().getId());
                add.setCart_product_qty(Integer.parseInt(dd.getQty()));

                if (my.getAttribute("shopping_cart") == null) {
                    k = new Keep_cart();
                } else {
                    k = (Keep_cart) my.getAttribute("shopping_cart");
                }

                k.addCart(add);
                my.setAttribute("shopping_cart", k);

            }

        }

        double subtotal = 0.0;
        double total = 0.00;
        double discout = 0.00;
        double subtot = 0.00;
        int quen = 0;

        Keep_cart kpc = (Keep_cart) request.getSession().getAttribute("shopping_cart");
        Session adsession = Connections.NewHibernateUtil.getSessionFactory().openSession();

        if (kpc != null) {
            List<Added_cart_items> adc = kpc.returnArray();
            for (Added_cart_items add : adc) {
//
                if (add.getCart_product_id() == Integer.parseInt(id)) {
                    //quen = add.getCart_product_qty();
                    quen = add.getCart_product_qty();

                    Criteria crtmt = adsession.createCriteria(POJO.Products.class);

                    crtmt.add(Restrictions.eq("id", add.getCart_product_id()));

                    POJO.Products pj = (POJO.Products) crtmt.uniqueResult();

                    double num = Double.parseDouble(pj.getUnitPrice());
                                            //

                    //double ii = Double.parseDouble(pj.getUnitPrice());
                    total = (double) add.getCart_product_qty() * Double.parseDouble(pj.getUnitPrice());

                    String ds = pj.getPromotionn().getDeleteStatus().getStatus();

                    // if (add.getCart_product_id() == Integer.parseInt(id)) {
                    if (ds.equals("Active")) {
                        // out.print("if");
                        discout = Double.parseDouble(pj.getPromotionn().getDiscountPersentage()) * total / Double.parseDouble("100");
                        //out.print(discout+"discntEka");
                        subtot = total - discout;
                    } else {
                        //out.print("else");
                        subtot = total;
                    }
                }
//

                //   }
            }

            double subto = 0.00;
            double tota = 0.00;
            double discou = 0.00;

            for (Added_cart_items addc : adc) {

                Criteria crtm = adsession.createCriteria(POJO.Products.class);

                crtm.add(Restrictions.eq("id", addc.getCart_product_id()));

                POJO.Products pj = (POJO.Products) crtm.uniqueResult();

                tota = (double) addc.getCart_product_qty() * Double.parseDouble(pj.getUnitPrice());

                String ds = pj.getPromotionn().getDeleteStatus().getStatus();

                // if (add.getCart_product_id() == Integer.parseInt(id)) {
                if (ds.equals("Active")) {
                    // out.print("if");
                    discou = Double.parseDouble(pj.getPromotionn().getDiscountPersentage()) * tota / Double.parseDouble("100");
                    //out.print(discout+"discntEka");
                    subto = tota - discou;
                } else {
                    //out.print("else");
                    subto = tota;
                }

                subtotal += subto;

            }

        }
        DecimalFormat de = new DecimalFormat("0.00");
        System.out.println("quentity eka decre_qty eke=" + quen);
        out.print("ptot," + de.format(subtot) + ",subst," + de.format(subtotal) + ",quen," + quen);

    }

}
