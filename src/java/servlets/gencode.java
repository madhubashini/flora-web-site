/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;

import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "gencode", urlPatterns = {"/gencode"})
public class gencode extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        
        String una = request.getParameter("un");
        String codes = request.getParameter("code");
        
        System.out.println(una);
        System.out.println(codes);
        
        Session s = Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = s.beginTransaction();
        try {

            POJO.UserStatus ust=(POJO.UserStatus)s.load(POJO.UserStatus.class,Integer.parseInt("3"));
            Criteria c = s.createCriteria(POJO.UserReg.class);

            c.add(Restrictions.eq("userName", una));
            c.add(Restrictions.eq("codeGen", codes));
            c.add(Restrictions.eq("userStatus", ust));
            
            List<POJO.UserReg> u_list=c.list();
            POJO.UserReg ure =null;
            if(!u_list.isEmpty()){
                 ure = (POJO.UserReg) c.uniqueResult();
            }

            

            if (ure != null) {
                POJO.UserStatus uts = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("1"));
                ure.setUserStatus(uts);
                s.update(ure);
                tr.commit();
                System.out.println("1");
                
                HttpSession htpses=request.getSession();
                htpses.setAttribute("login", ure);
                
                out.print("1");
            } else {
                System.out.println("2");
                out.print("2");

            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

}
