/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "account_deact", urlPatterns = {"/account_deact"})
public class account_deact extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String code = request.getParameter("c");
        String name = request.getParameter("n");
        String pass = request.getParameter("p");

        // System.out.println(code+""+name+""+pass);
        try {
            Session sesm = Connections.NewHibernateUtil.getSessionFactory().openSession();
            Transaction trs = sesm.beginTransaction();

            Criteria crs = sesm.createCriteria(POJO.UserReg.class);
            crs.add(Restrictions.eq("codeGen", code));
            crs.add(Restrictions.eq("userName", name));
            crs.add(Restrictions.eq("password", pass));

            POJO.UserReg red = null;
            List<POJO.UserReg> liste = crs.list();
            
            if (!liste.isEmpty()) {
                red = (POJO.UserReg) crs.uniqueResult();
            }

            if (red != null) {
                POJO.UserReg reg = (POJO.UserReg) sesm.load(POJO.UserReg.class, red.getId());
                POJO.UserStatus ust = (POJO.UserStatus) sesm.load(POJO.UserStatus.class, Integer.parseInt("4"));
                reg.setUserStatus(ust);
                sesm.update(reg);
                trs.commit();
                request.getSession().invalidate();
                out.print("1");
            } else {
                out.print("2");
            }
        } catch (Exception e) {
        }

    }

}
