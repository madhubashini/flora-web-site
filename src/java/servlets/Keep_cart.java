/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HP
 */
public class Keep_cart {

    private List<Added_cart_items> productArray;

    public Keep_cart() {
        productArray = new ArrayList<Added_cart_items>();
    }

    public List<Added_cart_items> returnArray() {
        return productArray;
    }

    public void addCart(Added_cart_items ac) {

        int qty = 0;
        for (int i = 0; i < productArray.size(); i++) {
            Added_cart_items old = productArray.get(i);

            if (old.getCart_product_id() == ac.getCart_product_id()) {
                qty = old.getCart_product_qty() + ac.getCart_product_qty();
                productArray.remove(old);
                ac.setCart_product_qty(qty);
            }
        }
        productArray.add(ac);
    }

    public void decrementCart(Added_cart_items ac) {

        int qty = 0;
        for (int i = 0; i < productArray.size(); i++) {
            Added_cart_items old = productArray.get(i);

            if (old.getCart_product_id() == ac.getCart_product_id()) {
                qty = old.getCart_product_qty() - ac.getCart_product_qty();
                productArray.remove(old);
                ac.setCart_product_qty(qty);
            }
        }
        productArray.add(ac);
    }

    public void removeFromCart(Added_cart_items aci) {

        for (int i = 0; i < productArray.size(); i++) {
            Added_cart_items aac = productArray.get(i);
            if (aac.getCart_product_id() == aci.getCart_product_qty()) {
                productArray.remove(aac);

            }

        }

    }

    public void removepro(Added_cart_items nc) {
        for (int i = 0; i < productArray.size(); i++) {
            Added_cart_items elem = productArray.get(i);
            if (elem.getCart_product_id() == nc.getCart_product_id()) {
                productArray.remove(elem);
            }
        }
    }

}
