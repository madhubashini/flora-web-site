/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

/**
 *
 * @author HP
 */
public class Added_cart_items {
    
    private int cart_product_id;
    private int cart_product_qty;

    /**
     * @return the cart_product_id
     */
    public int getCart_product_id() {
        return cart_product_id;
    }

    /**
     * @param cart_product_id the cart_product_id to set
     */
    public void setCart_product_id(int cart_product_id) {
        this.cart_product_id = cart_product_id;
    }

    /**
     * @return the cart_product_qty
     */
    public int getCart_product_qty() {
        return cart_product_qty;
    }

    /**
     * @param cart_product_qty the cart_product_qty to set
     */
    public void setCart_product_qty(int cart_product_qty) {
        this.cart_product_qty = cart_product_qty;
    }
    
}
