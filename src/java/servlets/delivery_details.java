/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author HP
 */
@WebServlet(name = "delivery_details", urlPatterns = {"/delivery_details"})
public class delivery_details extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        HttpSession sess = request.getSession();

        if (request.getSession().getAttribute("deliveryD") != null) {

            sess.removeAttribute("deliveryD");
        }
        String userId = request.getParameter("user");
        String r_name = request.getParameter("rec_name");
        String r_contact = request.getParameter("rec_phone");
        String r_ad1 = request.getParameter("rec_address1");
        String r_ad2 = request.getParameter("rec_address2");
        String city = request.getParameter("city");
        String date = request.getParameter("date");
        String time = request.getParameter("time");
        String message = request.getParameter("message");
        String istruction = request.getParameter("instru");
        String s_name = request.getParameter("send_name");
        String s_email = request.getParameter("send_email");
        String s_contact = request.getParameter("send_phone");
//        String gft_item_price = request.getParameter("hittenfield");

        String send_title = request.getParameter("send_title");
        String rec_title = request.getParameter("rec_title");
        String location = request.getParameter("location");

//        System.out.println(userId);
//        System.out.println(r_name);
//        System.out.println(r_contact);
//        System.out.println(r_ad1);
//        System.out.println(r_ad2);
//        System.out.println(city);
//        System.out.println(date);
//        System.out.println(time);
//        System.out.println(message);
//        System.out.println(istruction);
//        System.out.println(s_name);
//        System.out.println(s_email);
//        System.out.println(s_contact);
//        System.out.println(gft_item_price);
//        System.out.println(send_title);
//        System.out.println(rec_title);
//        System.out.println(location);
        List l = new ArrayList();
        l.add(userId);
        l.add(r_name);
        l.add(r_contact);
        l.add(r_ad1);
        l.add(r_ad2);
        l.add(city);
        l.add(date);
        l.add(time);
        l.add(message);
        l.add(istruction);
        l.add(s_name);
        l.add(s_email);
        l.add(s_contact);
//        l.add(gft_item_price);
        l.add(send_title);
        l.add(rec_title);
        l.add(location);

        sess.setAttribute("deliveryD", l);

        ArrayList a = (ArrayList) sess.getAttribute("deliveryD");
        //System.out.println(a.get(0)+"id eka");

        out.print("1");
    }

}
