/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author HP
 */
@WebServlet(name = "pro_search_to_save", urlPatterns = {"/pro_search_to_save"})
public class pro_search_to_save extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        String id = "";
        String names = "";
        String wgt = "";
        String item = "";
        String untpr = "";
        String cat = "";
        String descs = "";
        String prmm = "";
        
        try {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List<FileItem> items = upload.parseRequest(request);
            
            for (Object element : items) {
                FileItem fileItem = (FileItem) element;
                if (fileItem.isFormField()) {
                    
                    if (fileItem.getFieldName().equals("id")) {
                        id = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("name")) {
                        names = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("weight")) {
                        wgt = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("item")) {
                        item = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("untp")) {
                        untpr = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("catg")) {
                        cat = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("desc")) {
                        descs = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("promo")) {
                        prmm = fileItem.getString();
                    }
                    
                } 
            }
            
            
            Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
            Transaction tr = ses.beginTransaction();
            
            POJO.SubCatogory sub = (POJO.SubCatogory) ses.load(POJO.SubCatogory.class, Integer.parseInt(cat));
            POJO.Promotionn prm = (POJO.Promotionn) ses.load(POJO.Promotionn.class, Integer.parseInt(prmm));
            POJO.Products prod=(POJO.Products)ses.load(POJO.Products.class,Integer.parseInt(id));
            
            prod.setName(names);
            prod.setWeight(wgt);
            prod.setItems(item);
            prod.setUnitPrice(untpr);
            prod.setSubCatogory(sub);
            prod.setDescription(descs);
            prod.setPromotionn(prm);
            
            ses.update(prod);
            
            
            tr.commit();
            ses.close(); 
            
            System.out.print("save changed");
            
            response.sendRedirect("admin_edit_product.jsp");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
}
