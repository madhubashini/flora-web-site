/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "change_bill", urlPatterns = {"/change_bill"})
public class change_bill extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String code = request.getParameter("code");
        String tit = request.getParameter("tit");
        String name = request.getParameter("name");
        String ema = request.getParameter("ema");
        String phn = request.getParameter("ph");
        
        //System.out.println(code+tit+name+ema+phn);
        
        Session sesn = Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = sesn.beginTransaction();

        try {

            Criteria crt = sesn.createCriteria(POJO.UserReg.class);
            crt.add(Restrictions.eq("codeGen", code));
            POJO.UserReg urg = (POJO.UserReg) crt.uniqueResult();
            
            Criteria cr = sesn.createCriteria(POJO.DefaultBilling.class);
            cr.add(Restrictions.eq("userReg", urg));
            
            List<POJO.DefaultBilling> myl=cr.list();
            POJO.DefaultBilling dd=null;
            
            if(!myl.isEmpty()){
                dd = (POJO.DefaultBilling) cr.uniqueResult(); 
            }
            
            
            
            if(dd==null){
                
                POJO.DefaultBilling db=new POJO.DefaultBilling(urg, tit, name, ema, phn);
                sesn.save(db);
                
            }else{
                
               dd.setTitle(tit);
               dd.setName(name);
               dd.setEmail(ema);
               dd.setPhoneNum(phn);
               sesn.update(dd);
            
            }
            
            
            tr.commit();
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
