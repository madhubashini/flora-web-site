/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "delete_weekly", urlPatterns = {"/delete_weekly"})
public class delete_weekly extends HttpServlet {

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       PrintWriter out=response.getWriter();
       
       String id=request.getParameter("id");
       
        Session ses=Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=ses.beginTransaction();
        
        try {
            
            Criteria c=ses.createCriteria(POJO.Weekly.class);
            c.add(Restrictions.eq("id",Integer.parseInt(id)));
            POJO.Weekly we=(POJO.Weekly)c.uniqueResult();
            
            POJO.DeleteStatus de=(POJO.DeleteStatus)ses.load(POJO.DeleteStatus.class,Integer.parseInt("2"));
            
            we.setDeleteStatus(de);
            ses.update(we);
            tr.commit();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
       
        
    }

   

}
