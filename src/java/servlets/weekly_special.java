/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "weekly_special", urlPatterns = {"/weekly_special"})
public class weekly_special extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String pro = request.getParameter("p");
        String add = request.getParameter("a");
        String end = request.getParameter("e");

        System.out.println(pro + "" + end + "" + add);

        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = ses.beginTransaction();

        POJO.DeleteStatus d = (POJO.DeleteStatus) ses.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
        POJO.Promotionn promo = (POJO.Promotionn) ses.load(POJO.Promotionn.class, Integer.parseInt("1"));

        Criteria crt = ses.createCriteria(POJO.Products.class);
        crt.add(Restrictions.eq("id",Integer.parseInt(pro)));
        crt.add(Restrictions.ne("promotionn", promo));
        crt.add(Restrictions.eq("deleteStatus", d));

        List lists = crt.list();

        try {

            if (!lists.isEmpty()) {
                POJO.Products pr = (POJO.Products) crt.uniqueResult();
                POJO.Weekly we = new POJO.Weekly(d, pr, add, end);
                ses.save(we);
                out.print("1");
            } else {
                out.print("2");
            }

            tr.commit();

        } catch (Exception e) {
        }

    }

}
