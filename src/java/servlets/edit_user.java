/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "edit_user", urlPatterns = {"/edit_user"})
public class edit_user extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String id = request.getParameter("uid");
        String stat = request.getParameter("status");
        String typ = request.getParameter("type");

        System.out.println(id + "" + stat + "" + typ);

        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
        
        Transaction tr = ses.beginTransaction();

        Criteria crs = ses.createCriteria(POJO.UserReg.class);
        crs.add(Restrictions.eq("id", Integer.parseInt(id)));
        POJO.UserReg urg = (POJO.UserReg) crs.uniqueResult();

        POJO.UserStatus statu = (POJO.UserStatus) ses.load(POJO.UserStatus.class, Integer.parseInt(stat));
        POJO.UserType type = (POJO.UserType) ses.load(POJO.UserType.class, Integer.parseInt(typ));

        urg.setUserStatus(statu);
        urg.setUserType(type);

        ses.update(urg);
        tr.commit();

        out.print("1");

    }

}
