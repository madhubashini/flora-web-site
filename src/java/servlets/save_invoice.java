/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "save_invoice", urlPatterns = {"/save_invoice"})
public class save_invoice extends HttpServlet {
    private String host;
    private String port;
    private String users;
    private String pass;

    @Override
    public void init() {

        // reads SMTP server setting from web.xml file
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        users = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        
        String subtot = request.getParameter("tot");
        ArrayList a = (ArrayList) request.getSession().getAttribute("deliveryD");

        out.print(a.get(0));

        String userId = a.get(0).toString();
        String r_name = a.get(1).toString();
        String r_contact = a.get(2).toString();
        String r_ad1 = a.get(3).toString();
        String r_ad2 = a.get(4).toString();
        String city = a.get(5).toString();
        String date = a.get(6).toString();
        String time = a.get(7).toString();
        String message = a.get(8).toString();
        String istruction = a.get(9).toString();
        String s_name = a.get(10).toString();
        String s_email = a.get(11).toString();
        String s_contact = a.get(12).toString();
        String send_title = a.get(13).toString();
        String rec_title = a.get(14).toString();
        String location = a.get(15).toString();

        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction transac = ses.beginTransaction();

        try {

            
            POJO.UserReg user = (POJO.UserReg) request.getSession().getAttribute("login");
            System.out.println(user.getId()+"Userge Id eka print");

            POJO.SendersInfo sender = new POJO.SendersInfo(user, send_title, s_name, s_email, s_contact, null);
            ses.save(sender);
            

            Criteria crtt = ses.createCriteria(POJO.DeliveryCity.class);
            crtt.add(Restrictions.eq("city", city));
            POJO.DeliveryCity delcity = (POJO.DeliveryCity) crtt.uniqueResult();
            POJO.DeliveryCity dc = (POJO.DeliveryCity) ses.load(POJO.DeliveryCity.class, delcity.getId());

            double total = Double.parseDouble(subtot);
            // String tot = Double.toString(total);
            DecimalFormat d = new DecimalFormat("0.00");
            String tott = d.format(total);

            POJO.RecipientInfo receip = new POJO.RecipientInfo(dc, user, rec_title, r_name, r_ad1, r_ad2, r_contact, location, null);
            ses.save(receip);

            POJO.DeliveryInfo deliveryInf = new POJO.DeliveryInfo(receip, sender, istruction, message, time, date, null);
            ses.save(deliveryInf);

            POJO.DeliveryStatus delstus = (POJO.DeliveryStatus) ses.load(POJO.DeliveryStatus.class, Integer.parseInt("1"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
            Date dd = new Date();
            String dat = sdf.format(dd);

            POJO.Invoice inv = new POJO.Invoice(deliveryInf, delstus, user, tott, dat, null,null);
            ses.save(inv);

            Keep_cart kpc = (Keep_cart) request.getSession().getAttribute("shopping_cart");
            if (kpc != null) {
                List<Added_cart_items> adc = kpc.returnArray();
                for (Added_cart_items add : adc) {

                    POJO.Products pro = (POJO.Products) ses.load(POJO.Products.class, add.getCart_product_id());

                    Criteria crtmt = ses.createCriteria(POJO.DbCart.class);
                    crtmt.add(Restrictions.eq("products", pro));
                    crtmt.add(Restrictions.eq("userReg", user));
                    POJO.DbCart pj = (POJO.DbCart) crtmt.uniqueResult();

                    POJO.PayedItems payed = new POJO.PayedItems(inv, pro, add.getCart_product_qty() + "");
                    ses.save(payed);

                    ses.delete(pj);

                }
            }

            transac.commit();
            
            HttpSession http = request.getSession();
            http.setAttribute("Invoice", inv);

            http.removeAttribute("shopping_cart");
            http.removeAttribute("deliveryD");
            
            String subject = "Madhu Flora";
            String mail=user.getEMail();
            String codes="menna me badu tika gaththa oya";
            
            try {
                    EmailUtil.sendEmail(host, port, users, pass, mail, subject, codes);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
