/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "Register", urlPatterns = {"/Register"})
public class Register extends HttpServlet {

    private String host;
    private String port;
    private String user;
    private String pass;

    @Override
    public void init() {

        // reads SMTP server setting from web.xml file
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
        
        context.getInitParameter("user");
        context.setInitParameter("user", "a.i@gmail.com");
    }
    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String mail = request.getParameter("mail");
        String ps1 = request.getParameter("passw");
        String name = request.getParameter("fnames");
        String lname = request.getParameter("lnames");
        String username = request.getParameter("un");

        System.out.println(username);
        System.out.println(mail);
        System.out.println(ps1);
        System.out.println(name);
        System.out.println(lname + "lastname eka print");

        // generated code eka 
        SecureRandom random = new SecureRandom();
        String gc = new BigInteger(130, random).toString(32);
        System.out.println(gc);

        //String stts = "Pending";
        String resultMessage = "";
        String subject = "Madhu Flora";

        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr;

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
        Date d = new Date();
        String d1 = sdf.format(d);

        try {

            tr = ses.beginTransaction();

            String msg = "http://localhost:8080/Madhu_Flora_New/login_confirm.jsp?code=" + gc;
            String codes = "Your Verification code is" + " " + gc + " " + msg + " " + "Click Here";

            Criteria c = ses.createCriteria(POJO.UserReg.class);
            c.add(Restrictions.eq("EMail", mail));
            POJO.UserReg ure = (POJO.UserReg) c.uniqueResult();

            Criteria c2 = ses.createCriteria(POJO.UserReg.class);
            c2.add(Restrictions.eq("userName", username));
            POJO.UserReg una = (POJO.UserReg) c2.uniqueResult();

            

            if (ure == null && una == null) {

                POJO.UserType ut = (POJO.UserType) ses.load(POJO.UserType.class, Integer.parseInt("1"));
                POJO.UserStatus userstat = (POJO.UserStatus) ses.load(POJO.UserStatus.class, Integer.parseInt("3"));

                POJO.UserReg ur = new POJO.UserReg(userstat, ut, username, mail, ps1, name, lname, gc, d1, null, null, null, null, null, null, null,null);
                ses.save(ur);
                tr.commit();
                ses.close();

                System.out.println("seccusfully saved");

                //email send code eka
                try {

                    EmailUtil.sendEmail(host, port, user, pass, mail, subject, codes);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                out.print("2");
            }
            if (una != null && ure == null) {
                out.print("1");
            }
            if (una == null && ure != null) {
                out.print("3");
            }
            if (una != null && ure != null) {
                out.print("4");
            }
            

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
