/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "change_ship_address", urlPatterns = {"/change_ship_address"})
public class change_ship_address extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String tit = request.getParameter("ti");
        String nam = request.getParameter("na");
        String ad = request.getParameter("a1");
        String ad2 = request.getParameter("a2");
        String ct = request.getParameter("ci");
        String phn = request.getParameter("p");
        String loc = request.getParameter("lo");
        String user = request.getParameter("use");
         System.out.println(tit+""+nam+""+ad+""+ad2+""+ct+""+phn+""+loc+"code="+user);

        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = ses.beginTransaction();

        try {
            Criteria crt = ses.createCriteria(POJO.UserReg.class);
            crt.add(Restrictions.eq("codeGen", user));
            POJO.UserReg urg = (POJO.UserReg) crt.uniqueResult();

            Criteria cr = ses.createCriteria(POJO.DefaultShipping.class);
            cr.add(Restrictions.eq("userReg", urg));
            
            List<POJO.DefaultShipping> li=cr.list();
            
            POJO.DefaultShipping d=null;
            
            if(!li.isEmpty()){
                d=(POJO.DefaultShipping) cr.uniqueResult();
            }
            
            Criteria c = ses.createCriteria(POJO.DeliveryCity.class);
            c.add(Restrictions.eq("city", ct));
            POJO.DeliveryCity dc = (POJO.DeliveryCity) c.uniqueResult();

            if (d==null) {

                POJO.DefaultShipping de = new POJO.DefaultShipping(dc, urg, tit, nam, ad, ad2, phn, loc);
                ses.save(de);
            } else {

                d.setTitle(tit);
                d.setName(nam);
                d.setAddress(ad);
                d.setAddress2(ad2);
                d.setDeliveryCity(dc);
                d.setPhoneNum(phn);
                d.setLocationType(loc);

                ses.update(d);

            }

            tr.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
