/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "active_product", urlPatterns = {"/active_product"})
public class active_product extends HttpServlet {

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String id=request.getParameter("id");
        //System.out.println(id);
        
        Session session=Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=session.beginTransaction();
        
        try {
            
            Criteria crt=session.createCriteria(POJO.Products.class);
            crt.add(Restrictions.eq("id",Integer.parseInt(id)));
            POJO.Products pro=(POJO.Products)crt.uniqueResult();
            
            POJO.DeleteStatus st=(POJO.DeleteStatus)session.load(POJO.DeleteStatus.class,Integer.parseInt("1"));
            
            pro.setDeleteStatus(st);
            session.update(pro);
            tr.commit();
            
        } catch (Exception e) {
        }
        
        
    }

    

}
