/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author HP
 */
@WebServlet(name = "searchbykey_over5", urlPatterns = {"/searchbykey_over5"})
public class searchbykey_over5 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();

        String keyword = request.getParameter("keyword");

        if (keyword != null) {
            Criteria cr1 = ses.createCriteria(POJO.Products.class);

            cr1.add(Restrictions.like("name", "%" + keyword + "%"));

            List<POJO.Products> plist = cr1.list();

            try {
                JSONObject json = new JSONObject();

                for (POJO.Products pro : plist) {
                    json.put(pro.getId() + "", pro.getName());
                }

                out.print(json);

                //System.out.println(json);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }

            ses.close();

        }

    }
}
