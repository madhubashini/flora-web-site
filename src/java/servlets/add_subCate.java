/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import POJO.SubCatogory;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author HP
 */
@WebServlet(name = "add_subCate", urlPatterns = {"/add_subCate"})
public class add_subCate extends HttpServlet {

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name=request.getParameter("name");
        String catid=request.getParameter("cat");
        //System.out.println(catid+""+name);
    
        Session se=Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=se.beginTransaction();
        
        POJO.Catogory cate=(POJO.Catogory)se.load(POJO.Catogory.class,Integer.parseInt(catid));
        POJO.DeleteStatus de=(POJO.DeleteStatus)se.load(POJO.DeleteStatus.class,Integer.parseInt("1"));
        
        try {
            
            POJO.SubCatogory sub=new POJO.SubCatogory(cate, de, name, null);
            se.save(sub);
            
            tr.commit();
            
        } catch (Exception e) {
        }
    }

   

}
