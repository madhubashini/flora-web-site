/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "shipped_invo", urlPatterns = {"/shipped_invo"})
public class shipped_invo extends HttpServlet {

    private String host;
    private String port;
    private String user;
    private String pass;

    @Override
    public void init() {

        // reads SMTP server setting from web.xml file
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String invid = request.getParameter("inv_id");
        System.out.println(invid + "ideka");

        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = ses.beginTransaction();

        try {

            Criteria crt = ses.createCriteria(POJO.Invoice.class);
            crt.add(Restrictions.eq("id", Integer.parseInt(invid)));
            POJO.Invoice in = (POJO.Invoice) crt.uniqueResult();
            
            String mail=in.getUserReg().getEMail();
            String subject="Madhu Flora";
            String codes="menna me invoice item eka deliver kara";

            POJO.DeliveryStatus del = (POJO.DeliveryStatus) ses.load(POJO.DeliveryStatus.class, Integer.parseInt("2"));

            in.setDeliveryStatus(del);
            ses.update(in);
            tr.commit();

            try {

                EmailUtil.sendEmail(host, port, user, pass, mail, subject, codes);

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
