/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "add_to_cart", urlPatterns = {"/add_to_cart"})
public class add_to_cart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String id = request.getParameter("id");
            String value = request.getParameter("val");

            System.out.println(id + "ideka");
            System.out.println(value + "val eka");

            Added_cart_items adc = new Added_cart_items();

            try {

                if (Integer.parseInt(value) <= 5 && Integer.parseInt(value) != 0) {

                    HttpSession myse = request.getSession();

                    // if (request.getSession().getAttribute("login") == null) {
                    adc.setCart_product_id(Integer.parseInt(id));
                    adc.setCart_product_qty(Integer.parseInt(value));

                    Keep_cart kp = null;

                    if (myse.getAttribute("shopping_cart") == null) {
                        out.print("1");
                        kp = new Keep_cart();

                    } else {
                        out.print("1");
                        kp = (Keep_cart) myse.getAttribute("shopping_cart");
                    }

                    kp.addCart(adc);

                    myse.setAttribute("shopping_cart", kp);
                    // }

                    if (request.getSession().getAttribute("login") != null) {

                        myse.removeAttribute("shopping_cart");

                        System.out.println("aaaaa");

                        HttpSession sesi = request.getSession();

                        Session sei = Connections.NewHibernateUtil.getSessionFactory().openSession();
                        Transaction tr = sei.beginTransaction();

                        POJO.UserReg u = (POJO.UserReg) request.getSession().getAttribute("login");
                        POJO.UserReg ur = (POJO.UserReg) sei.load(POJO.UserReg.class, u.getId());

                        Criteria cr = sei.createCriteria(POJO.Products.class);
                        cr.add(Restrictions.eq("id", Integer.parseInt(id)));
                        POJO.Products pr = (POJO.Products) cr.uniqueResult();

                        System.out.println(" product Ideka" + pr.getId());

                        Criteria cr3 = sei.createCriteria(POJO.DbCart.class);
                        cr3.add(Restrictions.eq("userReg", ur));
                        cr3.add(Restrictions.eq("products", pr));
                        // POJO.DbCart dcart = (POJO.DbCart) cr3.uniqueResult();

                        List<POJO.DbCart> my_db = cr3.list();

                        if (!my_db.isEmpty()) {

                            System.out.println("tiyanawa DB cart eke");

                            String maf = null;
                            for (POJO.DbCart getd : my_db) {
                                maf = getd.getId() + "";
                            }
                            System.out.println(maf + "Db cart eke id eka");

                            POJO.DbCart pojo_db = (POJO.DbCart) sei.load(POJO.DbCart.class, Integer.parseInt(maf));

                            int ints = Integer.parseInt(pojo_db.getQty()) + Integer.parseInt(value);
                            if (ints > 5) {
                                pojo_db.setQty("5");
                                System.out.println("addtocart_page_tiyanawa Db cart eke 5 ta wadie" + ints);
                                sei.update(pojo_db);

                            }
                            if (ints <= 5) {
                                pojo_db.setQty(ints + "");
                                System.out.println("addtocart_page_tiyanawa Db cart eke 5 ta adue" + ints);
                                sei.update(pojo_db);
                            }

                        } else {
                            System.out.println("naaaaa Db cart eke");
                            POJO.DbCart dca = new POJO.DbCart(pr, ur, value);
                            sei.save(dca);
                        }
                        tr.commit();

                        Criteria crs = sei.createCriteria(POJO.DbCart.class);
                        crs.add(Restrictions.eq("userReg", ur));
                        List<POJO.DbCart> db = crs.list();

                        HttpSession my = request.getSession();
                        Keep_cart k = null;

                        for (POJO.DbCart dd : db) {

                            Added_cart_items add = new Added_cart_items();

                            add.setCart_product_id(dd.getProducts().getId());
                            add.setCart_product_qty(Integer.parseInt(dd.getQty()));

                            if (my.getAttribute("shopping_cart") == null) {
                                k = new Keep_cart();
                            } else {
                                k = (Keep_cart) my.getAttribute("shopping_cart");
                            }

                            k.addCart(add);
                            my.setAttribute("shopping_cart", k);

                        }

                        //my.setAttribute("remove", ur);
                    }

                } else {
                    out.print("99");
                }

            } catch (NumberFormatException e) {
                out.print("88");
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
