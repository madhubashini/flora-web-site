/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "ent_email", urlPatterns = {"/ent_email"})
public class ent_email extends HttpServlet {

    private String host;
    private String port;
    private String user;
    private String pass;
    private String resultMessage;

    @Override
    public void init() {

        // reads SMTP server setting from web.xml file
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String emails = request.getParameter("email");

        String subject = "Madhu Flora";

        Session s = Connections.NewHibernateUtil.getSessionFactory().openSession();

        try {
            Transaction tr = s.beginTransaction();
            POJO.UserStatus ut = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("1"));
            POJO.UserStatus ut2 = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("2"));
            POJO.UserStatus ut3 = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("3"));
            POJO.UserStatus ut4 = (POJO.UserStatus) s.load(POJO.UserStatus.class, Integer.parseInt("4"));

            Criteria c = s.createCriteria(POJO.UserReg.class);
            c.add(Restrictions.eq("EMail", emails));
            List<POJO.UserReg> uli = c.list();

            POJO.UserReg ure = null;
            if (!uli.isEmpty()) {
                ure = (POJO.UserReg) c.uniqueResult();
            }
            if (ure == null) {
                out.print("5");
            }

            if (ure != null && ure.getUserStatus().equals(ut)) {

                String link = "http://localhost:8080/Madhu_Flora_New/frgt_pswd.jsp?id=" + ure.getCodeGen() + "";
                //send email code eka
                try {
                    EmailUtil.sendEmail(host, port, user, pass, emails, subject, link);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                out.print("1");
            }

            if (ure != null && ure.getUserStatus().equals(ut2)) {
                out.print("2");
            }
            if (ure != null && ure.getUserStatus().equals(ut3)) {
                out.print("3");
            }
            if (ure != null && ure.getUserStatus().equals(ut4)) {
                out.print("4");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
