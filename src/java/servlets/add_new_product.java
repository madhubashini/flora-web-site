/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author HP
 */
@WebServlet(name = "add_new_product", urlPatterns = {"/add_new_product"})
public class add_new_product extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String names = "";
        String wgt = "";
        String item = "";
        String untpr = "";
        String cat = "";
        String descs = "";
        String prmm = "";
        String path = "FILES/";

        try {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List<FileItem> items = upload.parseRequest(request);

            for (Object element : items) {
                FileItem fileItem = (FileItem) element;
                if (fileItem.isFormField()) {

                    if (fileItem.getFieldName().equals("name")) {
                        names = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("weight")) {
                        wgt = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("item")) {
                        item = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("untp")) {
                        untpr = fileItem.getString();
                    }

                    if (fileItem.getFieldName().equals("catg")) {
                        cat = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("desc")) {
                        descs = fileItem.getString();
                    }
                    if (fileItem.getFieldName().equals("promo")) {
                        prmm = fileItem.getString();
                    }
                } else {

                    File sevedFile = new File(request.getServletContext().getRealPath("/") + path + fileItem.getName());
                    path += fileItem.getName();
                    fileItem.write(sevedFile);

                }
            }

            Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
            Transaction tr = ses.beginTransaction();

            POJO.SubCatogory sub = (POJO.SubCatogory) ses.load(POJO.SubCatogory.class, Integer.parseInt(cat));
            POJO.Promotionn prm = (POJO.Promotionn) ses.load(POJO.Promotionn.class, Integer.parseInt(prmm));
            POJO.DeleteStatus del = (POJO.DeleteStatus) ses.load(POJO.DeleteStatus.class, Integer.parseInt("1"));

            POJO.Products pr = new POJO.Products(del, prm, sub, names, path, descs, wgt, item, untpr, null, null, null,null);

            ses.save(pr);
            tr.commit();
            
            response.sendRedirect("admin_ad_product.jsp");
            ses.close();

            // out.print("uploaded");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
//names:name,img:image,we:weight,it:items,pr:price,st:status,ctg:cat,des:decs
