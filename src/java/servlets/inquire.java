/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author HP
 */
@WebServlet(name = "inquire", urlPatterns = {"/inquire"})
public class inquire extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String us = request.getParameter("us");
        String item = request.getParameter("ite");
        String qty = request.getParameter("qa");
        String date = request.getParameter("dat");

        System.out.println(us + qty + date + item);
        
        Session hises=Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=hises.beginTransaction();
        
        POJO.Products pr=(POJO.Products)hises.load(POJO.Products.class,Integer.parseInt(item));
        POJO.UserReg usr=(POJO.UserReg)hises.load(POJO.UserReg.class,Integer.parseInt(us));
        try {
            
            POJO.Inqu inqu=new POJO.Inqu(pr,usr, qty, date,"1");
            hises.save(inqu);
            tr.commit();
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
