/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "user_active", urlPatterns = {"/user_active"})
public class user_active extends HttpServlet {

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       PrintWriter out=response.getWriter();
       
        String id=request.getParameter("user");
        //System.out.println(id);
        
        Session session=Connections.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=session.beginTransaction();
        
        try {
            
            Criteria crt=session.createCriteria(POJO.UserReg.class);
            crt.add(Restrictions.eq("id",Integer.parseInt(id)));
            POJO.UserReg user=(POJO.UserReg)crt.uniqueResult();
            
            POJO.UserStatus st=(POJO.UserStatus)session.load(POJO.UserStatus.class,Integer.parseInt("1"));
            
            user.setUserStatus(st);
            session.update(user);
            tr.commit();
            
        } catch (Exception e) {
        }
        
        
        
        
    }

    

}
