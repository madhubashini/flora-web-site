/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author HP
 */
@WebServlet(name = "ent_psw2", urlPatterns = {"/ent_psw2"})
public class ent_psw2 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        String code = request.getParameter("id");
        String psw = request.getParameter("pwrd");
        String pswold = request.getParameter("newps");

        Session s = Connections.NewHibernateUtil.getSessionFactory().openSession();

        try {
            Transaction tr = s.beginTransaction();

            Criteria c = s.createCriteria(POJO.UserReg.class);
            c.add(Restrictions.eq("codeGen", code));
            c.add(Restrictions.eq("password", pswold));
            List<POJO.UserReg> u = c.list();
            POJO.UserReg ure = null;
            if (!u.isEmpty()) {
                ure = (POJO.UserReg) c.uniqueResult();
            }else{
              out.print("2");
            }

            if (ure != null) {
                POJO.UserReg usr = (POJO.UserReg) s.get(POJO.UserReg.class, ure.getId());
                usr.setPassword(psw);
                s.update(usr);
                tr.commit();
                out.print("1");
            } else {
                //out.print("0");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
