package POJO;
// Generated Sep 30, 2016 1:51:48 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * DeliveryInfo generated by hbm2java
 */
public class DeliveryInfo  implements java.io.Serializable {


     private Integer id;
     private RecipientInfo recipientInfo;
     private SendersInfo sendersInfo;
     private String instruction;
     private String message;
     private String time;
     private String date;
     private Set invoices = new HashSet(0);

    public DeliveryInfo() {
    }

	
    public DeliveryInfo(RecipientInfo recipientInfo, SendersInfo sendersInfo) {
        this.recipientInfo = recipientInfo;
        this.sendersInfo = sendersInfo;
    }
    public DeliveryInfo(RecipientInfo recipientInfo, SendersInfo sendersInfo, String instruction, String message, String time, String date, Set invoices) {
       this.recipientInfo = recipientInfo;
       this.sendersInfo = sendersInfo;
       this.instruction = instruction;
       this.message = message;
       this.time = time;
       this.date = date;
       this.invoices = invoices;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public RecipientInfo getRecipientInfo() {
        return this.recipientInfo;
    }
    
    public void setRecipientInfo(RecipientInfo recipientInfo) {
        this.recipientInfo = recipientInfo;
    }
    public SendersInfo getSendersInfo() {
        return this.sendersInfo;
    }
    
    public void setSendersInfo(SendersInfo sendersInfo) {
        this.sendersInfo = sendersInfo;
    }
    public String getInstruction() {
        return this.instruction;
    }
    
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    public String getTime() {
        return this.time;
    }
    
    public void setTime(String time) {
        this.time = time;
    }
    public String getDate() {
        return this.date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    public Set getInvoices() {
        return this.invoices;
    }
    
    public void setInvoices(Set invoices) {
        this.invoices = invoices;
    }




}


