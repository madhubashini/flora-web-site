package POJO;
// Generated Sep 30, 2016 1:51:48 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Invoice generated by hbm2java
 */
public class Invoice  implements java.io.Serializable {


     private Integer id;
     private DeliveryInfo deliveryInfo;
     private DeliveryStatus deliveryStatus;
     private UserReg userReg;
     private String subtot;
     private String date;
     private Set removeDescrpts = new HashSet(0);
     private Set payedItemses = new HashSet(0);

    public Invoice() {
    }

	
    public Invoice(DeliveryInfo deliveryInfo, DeliveryStatus deliveryStatus, UserReg userReg) {
        this.deliveryInfo = deliveryInfo;
        this.deliveryStatus = deliveryStatus;
        this.userReg = userReg;
    }
    public Invoice(DeliveryInfo deliveryInfo, DeliveryStatus deliveryStatus, UserReg userReg, String subtot, String date, Set removeDescrpts, Set payedItemses) {
       this.deliveryInfo = deliveryInfo;
       this.deliveryStatus = deliveryStatus;
       this.userReg = userReg;
       this.subtot = subtot;
       this.date = date;
       this.removeDescrpts = removeDescrpts;
       this.payedItemses = payedItemses;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public DeliveryInfo getDeliveryInfo() {
        return this.deliveryInfo;
    }
    
    public void setDeliveryInfo(DeliveryInfo deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }
    public DeliveryStatus getDeliveryStatus() {
        return this.deliveryStatus;
    }
    
    public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }
    public UserReg getUserReg() {
        return this.userReg;
    }
    
    public void setUserReg(UserReg userReg) {
        this.userReg = userReg;
    }
    public String getSubtot() {
        return this.subtot;
    }
    
    public void setSubtot(String subtot) {
        this.subtot = subtot;
    }
    public String getDate() {
        return this.date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    public Set getRemoveDescrpts() {
        return this.removeDescrpts;
    }
    
    public void setRemoveDescrpts(Set removeDescrpts) {
        this.removeDescrpts = removeDescrpts;
    }
    public Set getPayedItemses() {
        return this.payedItemses;
    }
    
    public void setPayedItemses(Set payedItemses) {
        this.payedItemses = payedItemses;
    }




}


