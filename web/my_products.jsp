<%-- 
    Document   : my_products
    Created on : Dec 23, 2015, 11:05:24 AM
    Author     : HP
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="POJO.SubCatogory"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.hibernate.Session"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Products</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" type="text/css" href="css/pagination.css" />

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>        
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="js/my_javascripts.js"></script> 

    </head>
    <body>



        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#">Madhu Flora</a></div>

                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->
            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">

                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <h2>Products</h2>
                        <p></p>

                        <%                            //boolean bb;
                            String statss;
                            int index = 0;
                            int records = 0;
                            int recordsPerPage = 8;
                            String urlID = request.getParameter("id");
                            String key = request.getParameter("keys");
                            String realPageIndex = request.getParameter("PageIndex");

                            if (realPageIndex == null) {
                                index = 1;
                            } else {
                                index = Integer.parseInt(realPageIndex);
                            }
                            int result = (index * recordsPerPage) - recordsPerPage;

                            Session sess = Connections.NewHibernateUtil.getSessionFactory().openSession();

                            Criteria cri = sess.createCriteria(POJO.Products.class);
                            POJO.DeleteStatus pjp = (POJO.DeleteStatus) sess.load(POJO.DeleteStatus.class, Integer.parseInt("2"));
                            POJO.DeleteStatus po = (POJO.DeleteStatus) sess.load(POJO.DeleteStatus.class, Integer.parseInt("3"));
                            cri.add(Restrictions.ne("deleteStatus", pjp));

                            if (key != null) {
                                cri.add(Restrictions.like("name", "%" + key + "%"));

                            }
                            if (urlID != null) {
                                POJO.SubCatogory sb = (POJO.SubCatogory) sess.load(POJO.SubCatogory.class, Integer.parseInt(urlID));
                                cri.add(Restrictions.eq("subCatogory", sb));
                            }

                            cri.setFirstResult(result);
                            cri.setMaxResults(recordsPerPage);

                            List<POJO.Products> list = cri.list();

                            for (POJO.Products pro : list) {
                                double prv = Double.parseDouble(pro.getUnitPrice());
                                if (pro.getDeleteStatus().equals(po)) {
                                    statss = "Out Of Stock";
                                } else {
                                    statss = "In Stock";
                                }

                                DecimalFormat d = new DecimalFormat("0.00");
                        %>

                        <div class="product_box">
                            <img src=<%= pro.getImg()%> alt="floralset1" width="165" height="165"/>
                            <h3><%=pro.getName()%></h3>
                            <p class="product_price">Rs:<%=d.format(prv)%></p>
                            <p class="product_price"><%=statss%></p>
                            <%
                                if (!(pro.getDeleteStatus().equals(po))) {
                            %>
                            <p class="add_to_cart">
                                <a onclick="sendreques('<%=pro.getId()%>')">Add to Cart</a>
                                <script type="text/javascript">
                                    function sendreques(id) {
                                        var url = " my_product_details.jsp?id=" + id;
                                        window.location.replace(url);

                                    }
                                </script>
                                <%
                                    }
                                %>
                            </p>
                        </div>     

                        <%
                            }
                            Criteria crii = sess.createCriteria(POJO.Products.class);
                            crii.add(Restrictions.ne("deleteStatus", pjp));

                            if (key != null) {
                                crii.add(Restrictions.like("name", "%" + key + "%"));
                            }

                            if (urlID != null) {
                                POJO.SubCatogory sbb = (POJO.SubCatogory) sess.load(POJO.SubCatogory.class, Integer.parseInt(urlID));
                                crii.add(Restrictions.eq("subCatogory", sbb));
                            }
                            List<POJO.Products> li = crii.list();
                            int productCount = 0;
                            for (POJO.Products pr : li) {
                                //out.print( pr.getName());
                                ++productCount;
                            }
                            int pages = productCount / 8;
                            if (productCount > (pages * 8)) {
                                pages = pages + 1;
                            }

                        %>
                        <div class="pagination">
                            <ul>

                                <%    //
                                    String newUrl = "";
                                    for (int i = 1; i <= pages; i++) {
                                        String url = "http://localhost:8080/Madhu_Flora_New/my_products.jsp";
                                        if (urlID != null) {
                                            newUrl = url + "?PageIndex=" + i + "&id=" + urlID;
                                        } else {
                                            newUrl = url + "?PageIndex=" + i;
                                        }
                                        if (key != null) {
                                            newUrl = url + "?PageIndex=" + i + "&keys=" + key;

                                        }

                                %>



                                <li><a href="<%=newUrl%>"><%=i%></a>

                                    <%
                                        }

                                    %>
                            </ul>
                        </div>
                    </div>

                    <div class="cleaner"></div>

                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>
    </body>
</html>
