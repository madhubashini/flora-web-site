<%-- 
    Document   : my_userProfile
    Created on : Mar 6, 2016, 1:41:46 PM
    Author     : HP
--%>

<%
    Session vec1 = Connections.NewHibernateUtil.getSessionFactory().openSession();

    POJO.UserReg u = (POJO.UserReg) request.getSession().getAttribute("login");

    if (u != null) {
        POJO.UserReg urt = (POJO.UserReg) vec1.load(POJO.UserReg.class, u.getId());
        if (urt.getUserStatus().getId() == 1) {

%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>prof</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/check.css">

        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>       
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="js/my_javascripts.js"></script> 
    </head>
    <body>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>
                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>

                    <%                        if (urt != null) {
                            String code = urt.getCodeGen();

                    %>
                    <div id="content" class="right">    

                        <form action="" class="contact" style="width: 685px;height:auto">

                            <fieldset class="contact-inner"style="height:auto">
                                <p>
                                <h1 style="color: black;margin-left:150px">Account Details</h1>
                                </p>
                                <p>
                                <h4 style=" color: black;margin-left:10px"> Hi <%=urt.getFstName()%> , <br><br>
                                    This is your account details.
                                    you can edit your details and show order history here .</h4>
                                </p>
                                <table style="margin-left: 10px">
                                    <tr>
                                        <td ><h4 style="color: black">Name</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td style="color: black"><h4 style="color: black"><%= urt.getFstName()%> &nbsp;<%=urt.getLastName()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td style="color: black"><h4 style="color: black">E-mail</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td style="color: black"><h4 style="color: black"><%=urt.getEMail()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4 style="color: black">User Name</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=urt.getUserName()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4 style="color: black">Password</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><a onclick="ide()" style="color: red;text-decoration: underline">Change Password</a></h4></td>
                                    </tr>



                                    <script type="text/javascript">
                                        function ide() {
                                            var ids = '<%=urt.getCodeGen()%>';
                                            var url = "forgot_psw2.jsp?id=" + ids;
                                            window.location.replace(url);
                                        }
                                    </script>



                                    <%

                                        Criteria krit = vec1.createCriteria(POJO.DefaultShipping.class);
                                        krit.add(Restrictions.eq("userReg", urt));
                                        //crt.addOrder(Order.desc("id"));
                                        List<POJO.DefaultShipping> shp = krit.list();
                                        // out.print(shp.size());

                                        Criteria ct44 = vec1.createCriteria(POJO.RecipientInfo.class);
                                        ct44.add(Restrictions.eq("userReg", urt));
                                        List<POJO.RecipientInfo> rec_info2 = ct44.list();

                                        if (shp.isEmpty()) {

                                            Criteria crtm = vec1.createCriteria(POJO.RecipientInfo.class);
                                            crtm.add(Restrictions.eq("userReg", urt));
                                            crtm.addOrder(Order.desc("id"));
                                            List<POJO.RecipientInfo> kirtList = crtm.list();
                                            innerfor:
                                            for (POJO.RecipientInfo dfa : kirtList) {
                                                POJO.RecipientInfo send = (POJO.RecipientInfo) vec1.load(POJO.RecipientInfo.class, dfa.getId());

                                    %>

                                    <tr>
                                        <td><h4 style="color: black">Default Shipping Address</h4> </td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td>
                                            <h4 style="color: black"><%=send.getTitle()%> .&nbsp;<%=send.getName()%>
                                                &nbsp &nbsp <a onclick="go1('<%=code%>')" style="color: red;text-decoration: underline">Change</a></h4>
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=send.getAddress()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=send.getAddress2()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>  
                                        <td><h4 style="color: black"><%=send.getDeliveryCity().getCity()%></h4></td>  
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=send.getPhoneNum()%></h4></td>
                                    </tr>


                                    <%
                                            break innerfor;

                                        }

                                    %>


                                    <%   }
                                        if (!shp.isEmpty()) {
                                            //  out.print("awaaa");

                                            inner:
                                            for (POJO.DefaultShipping sh : shp) {
                                                POJO.DefaultShipping sen = (POJO.DefaultShipping) vec1.load(POJO.DefaultShipping.class, sh.getId());

                                    %>


                                    <tr>
                                        <td><h4 style="color: black">Default Shipping Address</h4> </td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td>
                                            <h4 style="color: black"><%=sen.getTitle()%> .&nbsp;<%=sen.getName()%>
                                                &nbsp &nbsp <a onclick="go1('<%=code%>')" style="color: red;text-decoration: underline">Change</a></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=sen.getAddress()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=sen.getAddress2()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>  
                                        <td><h4 style="color: black"><%=sen.getDeliveryCity().getCity()%></h4></td>  
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=sen.getPhoneNum()%></h4></td>
                                    </tr>


                                    <%
                                            break inner;

                                        }

                                    %>


                                    <%     }
                                        if (shp.isEmpty() && rec_info2.isEmpty()) {

                                    %>

                                    <tr>
                                        <td><h4 style="color: black">Default Shipping Address</h4> </td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4><a onclick="go1('<%=code%>')" style="color: red;text-decoration: underline">Change</a></h4></td>
                                    </tr>

                                    <%
                                        }

                                    %>


                                    <%                                        Criteria c = vec1.createCriteria(POJO.DefaultBilling.class);
                                        c.add(Restrictions.eq("userReg", urt));
                                        // c.addOrder(Order.desc("id"));
                                        List<POJO.DefaultBilling> dbill = c.list();

                                        Criteria ct2 = vec1.createCriteria(POJO.SendersInfo.class);
                                        ct2.add(Restrictions.eq("userReg", urt));
                                        List<POJO.SendersInfo> re_list = ct2.list();

                                        if (dbill.isEmpty()) {

                                            Criteria c2 = vec1.createCriteria(POJO.SendersInfo.class);
                                            c2.add(Restrictions.eq("userReg", urt));
                                            c2.addOrder(Order.desc("id"));

                                            List<POJO.SendersInfo> myse = c2.list();
                                            inf:
                                            for (POJO.SendersInfo s : myse) {
                                                POJO.SendersInfo sends = (POJO.SendersInfo) vec1.load(POJO.SendersInfo.class, s.getId());

                                    %>

                                    <tr>
                                        <td><h4 style="color: black">Default Billing Details</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=sends.getTitle()%>.&nbsp;<%=sends.getName()%>
                                                &nbsp &nbsp <a onclick="sen('<%=code%>')" style="color: red;text-decoration: underline">Change</a></h4></td>

                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=sends.getEmail()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=sends.getPhoneNum()%></h4></td>
                                    </tr>



                                    <%
                                                break inf;
                                            }

                                        }
                                        if (!dbill.isEmpty()) {

                                            loops:
                                            for (POJO.DefaultBilling bl : dbill) {
                                                POJO.DefaultBilling se = (POJO.DefaultBilling) vec1.load(POJO.DefaultBilling.class, bl.getId());

                                    %>


                                    <tr>
                                        <td><h4 style="color: black">Default Billing Details</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=se.getTitle()%>.&nbsp;<%=se.getName()%>
                                                &nbsp &nbsp <a onclick="sen('<%=code%>')" style="color: red;text-decoration: underline">Change</a></h4></td>

                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=se.getEmail()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><h4 style="color: black"><%=se.getPhoneNum()%></h4></td>
                                    </tr>


                                    <%
                                                break loops;
                                            }

                                        }
                                        if (dbill.isEmpty() && re_list.isEmpty()) {

                                    %>

                                    <tr>
                                        <td><h4 style="color: black">Default Billing Details</h4> </td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4><a onclick="sen('<%=code%>')" style="color: red;text-decoration: underline">Change</a></h4></td>
                                    </tr>

                                    <%                                        }
                                    %>




                                </table>


                                <%
                                    Criteria c3 = vec1.createCriteria(POJO.Invoice.class);
                                    c3.add(Restrictions.eq("userReg", urt));
                                    c3.addOrder(Order.desc("id"));
                                    List<POJO.Invoice> invc = c3.list();
                                    if (!invc.isEmpty()) {
                                %>
                                <table>
                                    <tr>
                                        <td><h4 style="color: black">Order Summery</h4></td>
                                        <td><h4 style="color: black">:</h4> </td>
                                    </tr>
                                </table>

                                <table class="table1">
                                    <thead>
                                        <tr>
                                            <th scope="col" abbr="Starter">Invoice ID</th>
                                            <th scope="col" abbr="Medium">Sender Name</th>
                                            <th scope="col" abbr="Business">Receiver Name</th>
                                            <th scope="col" abbr="Deluxe">Delivery Date</th>
                                            <th scope="col" abbr="Deluxe">Shipping Status</th>
                                            <th scope="col" abbr="Deluxe">More Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%                                            DecimalFormat x1 = new DecimalFormat("0.00");
                                            double d;

                                            for (POJO.Invoice i : invc) {
                                                d = Double.parseDouble(i.getSubtot());

                                        %>
                                        <tr>
                                            <td><%=i.getId()%></td>
                                            <td><%=i.getDeliveryInfo().getSendersInfo().getName()%></td>
                                            <td><%=i.getDeliveryInfo().getRecipientInfo().getName()%></td>
                                            <td><%=i.getDeliveryInfo().getDate()%></td>
                                            <td><%=i.getDeliveryStatus().getStatus()%></td>
                                            <td><a  style="color: red" onclick="accn('<%=i.getId()%>')">View</a></td>

                                        </tr>
                                        <%    }

                                        %>

                                    </tbody>
                                </table>

                                <%                                            }
                                %>
                                <br>
                                <br>

                                <p class="contact-submit">
                                    <a  id="deactbtn" onclick="mads('<%=urt.getCodeGen()%>')" class="New-lassana-btn">Deactivate Account</a>
                                </p>


                            </fieldset>
                            <%                                } else {

                                }

                            %>
                        </form>



                        <script type="text/javascript">

                            function go1(getc) {
                                // alert("aaaa");
                                var ur = getc;
                                window.location.replace("change_shipping.jsp?codeI=" + ur);

                            }

                        </script>
                        <script type="text/javascript">

                            function sen(gets) {
                                // alert("aaaaa");
                                var ur = gets;
                                window.location.replace("change_billing.jsp?codeI=" + ur);

                            }

                        </script>
                        <script type="text/javascript">
                            function mads(rest) {
                                // alert(rest);
                                window.location.replace("my_userPro_deac.jsp?code=" + rest);
                            }
                        </script>
                        <script type="text/javascript">
                            function accn(result) {
                                //alert(result);
                                var url = "my_userPro_details.jsp?id=" + result;
                                window.location.replace(url);
                            }
                        </script>
                    </div>


                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>

        </div>                
    </body>
</html>
<%//
        } else {
            request.getSession().invalidate();
            response.sendRedirect("index.jsp");
        }
    } else {
        response.sendRedirect("User_login.jsp");
    }
%>