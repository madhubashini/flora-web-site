<%-- 
    Document   : frgt_pswd
    Created on : Jan 3, 2016, 5:47:50 PM
    Author     : HP
--%>

<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/loginpage.css">

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>      
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>        
        <script type="text/javascript" src="js/my_javascripts.js"></script>

    </head>
    <body>
        <%
            String id = request.getParameter("id");
            //out.print(id);
        %>

        <style>
            #templatemo_menu1 a{
                color: darkorange;
            }
        </style>
        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <br>
                        <br>
                        <div class="form">

                            <div class="tab-content">
                                <div id="loginss"> 
                                    <h1>Change Password!</h1>

                                    <form id="resetfrgtp">
                                        <div class="field-wrap">
                                            <input type="password" id="ps1"  placeholder="New Password" autofocus>
                                        </div>
                                        <p style="color: white" id="ps_emp1" class="hideitems">Password Fields Cant'be Empty</p>
                                        <div class="field-wrap">
                                            <input type="password" id="ps2"  placeholder="Re-Type password">
                                        </div>
                                        <p style="color: white" id="ps_emp2" class="hideitems">Password Fields Cant'be Empty</p>
                                        <p style="color: white" id="lengths1" class="hideitems">Password Length Doesn't Match</p>
                                        <p style="color: white" id="matchs" class="hideitems">Password Doesn't Match</p>
                                        <p style="color: white" id="succs" class="hideitems">Your Password Has Changed Successfully</p>


                                        <ul class="tab-group">
                                            <li><a  onclick="test()" class="tab">Change</a></li>  
                                        </ul>

                                        <br><br>
                                    </form>


                                </div>
                            </div>
                        </div>




                        <script>

                            function test() {
                                // alert("awa");

                                var p1 = document.getElementById("ps1").value;
                                var p2 = document.getElementById("ps2").value;

                                if (p1 == "") {
                                    $('#ps_emp1').show();
                                    pswfil = false;
                                }
                                if (p2 == "") {
                                    $('#ps_emp2').show();
                                    pswfil = false;
                                }
                                if ((!p1 == "") && (!p2 == "")) {
                                    pswfil = true;

                                    if (p1 == p2) {
                                        pwmatch = true;


                                        if ((p1.length <= 4) || ((p2.length <= 4))) {
                                            $('#lengths1').show();
                                            pwlen = false;
                                        } else {
                                            pwlen = true;
                                        }


                                    } else {
                                        $('#matchs').show();
                                        pwmatch = false;
                                    }

                                }

                                if (pswfil == true && pwmatch == true && pwlen == true) {
                                    callservlet();
                                }
                            }


                            function callservlet() {

                                var pwrdsend = $("#ps1").val();
                                var id_frm_url = '<%=id%>';
                                //alert(id_frm_url);
                                $.post(
                                        "ent_passw",
                                        {id: id_frm_url, pwrd: pwrdsend},
                                function (result) {
                                    var data = result.toString();
                                    if (data == 1) {
                                        $('#succs').show();
                                     
                                    }
                                    if (data == 0) {

                                    }
                                    //alert(data);
                                });


                            }

                        </script>

                        <div class="cleaner h20"></div>                       
                        <div class="cleaner h20"></div>

                    </div>

                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>                             

        <!-- END of main wrapper -->

        <!--  
        
        -->
    </body>
</html>
