<%-- 
    Document   : change_shipping
    Created on : Jun 10, 2016, 10:21:08 AM
    Author     : HP
--%>

<%
    Session vec1 = Connections.NewHibernateUtil.getSessionFactory().openSession();

    POJO.UserReg u = (POJO.UserReg) request.getSession().getAttribute("login");

    if (u != null) {
        POJO.UserReg urt = (POJO.UserReg) vec1.load(POJO.UserReg.class, u.getId());
        if (urt.getUserStatus().getId() == 1) {

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/loginpage.css">

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>      
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>        
        <script type="text/javascript" src="js/my_javascripts.js"></script>

    </head>

    <body>




        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <br>
                        <br>
                        <div class="form">
                            <div class="tab-content">
                                <h1>Change Shipping Details!</h1>

                                <form id="changeship">

                                    <%    String awaCo = request.getParameter("codeI");
                                    %>

                                    <div class="field-wrap">
                                        <select class="madish-sele" id="sel1" autofocus>
                                            <option  value="0" selected="se">Select Title</option>
                                            <option value="1">Mr</p></option>
                                            <option value="1">Ms</option>
                                        </select>
                                    </div>
                                    <p  style="color: white" id="tit" class="hideitems">Select Title</p>
                                    <div class="field-wrap">
                                        <input type="text" id="na"  placeholder="Name">
                                    </div>
                                    <p  style="color: white" id="nams" class="hideitems">Name Can't be empty</p>
                                    <div class="field-wrap">
                                        <input type="text" id="ad1"  placeholder="Address 1">
                                    </div>
                                    <div class="field-wrap">
                                        <input type="text" id="ad2"  placeholder="Address 2">
                                    </div>
                                    <p  style="color: white" id="addr" class="hideitems">Fill at least one field</p>
                                    <div class="field-wrap">
                                        <select class="madish-sele"  id="sel2">
                                            <option  value="0" selected="se">Select City</option>
                                            <%                                    Session ssn1 = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                                Criteria krt2 = ssn1.createCriteria(POJO.DeliveryCity.class);
                                                List<POJO.DeliveryCity> dc = krt2.list();
                                                for (POJO.DeliveryCity d : dc) {

                                            %>
                                            <option value="1"><%=d.getCity()%></option>
                                            <%}%>

                                        </select>
                                    </div>
                                    <p  style="color: white" id="citys" class="hideitems">Select city</p>
                                    <div class="field-wrap">
                                        <input type="tel" id="phn"  placeholder="Phone Number">
                                    </div>
                                    <p  style="color: white" id="p_emp" class="hideitems">Phone NUmber Can't be empty</p>
                                    <p  style="color: white" id="p_ln" class="hideitems">Invalid phone number</p>
                                    <div class="field-wrap">
                                        <select class="madish-sele" id="sel3">
                                            <option value="0"selected="se">Select Location Type</option>
                                            <option value="1">Home</option>
                                            <option value="1">Flat</option>
                                            <option value="1">Office</option>
                                            <option value="1">Hospital</option>
                                            <option value="1">Other</option>
                                        </select>
                                    </div>
                                    <p  style="color: white" id="loca" class="hideitems">Select Location</p>
                                    <p style="color: white" id="succs" class="hideitems">Changed Successfully</p>

                                    <ul class="tab-group">
                                        <li><a onclick="callser()" class="sign-up-button">Change</a></li>     
                                    </ul>

                                    <br><br>

                                </form>


                            </div>


                        </div>





                        <script type="text/javascript">

                            function callser() {
                                //alert("aaaa");
                                var user = '<%=awaCo%>';
                                var title = $('#sel1').find('option:selected').text();
                                var tit = $('#sel1').find('option:selected').val();
                                var name = document.getElementById("na").value;
                                var add1 = document.getElementById("ad1").value;
                                var add2 = document.getElementById("ad2").value;
                                var city = $('#sel2').find('option:selected').text();
                                var cit_val = $('#sel2').find('option:selected').val();
                                var phn = document.getElementById("phn").value;
                                var loca = $('#sel3').find('option:selected').text();
                                var loca_val = $('#sel3').find('option:selected').val();
                                //alert(title+name+add1+add2+city+phn+loca);

                                if (tit == 1) {
                                    titl = true;
                                } else {
                                    $('#tit').show();
                                    titl = false;
                                }

                                if (name == "") {
                                    $("#nams").show();
                                    nam = false;
                                } else {
                                    nam = true;
                                }

                                if ((add1 == "" && add2 == "")) {
                                    $('#addr').show();
                                    addrs = false;
                                } else {
                                    addrs = true;
                                }

                                if (cit_val == 1) {
                                    cit = true;
                                } else {
                                    $('#citys').show();
                                    cit = false;
                                }
                                var phonefil = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/;
                                if (phn == "") {
                                    phns = false;
                                    $('#p_emp').show();
                                } else {
                                    phns = true;
                                    if (!(phonefil.test(phn))) {
                                        phnlen = false;
                                        $('#p_ln').show();
                                    } else {
                                        phnlen = true;

                                    }
                                }

                                if (loca_val == 1) {
                                    loc = true;
                                } else {
                                    $('#loca').show();
                                }

                                if (titl == true && nam == true && addrs == true && cit == true && phns == true && phnlen == true && loc == true) {

                                    $.post(
                                            "change_ship_address",
                                            {ti: title, na: name, a1: add1, a2: add2, ci: city, p: phn, lo: loca, use: user},
                                    function (result) {
                                        $('#succs').show();
                                        document.getElementById("changeship").reset();

                                    }

                                    );
                                }

                            }

                        </script>

                        <div class="cleaner h20"></div>                       
                        <div class="cleaner h20"></div>

                    </div>

                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>                             

        <!-- END of main wrapper -->

        <!--  
        
        -->
    </body>
</html>

<%//
        } else {
            request.getSession().invalidate();
            response.sendRedirect("index.jsp");
        }
    } else {
        response.sendRedirect("User_login.jsp");
    }
%>