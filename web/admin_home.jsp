<%-- 
    Document   : admin_home
    Created on : Jun 24, 2016, 7:06:56 PM
    Author     : HP
--%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%
    String num = null;
    if (request.getSession().getAttribute("login") != null) {
        POJO.UserReg logu = (POJO.UserReg) request.getSession().getAttribute("login");
        num = logu.getUserType().getId() + "";
        System.out.print("if eke");
        
        if (num.equals("2")) {
            

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/fullcalendar.css' rel='stylesheet'>
        <link href='admin_css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link href='admin_css/uploadify.css' rel='stylesheet'>

        <link rel="shortcut icon" href="admin_image/favicon.ico">

    </head>
    <body>
        <!-- topbar start -->
        <%@include file="admin_header.jsp" %>
        <!-- topbar ends -->
        <div class="container-fluid">

            <div class="row-fluid">
                <!-- left menu starts -->

                <!-- left menu ends -->
                <div id="content" class="span10" style="margin-left: 8%">
                    <!-- content starts -->
                    <div>
                        <table>
                            <tr>
                                <td style="width: 100px"></td>
                                <td><img src="images/home3.jpe" width="130px" height="130px"></td>
                                <td style="width: 40px"></td>
                                <td><h1>Administrator Home Page</h1></td>
                            </tr>
                        </table>

                    </div>
                    <br>
                    <br>
                    <br>
                    <p style="font-size: 20px">Welcome Administrators !</p><br>
                    <p style="font-size: 16px"> * &nbsp; You can use  Top Left corner Logout link to logout from site and Admin Profile link to log your profile .</p>
                    <p style="font-size: 16px">* &nbsp; You can use Top Right corner Visit Site link to visit the Madhu Flora Home Page</p>
                    <p style="font-size: 16px">* &nbsp; You can use Bottom Main Menu to access the administrator activities</p>
                    <p style="font-size: 16px">* &nbsp; Please Keep Mind to Logout when you done your activities</p>


                    <br>
                    <h2>Main Menu -</h2>
                    <br>

                    <div class="row-fluid sortable">
                        <div class="box span12">

                            <div class="box-header well" data-original-title>
                            </div>
                            <div class="box-content">
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_ad_product.jsp" style="margin-left: 50px;margin-right: 50px">
                                    <div style="height: 100px"><img src="images/addproduct.jpe" width="100px" height="100px"></div>
                                    <div>Add Product</div>
                                    <div style="height: 20px"></div>
                                </a>
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_search_product.jsp" style="margin-left: 50px;margin-right: 50px">
                                    <div style="height: 100px"><img src="images/searchpro.jpe" width="100px" height="100px"></div>
                                    <div>Search Product</div>
                                    <div style="height: 20px"></div>
                                </a>
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_edit_product.jsp" style="margin-left: 50px;margin-right: 50px"> 
                                    <div style="height: 100px"><img src="images/editproduct.jpe" width="100px" height="100px"></div>
                                    <div>Edit Product</div>
                                    <div style="height: 20px"></div>
                                </a>
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_weekly_special.jsp" style="margin-left: 50px;margin-right: 50px">
                                    <div style="height: 100px"><img src="images/weekly3.jpe" width="100px" height="100px"></div>
                                    <div>Weekly Special</div>
                                    <div style="height: 20px"></div>
                                </a>
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_ad_category.jsp" style="margin-left: 50px;margin-right: 50px">
                                    <div style="height: 100px"><img src="images/category.jpe" width="100px" height="100px"></div>
                                    <div>Category</div>
                                    <div style="height: 20px"></div>
                                </a>
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_ad_promotion.jsp" style="margin-left: 50px;margin-right: 50px">
                                    <div style="height: 100px"><img src="images/discount.jpe" width="100px" height="100px"></div>
                                    <div>Promotion</div>
                                    <div style="height: 20px"></div>

                                </a>
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_user_add.jsp" style="margin-left: 50px;margin-right: 50px">
                                    <div style="height: 100px"><img src="images/user.jpe" width="100px" height="100px"></div>                                    
                                    <div>Manage User</div>
                                    <div style="height: 20px"></div>

                                </a>
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_order_mng.jsp" style="margin-left: 50px;margin-right: 50px">
                                    <div style="height: 100px"><img src="images/order.jpe" width="100px" height="100px"></div>
                                    <div>Manage Order</div>
                                    <div style="height: 20px"></div>
                                </a>
                                <a data-rel="tooltip" class="well span3 top-block" href="admin_manage_inqu.jsp" style="margin-left: 50px;margin-right: 50px">
                                    <div style="height: 100px"><img src="images/form.jpe" width="100px" height="100px"></div>
                                    <div>Manage Inquires</div>
                                    <div style="height: 20px"></div>

                                    <%                                        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                        Criteria cri = ses.createCriteria(POJO.Inqu.class);
                                        cri.add(Restrictions.eq("stat", "1"));
                                        List<POJO.Inqu> l = cri.list();
                                        int inc = 0;
                                        
                                        if (!l.isEmpty()) {
                                            for (POJO.Inqu i : l) {
                                                ++inc;
                                            }
                                        } else {
                                            System.out.print("empty");
                                        }

                                    %>
                                    <span class="notification red"><%=inc%></span>

                                </a>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <hr>
            <footer>
                <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>
            </footer>
        </div>

    </body>
</html>
<%        }
        
    } else {
        response.sendRedirect("index.jsp");
    }

%>