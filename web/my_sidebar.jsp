<%-- 
    Document   : my_sidebar
    Created on : Mar 1, 2016, 1:33:29 PM
    Author     : HP
--%>

<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div id="sidebar" class="left">
            <div class="sidebar_box"><span class="bottom"></span>
                <h3>Discounted</h3>   
                <div class="content"> 
                    <ul class="sidebar_list">
                        <%
                            Session hid = Connections.NewHibernateUtil.getSessionFactory().openSession();
                            Criteria cat = hid.createCriteria(POJO.Catogory.class);
                            POJO.DeleteStatus dels = (POJO.DeleteStatus) hid.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                            cat.add(Restrictions.eq("deleteStatus", dels));
                            List<POJO.Catogory> ml = cat.list();
                            for (POJO.Catogory cc : ml) {
                        %>
                        <li><a onclick="idk('<%=cc.getId()%>')"><%=cc.getCatogory()%></a></li>
                            <%}%>

                    </ul>
                    <script type="text/javascript">
                        function idk(idm) {
                            //alert(idm);
                            var ur = "my_Discounted.jsp?idf=" + idm;
                            window.location.replace(ur);

                        }
                    </script>
                </div>
            </div>
            <div class="sidebar_box" ><span class="bottom"></span>
                <h3>Weekly Special</h3>   
                <div class="content special">
                    <%
                        Session pop = Connections.NewHibernateUtil.getSessionFactory().openSession();
                        POJO.DeleteStatus dest = (POJO.DeleteStatus) pop.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                        Criteria pop_crt = pop.createCriteria(POJO.Weekly.class);
                        pop_crt.addOrder(Order.desc("id"));
                        List<POJO.Weekly> we = pop_crt.list();
                        DecimalFormat dfhk = new DecimalFormat("0.00");
                        loops:
                        for (POJO.Weekly weekl : we) {
                            double dec_dd = Double.parseDouble(weekl.getProducts().getUnitPrice());
                            if ((weekl.getDeleteStatus().equals(dest)) && (weekl.getProducts().getDeleteStatus().equals(dest)) ) {
                    %>
                    <a onclick="detail('<%=weekl.getProducts().getId()%>')">

                        <img src='<%=weekl.getProducts().getImg()%>' alt="Flowers" width="212px" height="230px"/>
                        <span><h4><%=weekl.getProducts().getName()%></h4></span>
                        <br>
                        <p>
                            Price:
                            <span class="price  normal_price"><%=dfhk.format(dec_dd)%></span>&nbsp;&nbsp;

                            <%
                                double doub = Double.parseDouble(weekl.getProducts().getPromotionn().getDiscountPersentage()) * dec_dd / 100;
                                double myd = dec_dd - doub;
                            %>
                            <span class="price special_price"><%=dfhk.format(myd)%></span>
                        </p>

                        <%
                            }

                            break loops;

                        }
                        %>
                    </a>
                </div>
                <script type="text/javascript">

                    function detail(id) {
                        var i = id;
                        var url = "my_product_details.jsp?id=" + i;
                        window.location.replace(url);

                    }
                </script>
            </div>
        </div>
    </body>
</html>
