<%-- 
    Document   : admin_leftBar
    Created on : Mar 14, 2016, 4:54:07 PM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="span2 main-menu-span" style="margin-top: 5%">
        <div class="well nav-collapse sidebar-nav">
            <ul class="nav nav-tabs nav-stacked main-menu">

                <li class="nav-header hidden-tablet">Product Management</li>
                <li><a class="ajax-link" href="admin_ad_product.jsp"><i class="icon-folder-open"></i><span class="hidden-tablet"> Add New Product</span></a></li>
                <li><a class="ajax-link" href="admin_search_product.jsp"><i class="icon-eye-open"></i><span class="hidden-tablet"> Search Product</span></a></li>
                <li><a class="ajax-link" href="admin_edit_product.jsp"><i class="icon-edit"></i><span class="hidden-tablet"> Edit Product</span></a></li>
                <li><a class="ajax-link" href="admin_weekly_special.jsp"><i class="icon-th"></i><span class="hidden-tablet">Weekly Special</span></a></li>
                <li><a class="ajax-link" href="admin_ad_category.jsp"><i class="icon-folder-open"></i><span class="hidden-tablet"> Add New Category</span></a></li>
                <li><a class="ajax-link" href="admin_ad_promotion.jsp"><i class="icon-folder-open"></i><span class="hidden-tablet"> Add New Promotion</span></a></li>
                <li class="nav-header hidden-tablet">User Management</li>
                <li><a class="ajax-link" href="admin_user_add.jsp"><i class="icon-edit"></i><span class="hidden-tablet">Manage User</span></a></li>
                <li class="nav-header hidden-tablet">Order Management</li>
                <li><a class="ajax-link" href="admin_order_mng.jsp"><i class="icon-edit"></i><span class="hidden-tablet">Manage Order</span></a></li>
                <li class="nav-header hidden-tablet">Inquiries</li>
                <li><a class="ajax-link" href="admin_manage_inqu.jsp"><i class="icon-edit"></i><span class="hidden-tablet">Manage Inquiry</span></a></li>
            </ul>
        </div><!--/.well -->
    </div><!--/span-->
</html>
