<%-- 
    Document   : admin_deleted_activity
    Created on : Jul 7, 2016, 2:09:04 PM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link rel="shortcut icon" href="img/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    </head>
    <body>


        <%
            
            POJO.UserReg use=null;
            String shwform = request.getParameter("pid");
            Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
            if (shwform != null) {
                 use = (POJO.UserReg) ses.load(POJO.UserReg.class, Integer.parseInt(shwform));
            }
        %>
        <form class="form-horizontal" method="post" action="" style="margin-right: 50px">

            <table class="table  table-bordered bootstrap-datatable datatable" style="margin-right: 50px;color: black;margin-top: 10%;margin-left: 5%">
                <thead>
                    <tr>
                        <th>Invoice Id</th>
                        <th> Invoice Date</th>
                        <th>User Id</th>
                        <th>Delivery Date</th>
                        <th>Sub Total</th>
                        <th>Delivery Status</th>
                    </tr>
                </thead>   
                <tbody>
                    <%                        Criteria crt3 = ses.createCriteria(POJO.Invoice.class);
                        crt3.add(Restrictions.eq("userReg", use));
                        List<POJO.Invoice> in = crt3.list();
                        for (POJO.Invoice i : in) {


                    %>
                    <tr>
                        <td><%=i.getId()%></td>
                        <td><%= i.getDate()%></td>
                        <td><%=i.getUserReg().getId()%></td>
                        <td><%=i.getDeliveryInfo().getDate()%></td>
                        <td><%=i.getSubtot()%></td>
                        <td><%= i.getDeliveryStatus().getStatus()%></td>
                    </tr>
                    <%    }
                    %>
                </tbody>
            </table>
        </form> 
        
    </body>
</html>
