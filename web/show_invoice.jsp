<%-- 
    Document   : show_invoice
    Created on : Oct 13, 2016, 7:12:05 PM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" src="js/jquery.min.js"></script>
    </head>
    <body>
        <script>
            $(document).ready(function () {
                PrintElem('#a');
              
               
            });
            function PrintElem(elem)
            {
                //                Popup($('<div/>').append($(elem).clone()).html());
                Popup($(elem).html());
            }

            function Popup(data)
            {

                var headstr = "<html><head><title>Booking Details</title></head><body>";
                var footstr = "</body></html>";
                var newstr = document.getElementById('a').innerHTML;
                var oldstr = document.body.innerHTML;
                document.body.innerHTML = headstr + newstr + footstr;
                window.print();
                document.body.innerHTML = oldstr;
                return false;
            }
        </script>
        <!-- topbar start -->
        <!-- topbar ends -->
        <div class="container-fluid" id="a">

            <div class="row-fluid">
                <!-- left menu starts -->
                <!-- left menu ends -->
                <div id="content" class="span10">
                    <!-- content starts -->

                    <div class="row-fluid sortable" style="">
                        &nbsp;<br>
                        &nbsp;
                        <div class="box span12">
                            <div class="box-header well" data-original-title>
                            </div>

                            <div class="box-content">
                                <div class="dataTables_filter" id="DataTables_Table_0_filter"><label>
                                        <table>
                                            <tr>
                                                <!--                                                <td><img src="images/flower_b.png" width="200px" height="100px"></td>-->
                                                <td><h1 style="margin-left: 250%">Invoice</h1></td>
                                            </tr>
                                        </table>

                                        <%

                                            //POJO.Invoice invo = (POJO.Invoice) request.getSession().getAttribute("Invoice");
                                            String id=request.getParameter("idd");
                                            Session myc = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                            DecimalFormat kdf = new DecimalFormat("0.00");

                                            POJO.Invoice invo=(POJO.Invoice)myc.load(POJO.Invoice.class,Integer.parseInt(id));
                                            if (invo != null) {
                                        %>
                                        &nbsp;<br>


                                        <table style="margin-left:70%">
                                            <tr>
                                                <td>Date</td>
                                                <td>-</td>
                                                <td><%=invo.getDate()%></td>
                                            </tr>
                                            <tr>
                                                <td>Invoice ID</td>
                                                <td>-</td>
                                                <td><%=invo.getId()%></td>
                                            </tr>
                                            <tr>
                                                <td>Customer ID</td>
                                                <td>-</td>
                                                <td><%=invo.getUserReg().getId()%></td>
                                            </tr>

                                            <tr>
                                                <td>Order Due Date</td>
                                                <td>-</td>
                                                <td><%=invo.getDeliveryInfo().getDate()%></td> 
                                            </tr>
                                        </table>


                                </div>              
                                <hr>

                                <table style="width: 70%;margin-left: 12%">
                                    <thead>
                                        <tr>
                                            <td><h4>Product Id</h4></td>
                                            <td><h4>Name</h4></td>
                                            <td><h4>Unit Price</h4></td>
                                            <td><h4>Discount&nbsp;%</h4></td>
                                            <td><h4>Quantity</h4></td>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <%
                                            Criteria crtr = myc.createCriteria(POJO.PayedItems.class);
                                            crtr.add(Restrictions.eq("invoice", invo));
                                            List<POJO.PayedItems> pyd = crtr.list();
                                            for (POJO.PayedItems p : pyd) {
                                                double mydou = Double.parseDouble(p.getProducts().getUnitPrice());
                                        %>
                                        <tr>
                                            <td class="center"><%= p.getProducts().getId()%></td>
                                            <td class="center"><%=p.getProducts().getName()%></td>
                                            <td class="center"><%=kdf.format(mydou)%></td>
                                            <td class="center"><%=p.getProducts().getPromotionn().getDiscountPersentage()%></td>
                                            <td class="center"><%= p.getQty()%></td>
                                        </tr>

                                        <%
                                            }

                                        %>
                                    </tbody>
                                </table>  

                                <hr>
                                &nbsp;<br>

                                <%                                        double du = Double.parseDouble(invo.getSubtot());
                                    double dud = Double.parseDouble(invo.getDeliveryInfo().getRecipientInfo().getDeliveryCity().getCharge());
                                    double prs = du - dud;
                                %>


                                <div style="margin-left: 12%">  
                                    <table>
                                        <tr>
                                            <td>
                                                <h3>Shipping Address</h3>&nbsp;<br>
                                            </td>
                                            <td style="width: 20px"></td>
                                            <td>
                                                <h3>Billing Details</h3>&nbsp;<br>
                                            </td>
                                            <td style="width: 20px"></td>
                                            <td>
                                                <h3></h3>&nbsp;<br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><%=invo.getDeliveryInfo().getRecipientInfo().getTitle()%>.&nbsp; <%=invo.getDeliveryInfo().getRecipientInfo().getName()%></td>
                                            <td style="width: 20px"></td>
                                            <td><%=invo.getDeliveryInfo().getSendersInfo().getTitle()%>.&nbsp;<%=invo.getDeliveryInfo().getSendersInfo().getName()%></td>
                                            <td style="width: 45px"></td>
                                            <td>Item Total</td>
                                            <td>Rs.</td>
                                            <td><%=prs%></td>

                                        </tr>
                                        <tr>
                                            <td><%=invo.getDeliveryInfo().getRecipientInfo().getAddress()%></td>
                                            <td style="width: 20px"></td>
                                            <td><%=invo.getDeliveryInfo().getSendersInfo().getEmail()%></td>
                                            <td style="width: 45px"></td>
                                            <td>Delivery Chargers </td>
                                            <td>Rs.</td>
                                            <td><%=kdf.format(dud)%></td>
                                        </tr>
                                        <tr>
                                            <td><%=invo.getDeliveryInfo().getRecipientInfo().getAddress2()%></td>
                                            <td style="width: 20px"></td>
                                            <td><%=invo.getDeliveryInfo().getSendersInfo().getPhoneNum()%></td>
                                            <td style="width: 45px"></td>
                                            <td>Discount</td>
                                            <td>Rs.</td>
                                            <td>0</td>

                                        </tr>

                                        <tr>
                                            <td><%=invo.getDeliveryInfo().getRecipientInfo().getDeliveryCity().getCity()%></td>
                                            <td style="width: 20px"></td>
                                            <td style="width: 40px"></td>
                                            <td style="width: 45px"></td>
                                            <td>Total</td>
                                            <td>Rs.</td>
                                            <td><%=kdf.format(du)%></td>

                                        </tr>
                                        <tr>
                                            <td><%=invo.getDeliveryInfo().getRecipientInfo().getPhoneNum()%></td>
                                            <td style="width: 20px"></td>
                                            <td style="width: 45px"></td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <p><h4> Should you have any inquiries concerning this invoice  , please contact us on Our Hot Line Number <br>
                                        &nbsp;
                                        <p style="margin-left:60%">

                                            Madhu Flora ,<br>
                                            151/D ,<br>
                                            Dickman's Road ,<br>
                                            Colombo 4 .<br>
                                            T.P. 0112184518 .
                                        </p>
                                    </h4>
                                    </p>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <%    }
                %>
            </div>

        </div>

    </body>
</html>
