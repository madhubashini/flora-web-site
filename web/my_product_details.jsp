<%-- 
    Document   : my_product_details
    Created on : Dec 23, 2015, 11:06:38 AM
    Author     : HP
--%>

<%@page import="org.hibernate.criterion.Projections"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Product_Details</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">


        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>      
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>        
        <script type="text/javascript" src="js/my_javascripts.js"></script> 

    </head>
    <body onload="status()">

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#">Floral Shop</a></div>

                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->
            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">

                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">



                        <form action="" class="contact" style="width: 685px">


                            <fieldset class="contact-inner">
                                <p>
                                <h1 style="color: black;margin-left:210px"> Product Details</h1>
                                </p>
                                <br>
                                <%                                    String id = request.getParameter("id");
                                    String stat = null;
                                    String promo = null;
                                    //out.print(id);
                                    Session sess = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                    Criteria cri = sess.createCriteria(POJO.Products.class);

                                    if (id != null) {
                                        cri.add(Restrictions.eq("id", new Integer(id)));

                                        List<POJO.Products> list = cri.list();

                                        for (POJO.Products pro : list) {
                                            stat = pro.getPromotionn().getDeleteStatus().getStatus();
                                            promo = pro.getPromotionn().getId() + "";

                                %>
                                <table>
                                    <tr>
                                        <td>
                                            <img src="<%=pro.getImg()%>" alt="aaaaa" width="200px" height="200px"/>
                                        </td>
                                        <td>
                                            <table style="margin-left:140px">
                                                <tr>
                                                    <td><h4 style="color: black">Price</h4></td>
                                                    <td><h4 style="color: black">:</h4></td>
                                                    <td><h4 style="color: black"><%=pro.getUnitPrice()%></h4></td>
                                                </tr>

                                                <tr>
                                                    <td><h4 style="color: black">Availability</h4></td>
                                                    <td><h4 style="color: black">:</h4></td>
                                                    <td><h4 style="color: black">On Stock</h4></td>
                                                </tr>
                                                <tr>
                                                    <td><h4 style="color: black">Discount</h4></td>
                                                    <td><h4 style="color: black">:</h4></td>
                                                    <td><h4 style="color: black"><%=pro.getPromotionn().getDiscountPersentage()%>%</h4></td>
                                                </tr>
                                                <tr>
                                                    <td id="msg" class="hideitems"><h4 style="color:red">Discount Is Out Of Date</h4></td>

                                                </tr>
                                                <script type="text/javascript">
                                                    var s = '<%=stat%>';
                                                    var p = '<%=promo%>';
                                                    if (p != "1") {
                                                        if (s == 'Inactive') {
                                                            $('#msg').show();
                                                        }
                                                    }
                                                </script>

                                                <tr>
                                                    <td><h4 style="color: black">Quantity </h4></td>
                                                    <td><h4 style="color: black">:</h4></td>
                                                    <td><h4 style="color: black">
                                                            <select id="quen">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </h4>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="number_error" class="hideitems"><h4 style="color:red">Number Format Exception</h4></td>
                                                    <td id="length" class="hideitems"><h4 style="color:red">Invalid Quantity</h4></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <p>
                                <h4 style="color: black">Product Description : &nbsp; <%=pro.getName()%></h4>
                                <p><h5 style="color: black"><%=pro.getDescription()%></h5></p>
                                <br>
                                <p class="contact-submit">
                                    <a  onclick="gocart('<%=pro.getId()%>')" class="New-lassana-btn">Add to cart<img style="vertical-align: middle" src="images/index.png"></a>
                                </p>
                            </fieldset>
                            <%
                                    }

                                } else {

                                    response.sendRedirect("index.jsp");
                                }

                            %>
                            <script type="text/javascript">
                                function gocart(getId) {
                                    var miid = getId;
                                    var qty = $('#quen').find('option:selected').text();
                                    var url = "my_cart.jsp?idd=" + miid + "&qty=" + qty;
                                    $.post(
                                            "add_to_cart",
                                            {id: miid, val: qty},
                                    function (se) {
                                        if (se == 1) {
                                            window.location.replace(url);
                                        }
                                        if(se == 88){
                                          $('#number_error').show(); 
                                        }
                                        if(se == 99){
                                          $('#length').show(); 
                                        }
                                    }
                                    );
                                }
                            </script>
                        </form>
                        <%                                sess.close();
                        %>
                    </div>
                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>

    </body>
</html>
