<%-- 
    Document   : Over_five_items
    Created on : Jul 12, 2016, 3:58:59 PM
    Author     : HP
--%>


<%
    Session new_ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
    POJO.UserReg usr = null;
    String uid = null;
    if (request.getSession().getAttribute("login") != null) {
        usr = (POJO.UserReg) request.getSession().getAttribute("login");
        uid = usr.getId() + "";
    } else {
        response.sendRedirect("User_login.jsp");
    }

%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.hibernate.Session"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/loginpage.css">

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>      
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>        
        <script type="text/javascript" src="js/my_javascripts.js"></script>

        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/jquery-ui.js"></script>

    </head>
    <body>


        <style>
            #templatemo_menu1 a{
                color: darkorange;
            }
        </style>
        <style>
            .suggetionbox{
                background: #9E9E9E;
            }
            .focusedtotf{
                width: 50%;
                background: #9E9E9E;
                position: absolute;
                z-index: 1;
            }
            .focusedtotf p{
                color: black;
            }

        </style>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>
                    <%@include file="my_header.jsp" %>
                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>
                    <div id="content" class="right">

                        <div class="form" style="">

                            <div>
                                <div id="loginss">
                                    <h1 style="margin-left:">Inquire From Administrator</h1>
                                    <div class="field-wrap">
                                        <span>
                                            <input type="text" id="searchdebo"  placeholder="Search By name" style="width: 50%;float: right">
                                        </span>

                                        <div id="outerofsu" class="focusedtotf" style="display: none;margin-top: 5%;margin-left: 50%; z-index: 9;width: 50%">
                                            <p style="margin-left:5%">Suggession</p>
                                            <div id="loadeddat" class="suggetionbox">
                                                <p style="color:red;margin-left:5%">None</p>
                                            </div>
                                        </div>
                                        <br>
                                        <script>
                                            $("#searchdebo").mouseenter(function () {
                                                $("#outerofsu").show();
                                            });
                                            $("#searchdebo").mouseleave(function () {
                                                $("#outerofsu").hide();
                                            });
                                            $("#outerofsu").mouseover(function () {
                                                $("#outerofsu").show();
                                            });
                                            $("#outerofsu").mouseleave(function () {
                                                $("#outerofsu").hide();
                                            });



                                            $("#searchdebo").keyup(function (event) {

                                                $("#loadeddat").html('');

                                                if ($("#searchdebo").val() != "") {

                                                    var word = $("#searchdebo").val();
                                                    $.post(
                                                            "searchbykey_over5",
                                                            {keyword: word},
                                                    function (result) {

                                                        var json = JSON.parse(result);

                                                        for (i in json) {
//                                                            alert(i+","+json[i])
                                                            $("#loadeddat").append(' <a style="color:black;margin-left:5%"  href="Over_five_items.jsp?idd=' + i + '"> ' + json[i] + '</a><br>');
                                                        }
                                                    });
//                                                   
                                                } else {
                                                    $("#loadeddat").append('<p style="color:red">None</p>');

                                                }
                                            });
                                        </script>
                                    </div>

                                    <%                                       // out.print(request.getParameter("idd"));
                                        String nameid = request.getParameter("idd");
                                        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                        POJO.Products prod = null;

                                        if (nameid != null) {
                                            prod = (POJO.Products) ses.load(POJO.Products.class, Integer.parseInt(nameid));
                                        }
                                        if (prod != null) {
                                    %>
                                    <br>
                                    <img style="width: 200px;height: 200px;margin-left: 30%" src='<%=prod.getImg()%>'>
                                    <br>
                                    <p><h3 style="color:white">Please Send Inquire Least Before 4 Days From Inquire Date !</h3> </p>
                                    <br>
                                    <div class="field-wrap">
                                        <p style="color: white"> Item Name -</p><input type="" id="na"  placeholder=""  value='<%=prod.getName()%>'>
                                    </div>
                                    <div class="field-wrap">
                                        <p style="color: white">Inquire Quantity -</p><input type="number" id="qty"  placeholder="">
                                    </div>
                                    <div class="field-wrap">
                                        <p style="color: white">Inquire Date -</p><input type="" id="datepicker"  placeholder="">
                                    </div>


                                    <%
                                    } else {
                                    %>
                                    <br>
                                    <img style="width: 150px;height: 150px;margin-left: 30%">
                                    <br>
                                    <br>
                                    <div class="field-wrap">
                                        <p style="color: white">Item Name -</p>  <input type="" id=""  placeholder="" >
                                    </div>
                                    <div class="field-wrap">
                                        <p style="color: white">Inquire Quantity -</p> <input type="" id=""  placeholder="">
                                    </div>
                                    <div class="field-wrap">
                                        <p style="color: white">Inquire Date -</p>  <input type="" id=""  placeholder="">
                                    </div>

                                    <%
                                        }
                                    %>


                                    <ul class="tab-group" style="margin-left: 30%">
                                        <li><a  onclick="serv()" class="tab">Send</li>  
                                    </ul>
                                    <script>
                                        function serv() {
                                            //alert("aaa");
                                            var us = '<%=uid%>';
                                            var it = '<%=nameid%>';
                                            var qua = document.getElementById("qty").value;
                                            var date = document.getElementById("datepicker").value;

                                            $.post(
                                                    "inquire",
                                                    {us: us, ite: it, qa: qua, dat: date},
                                            function (result) {
                                                alert("Succesfully Send");
                                                
                                                
                                            }
                                            );


                                        }
                                    </script>
                                    <br><br>
                                    <script>

                                        $(document).ready(
                                                function () {
                                                    var date = new Date();
                                                    var d = new Date(date.setTime(date.getTime() + 2 * 86400000));
                                                    $("#datepicker").datepicker({
                                                        minDate: d,
                                                        changeMonth: true, //this option for allowing user to select month
                                                        changeYear: false //this option for allowing user to select from year range
                                                    });
                                                }

                                        );

                                    </script>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>                             

        <!-- END of main wrapper -->

        <!--  
        
        -->
    </body>
</html>
