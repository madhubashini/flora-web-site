<%-- 
    Document   : change_billing
    Created on : Jun 10, 2016, 2:50:22 PM
    Author     : HP
--%>
<%
    Session adsession = Connections.NewHibernateUtil.getSessionFactory().openSession();

    POJO.UserReg u = (POJO.UserReg) request.getSession().getAttribute("login");

    if (u != null) {
        POJO.UserReg ur = (POJO.UserReg) adsession.load(POJO.UserReg.class, u.getId());
        if (ur.getUserStatus().getId() == 1) {

%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/loginpage.css">

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>      
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>        
        <script type="text/javascript" src="js/my_javascripts.js"></script>

    </head>
    <body>

        <%            String id = request.getParameter("codeI");
            //out.print(id);
        %>


        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <br>
                        <br>
                        <div class="form">

                            <div class="tab-content">

                                <h1>Change Billing Details!</h1>

                                <form id="changebilling">
                                    <div class="field-wrap">
                                        <select class="madish-sele" id="sel"  autofocus>
                                            <option  value="0" selected="se">Select Title</option>
                                            <option value="1">Mr</p></option>
                                            <option value="1">Ms</option>
                                        </select>
                                        <p style="color: white" id="ti_emp" class="hideitems">Select Title</p>
                                    </div>
                                    <div class="field-wrap">
                                        <input type="text" id="nam"  placeholder="Name" autofocus>
                                    </div>
                                    <p style="color: white" id="nam_emo" class="hideitems">Name can't be empty</p>
                                    <div class="field-wrap">
                                        <input type="text" id="ema"  placeholder="E-mail">
                                    </div>
                                    <p style="color: white" id="em_emp" class="hideitems">E-mail can't be empty</p>
                                    <p style="color: white" id="em_invald" class="hideitems"> Invalid E-mail</p>
                                    <div class="field-wrap">
                                        <input type="tel" id="ph"  placeholder="Phone number">
                                    </div>
                                    <p style="color: white" id="phn_emp" class="hideitems">Phone number can't be empty</p>
                                    <p style="color: white" id="phn_len" class="hideitems">Invalid phone number</p>
                                    <br><br>
                                    <ul class="tab-group">
                                        <li><a onclick="nex('<%=id%>')" class="tab">Change</a></li>   
                                    </ul>

                                    <p style="color: white" id="changed" class="hideitems">Successfully Changed</p>


                                </form>


                            </div>

                        </div>
                    </div>
                </div>

                <script type="text/javascript">

                    function nex(code) {
                        var cod = code;
                        var tit = $('#sel').find('option:selected').val();
                        var titl = $('#sel').find('option:selected').text();
                        var nam = document.getElementById("nam").value;
                        var em = document.getElementById("ema").value;
                        var phn = document.getElementById("ph").value;
                        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                        if (tit == 1) {
                            title = true;
                        } else {
                            $('#ti_emp').show();
                            title = false;
                        }

                        if (nam == "") {
                            $('#nam_emo').show();
                            namefil = false;
                        } else {
                            namefil = true;
                        }

                        if (em == "") {
                            $('#em_emp').show();
                            emlfil = false;
                        } else {
                            emlfil = true;

                            if (!(filter.test(em))) {
                                $('#em_invald').show();
                                emch = false;
                            } else {
                                emch = true;
                            }

                        }
                        var phonefil = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/;
                        if (phn == "") {
                            $('#phn_emp').show();
                            phnemp = false;
                        } else {
                            phnemp = true;

                            if (!(phonefil.test(phn))) {
                                $('#phn_len').show();
                                phnlen = false;
                            } else {
                                phnlen = true;
                            }

                        }

                        if (title == true && namefil == true && emlfil == true && emch == true && phnemp == true && phnlen == true) {
                            // alert("awaaa");

                            $.post(
                                    "change_bill",
                                    {code: cod, tit: titl, name: nam, ema: em, ph: phn},
                            function (result) {
                                $('#changed').show();
                                document.getElementById("changebilling").reset();

                            }

                            );

                        }

                    }

                </script>


                <div class="cleaner h20"></div>                       
                <div class="cleaner h20"></div>

            </div>

            <div class="cleaner"></div>
        </div> <!-- END of main -->
    </div> <!-- END of main wrapper -->

    <%@include file="my_footer.jsp" %>
</div>                             

<!-- END of main wrapper -->

<!--  

-->
</body>
</html>
<%//
        } else {
            request.getSession().invalidate();
            response.sendRedirect("index.jsp");
        }
    } else {
        response.sendRedirect("User_login.jsp");
    }
%>