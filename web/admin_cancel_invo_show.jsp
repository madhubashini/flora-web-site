<%-- 
    Document   : admin_cancel_invo_show
    Created on : Jun 30, 2016, 6:46:38 PM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/fullcalendar.css' rel='stylesheet'>
        <link href='admin_css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link href='admin_css/uploadify.css' rel='stylesheet'>
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <link rel="shortcut icon" href="img/favicon.ico">
    </head>
    <body>

        <%
            Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
            String id = request.getParameter("id");
            //System.out.print(id);
            if (id != null) {
                Criteria crt = ses.createCriteria(POJO.Invoice.class);
                crt.add(Restrictions.eq("id", Integer.parseInt(id)));
                POJO.Invoice invo = (POJO.Invoice) crt.uniqueResult();

        %>

        <form class="form-horizontal" method="post" action="" style="margin-left: 10px">
            <table>
                <tr>
                    <td><label>Invoice Id</label></td>
                    <td style="width: 10%"></td>
                    <td>-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=id%></label></td>
                </tr>
                <tr>
                    <td><label>User Id</label></td>
                    <td style="width: 10%"></td>
                    <td>-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=invo.getUserReg().getId()%></label></td>
                </tr>
                <tr>
                    <td><label>Delivery Date</label></td>
                    <td style="width: 10%"></td>
                    <td>-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=invo.getDeliveryInfo().getDate()%></label></td>
                </tr>
                <tr>
                    <td><label>Message</label></td>
                    <td style="width: 10%"></td>
                    <td>-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=invo.getDeliveryInfo().getMessage()%></label></td>
                </tr>
                <tr>
                    <td><label>Instructions</label></td>
                    <td style="width: 10%"></td>
                    <td>-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=invo.getDeliveryInfo().getInstruction()%></label></td>
                </tr>
                <tr>
                    <td style="vertical-align: super"><label>Shipping Details</label></td>
                    <td style="width: 10%"></td>
                    <td style="vertical-align: super">-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=invo.getDeliveryInfo().getRecipientInfo().getTitle()%> &nbsp;<%=invo.getDeliveryInfo().getRecipientInfo().getName()%></label>
                        <label><%= invo.getDeliveryInfo().getRecipientInfo().getAddress()%></label>
                        <label><%= invo.getDeliveryInfo().getRecipientInfo().getAddress2()%></label>
                        <label><%=invo.getDeliveryInfo().getRecipientInfo().getDeliveryCity().getCity()%></label>
                    </td>
                </tr>
                <tr>
                    <td><label>Location Type</label></td>
                    <td style="width: 10%"></td>
                    <td>-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=invo.getDeliveryInfo().getRecipientInfo().getLocationType()%></label></td>
                </tr>
                <tr>
                    <td><label>Receiver's Contact Number </label></td>
                    <td style="width: 10%"></td>
                    <td>-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=invo.getDeliveryInfo().getRecipientInfo().getPhoneNum()%></label></td>
                </tr>
                <tr>
                    <td style="vertical-align: super"><label>Billing Details</label></td>
                    <td style="width: 10%"></td>
                    <td style="vertical-align: super">-</td>
                    <td style="width: 10%"></td>
                    <td>
                        <label><%=invo.getDeliveryInfo().getSendersInfo().getTitle()%>&nbsp;<%=invo.getDeliveryInfo().getSendersInfo().getName()%></label>
                        <label><%=invo.getDeliveryInfo().getSendersInfo().getEmail()%></label>
                    </td>
                </tr>
                <tr>
                    <td><label>Sender's Contact Number</label></td>
                    <td style="width: 10%"></td>
                    <td>-</td>
                    <td style="width: 10%"></td>
                    <td><label><%=invo.getDeliveryInfo().getSendersInfo().getPhoneNum()%></label></td>
                </tr>
                <tr>
                    <td><label>Ordered Items</label></td>
                    <td style="width: 10%"></td>
                    <td></td>
                </tr>
            </table>
            <br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Item Name</th>
                        <th>Item ID</th>
                        <th>Quantity</th>
                    </tr> 
                </thead>
                <tbody>
                    <%
                        Criteria cr1 = ses.createCriteria(POJO.PayedItems.class);
                        cr1.add(Restrictions.eq("invoice", invo));
                        List<POJO.PayedItems> payli = cr1.list();
                        for (POJO.PayedItems p : payli) {

                    %>

                    <tr>
                        <td><label><%=p.getProducts().getName()%></label></td>
                        <td><%=p.getProducts().getId()%></td>
                        <td><label><%=p.getQty()%></label></td>


                    </tr>
                    <%
                            
                        }
                    %>
                </tbody>
            </table>

            <%
                POJO.DeliveryStatus del = (POJO.DeliveryStatus) ses.load(POJO.DeliveryStatus.class, Integer.parseInt("3"));
                if (invo.getDeliveryStatus().equals(del)) {
                    Criteria crtt = ses.createCriteria(POJO.RemoveDescrpt.class);
                    crtt.add(Restrictions.eq("invoice", invo));
                    POJO.RemoveDescrpt de = (POJO.RemoveDescrpt) crtt.uniqueResult();
            %>
            <table>
                <tr style="color: red">
                    <td style="vertical-align: super;font-size: 13px">Reason for cancel order</td>
                    <td style="width: 16%"></td>
                    <td style="vertical-align: super">-</td>
                    <td style="width:35px"></td>
                    <td style="font-size: 13px"><%=de.getDscpt()%></td>
                </tr>
            </table>

            <%                             }
                }
            %>
        </form>

        <footer>
<!--            <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>-->
        </footer>
    </div>

</body>
</html>
