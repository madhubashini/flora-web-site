<%-- 
    Document   : my_userPro_details
    Created on : Mar 7, 2016, 10:21:16 AM
    Author     : HP
--%>

<%
    Session v1 = Connections.NewHibernateUtil.getSessionFactory().openSession();

    POJO.UserReg u = (POJO.UserReg) request.getSession().getAttribute("login");

    if (u != null) {
        POJO.UserReg urt = (POJO.UserReg) v1.load(POJO.UserReg.class, u.getId());
        if (urt.getUserStatus().getId() == 1) {

%>


<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>prof2</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>       
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="js/my_javascripts.js"></script> 
    </head>
    <body>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>
                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>


                    <div id="content" class="right">    

                        <form action="" class="contact" style="width: 685px;height: auto">



                            <fieldset class="contact-inner"style="height:auto">
                                <p>
                                <h1 style="color: black;margin-left:160px">Order Details</h1>
                                </p>
                                <br>
                                <table>
                                    <tr style="margin-left: 5px">
                                        <td><h4 style="color: black">Ordered Items</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                    </tr>
                                </table>
                                <table class="table1" style="width: 75%">
                                    <thead>
                                        <tr>
                                            <th><h4 style="color: black">Image</h4></th>
                                    <th><h4 style="color: black">Item Name</h4></th>
                                    <th><h4 style="color: black">Quantity</h4></th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                        <%                                            String id = request.getParameter("id");
                                            Criteria c4 = v1.createCriteria(POJO.PayedItems.class);
                                            POJO.Invoice in = (POJO.Invoice) v1.load(POJO.Invoice.class, Integer.parseInt(id));
                                            c4.add(Restrictions.eq("invoice", in));
                                            double subt = Double.parseDouble(in.getSubtot());
                                            List<POJO.PayedItems> l = c4.list();

                                        %>

                                        <%                                            for (POJO.PayedItems p : l) { 
                                        %>
                                        <tr>
                                            <td> <h4 style="color: black"><img src='<%=p.getProducts().getImg()%>' style="width:50px;height: 50px"></h4></td>
                                            <td> <h4 style="color: black"><%=p.getProducts().getName()%></h4></td>
                                            <td> <h4 style="color: black"><%=p.getQty()%></h4></td>
                                        </tr>

                                        <%}%>


                                    </tbody>
                                </table>
                                <br>

                                <table style="margin-left: 10px">
                                    <tr>
                                        <td><h4 style="color: black">Shipped Status</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color:red"><%=in.getDeliveryStatus().getStatus()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4 style="color: black">Recipient's Name</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=in.getDeliveryInfo().getRecipientInfo().getName()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4 style="color: black">Recipient's Contact Number</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=in.getDeliveryInfo().getRecipientInfo().getPhoneNum()%></h4></td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black">Recipient's Address</h4></td>
                                        <td><h4 style="color: black"> :</h4></td>
                                        <td><h4 style="color: black"><%=in.getDeliveryInfo().getRecipientInfo().getAddress()%>&nbsp;&nbsp;
                                                <%=in.getDeliveryInfo().getRecipientInfo().getAddress2()%>
                                                &nbsp;&nbsp;<%=in.getDeliveryInfo().getRecipientInfo().getDeliveryCity().getCity()%>
                                            </h4>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black">Delivery Date & Time</h4></td>
                                        <td><h4 style="color: black"> :</h4></td>
                                        <td><h4 style="color: black"> <%=in.getDeliveryInfo().getDate()%> &nbsp;&nbsp;<%=in.getDeliveryInfo().getTime()%>  </h4></td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black">Instruction</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=in.getDeliveryInfo().getInstruction()%></h4></td>
                                    </tr>   
                                    <tr>
                                        <td><h4 style="color: black">Message</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=in.getDeliveryInfo().getMessage()%></h4></td>
                                    </tr> 

                                    <tr>
                                        <td><h4 style="color: black">Sender's Name & Email</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=in.getDeliveryInfo().getSendersInfo().getName()%>(<%=in.getDeliveryInfo().getSendersInfo().getEmail()%>)</h4></td>

                                    </tr>
                                    <tr>
                                        <td><h4 style="color: black">Sender's Contact Number</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=in.getDeliveryInfo().getSendersInfo().getPhoneNum()%></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4 style="color: black">Delivery Chargers</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black">Rs.&nbsp;<%=in.getDeliveryInfo().getRecipientInfo().getDeliveryCity().getCharge()%></h4></td>
                                    </tr> 
                                    <tr>
                                        <%

                                            double dd = Double.parseDouble(in.getDeliveryInfo().getRecipientInfo().getDeliveryCity().getCharge());
                                            double prs = subt - dd;
                                            DecimalFormat fdf = new DecimalFormat("0.00");

                                        %>
                                        <td><h4 style="color: black">Price Of Gift Items</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black">Rs.&nbsp;<%=fdf.format(prs)%></h4></td>
                                    </tr>   



                                    <tr>
                                        <td><h4 style="color: black">Total</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black">Rs.&nbsp;<%=fdf.format(subt)%></h4></td>
                                    </tr> 

                                </table>

                            </fieldset>
                        </form>


                    </div>


                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>                
    </body>
</html>
<%//
        } else {
            request.getSession().invalidate();
            response.sendRedirect("index.jsp");
        }
    } else {
        response.sendRedirect("User_login.jsp");
    }
%>