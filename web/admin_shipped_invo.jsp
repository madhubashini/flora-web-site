<%-- 
    Document   : admin_shipped_invo
    Created on : Jun 22, 2016, 12:31:05 PM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link rel="shortcut icon" href="img/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />

        <script src="js/new2.js"></script>
        <script src="js/new_jquery.min.js"></script>
    </head>
    <body>
        <!-- topbar start -->
        <%@include file="admin_header.jsp" %>
        <!-- topbar ends -->
        <div class="container-fluid">

            <div class="row-fluid">
                <!-- left menu starts -->
                <%@include file="admin_leftBar.jsp" %>
                <!-- left menu ends -->
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <p style="font-size: 30px;margin-left: 45%;margin-top: 5%">Shipped Invoices</p>
                    </div>
                    <br>
                    <div class="row-fluid sortable">
                        <div style="width: 1000px">

                            <%
                                Session sesi = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                String reqId = null;
                                if (request.getParameter("ID") != null) {
                                    reqId = request.getParameter("ID");
                                }

                            %>


                            <div class="box-content">
                                <div style="margin-left: 50%">
                                    <div class="dataTables_filter" id="DataTables_Table_0_filter"><label>
                                            Search By  Invoice ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input id="tex" type="number" aria-controls="DataTables_Table_0">&nbsp;&nbsp;&nbsp;
                                            <button type="button"  class="btn btn-primary" onclick="gt()">Search</button>
                                        </label>
                                    </div>
                                </div>
                                <p id="me" class="hide" style="color: red">You Entered Invalid ID</p>
                                <br>
                                <script>
                                    function gt() {
                                        var id = document.getElementById("tex").value;
                                        window.location.replace("admin_shipped_invo.jsp?ID=" + id);
                                    }
                                </script>
                                <hr>
                                <br>
                                <%                                    int index = 0;
                                    int records = 0;
                                    int recordsPerPage = 10;
                                    int proCount = 0;

                                    String realPageIndex = request.getParameter("pageIndx");

                                    if (realPageIndex == null) {
                                        index = 1;
                                    } else {
                                        index = Integer.parseInt(realPageIndex);
                                    }
                                    int result = (index * recordsPerPage) - recordsPerPage;

                                    Criteria crt = sesi.createCriteria(POJO.Invoice.class);

                                    if (reqId != null) {
                                        crt.add(Restrictions.eq("id", Integer.parseInt(reqId)));
                                    }

                                    POJO.DeliveryStatus de = (POJO.DeliveryStatus) sesi.load(POJO.DeliveryStatus.class, Integer.parseInt("2"));
                                    crt.add(Restrictions.eq("deliveryStatus", de));

                                    crt.setFirstResult(result);
                                    crt.setMaxResults(recordsPerPage);

                                    List<POJO.Invoice> lis = crt.list();

                                    if (!lis.isEmpty()) {


                                %>

                                <table class="table table-bordered bootstrap-datatable datatable">
                                    <thead>


                                        <tr>
                                            <th>Invoice ID</th>
                                            <th>Date</th>
                                            <th>User ID</th>
                                            <th>Sub Total Rs.</th>
                                            <th>Delivery Date</th>
                                            <th>Show Invoice</th>
                                            <th></th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <%                                            for (POJO.Invoice i : lis) {
                                        %>
                                        <tr>
                                            <td><%=i.getId()%></td>
                                            <td class="center"><%=i.getDate()%></td>
                                            <td class="center"><%=i.getUserReg().getId()%></td>
                                            <td class="center"><%=i.getSubtot()%></td>
                                            <td class="center"><%=i.getDeliveryInfo().getDate()%></td>

                                            <td class="center">
                                                <button type="button" onclick="ope('<%=i.getId()%>')" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Invoice Detail</button>
                                            </td>
                                            <td>
                                                <a  onclick="inpr('<%=i.getId()%>')" style="text-decoration: underline;color: white">
                                                    <i></i>  
                                                    Print                                           
                                                </a>
                                            </td>
                                        </tr>
                                    <script>
                                        function inpr(id){
                                             window.open("show_invoice.jsp?idd="+id, "Invoice", "status=1,width=860,height=700");
                                        }
                                    </script>
                                        <%
                                            }
                                        %>

                                    </tbody>


                                </table>

                                <%
                                    Criteria c2 = sesi.createCriteria(POJO.Invoice.class);
                                    if (reqId != null) {
                                        c2.add(Restrictions.eq("id", Integer.parseInt(reqId)));
                                    }

                                    POJO.DeliveryStatus des = (POJO.DeliveryStatus) sesi.load(POJO.DeliveryStatus.class, Integer.parseInt("2"));
                                    c2.add(Restrictions.eq("deliveryStatus", des));

                                    List<POJO.Invoice> ll = c2.list();

                                    for (POJO.Invoice prm : ll) {
                                        proCount++;
                                        //out.print(prm.getId());
                                    }
                                    int pages = proCount / 10;
                                    if (proCount > (pages * 10)) {
                                        pages = pages + 1;
                                    }

                                %>
                                <div class="pagination pagination-centered">
                                    <ul>
                                        <%                                            String url = null;
                                            for (int ii = 1; ii <= pages; ii++) {

                                                if (reqId != null) {
                                                    url = "http://localhost:8080/Madhu_Flora_New/admin_shipped_invo.jsp?pageIndx=" + ii + "&ID=" + reqId;
                                                } else {
                                                    url = "http://localhost:8080/Madhu_Flora_New/admin_shipped_invo.jsp?pageIndx=" + ii;
                                                }
                                        %>



                                        <li class="active">
                                            <a href='<%=url%>'><%=ii%></a>
                                        </li>



                                        <%
                                            }
                                        %>
                                    </ul>
                                </div>


                                <%    } else {
                                %>

                                <%
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                .modal.fade.in {
                    top: 38%;
                }
            </style>
            <div class="modal fade" id="myModal" style="overflow: hidden; " role="dialog">
                <div class="modal-dialog">
                    Modal content
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="color: black;margin-left: 30%">View Invoice Details</h4>
                        </div>
                        <div class="modal-body" style="padding: 0px;color: black;height: 550px; overflow: hidden; max-height: 549px">
                            <iframe id="ifram" class="pro-edit-iframe" id="prodetail-if" width="100%" height="100%" scrolling="no" border="0" src="admin_cancel_invo_show.jsp" frameborder="no">Your browser not support this feature</iframe>
                        </div>
                        <div class="modal-footer" style="height: 24px; padding: 10px 15px;">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function ope(id) {
                    //   alert(id);
                    window.parent.parent.scrollTo(0, 0);
                    var $iframe = $('#ifram');
                    $iframe.attr('src', "admin_cancel_invo_show.jsp?id=" + id + "");
                }

            </script>
            <footer>
                <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>
            </footer>
        </div>

    </body>
</html>
