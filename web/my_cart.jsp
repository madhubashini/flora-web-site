<%-- 
    Document   : my_cart
    Created on : Dec 23, 2015, 11:08:45 AM
    Author     : HP
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="servlets.Added_cart_items"%>
<%@page import="servlets.Keep_cart"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Shopping Cart</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="js/my_javascripts.js"></script> 


    </head>
    <body>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>
                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">

                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <fieldset class="contact-inner" style="height: auto">
                            <p>
                            <h1 style="color: black;margin-left:220px">Shopping Cart</h1>
                            </p>
                            <br>
                            <table class="table1">

                                <thead>
                                    <tr>
                                        <th>Item</th> 
                                        <th>Name</th> 
                                        <th>Quantity</th> 
                                        <th>Price Rs.</th> 
                                        <th>Total Rs.</th> 
                                        <th>Remove</th>
                                    </tr>
                                </thead>

                                <%//                                    int subtotal = 0;
//                                    int total = 0;
//                                    int discout;
//                                    int subtot;
                                    double subtotal = 0.0;
                                    double total = 0;
                                    double discout;
                                    double subtot;
                                    DecimalFormat deci = new DecimalFormat("0.00");

                                    Keep_cart kpc = (Keep_cart) request.getSession().getAttribute("shopping_cart");
                                    Session adsession = Connections.NewHibernateUtil.getSessionFactory().openSession();

                                    if (kpc != null) {
                                        List<Added_cart_items> adc = kpc.returnArray();

                                        for (Added_cart_items add : adc) {
                                            //                                        add.getCart_product_id()
                                            Criteria crtmt = adsession.createCriteria(POJO.Products.class);

                                            crtmt.add(Restrictions.eq("id", add.getCart_product_id()));

                                            POJO.Products pj = (POJO.Products) crtmt.uniqueResult();

                                            double num = Double.parseDouble(pj.getUnitPrice());
                                            //

                                            //double ii = Double.parseDouble(pj.getUnitPrice());
                                            total = (double) add.getCart_product_qty() * Double.parseDouble(pj.getUnitPrice());

                                            String ds = pj.getPromotionn().getDeleteStatus().getStatus();

                                            if (ds.equals("Active")) {
                                                // out.print("if");
                                                discout = Double.parseDouble(pj.getPromotionn().getDiscountPersentage()) * total / Double.parseDouble("100");
                                                //out.print(discout+"discntEka");
                                                subtot = total - discout;
                                            } else {
                                                //out.print("else");
                                                subtot = total;
                                            }

                                            subtotal += subtot;


                                %>
                                <tbody>
                                    <tr>
                                        <td><img src='<%=pj.getImg()%>' width="75px"/>

                                            <%

                                                Criteria c = adsession.createCriteria(POJO.Products.class);
                                                POJO.DeleteStatus pojop = (POJO.DeleteStatus) adsession.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                                                c.add(Restrictions.eq("deleteStatus", pojop));
                                                c.add(Restrictions.eq("id", add.getCart_product_id()));
                                                List<POJO.Products> li = c.list();

                                                if (li.isEmpty()) {
                                                    //out.print("inact");
                                            %>

                                            <p><h5  style="color: red">Please Remove This Item !</h5></p>


                                            <%  } else {
                                                    //out.print("act");
                                                }

                                            %>

                                        </td>
                                        <td><%=pj.getName()%></td> 
                                        <td>
                                            <span><button onclick="incre('<%=pj.getId()%>')" style="width:25px;height: 25px;background-color: #00dcff">+</button></span>&nbsp;
                                            <span id="deka<%=pj.getId()%>"><%=add.getCart_product_qty()%></span>&nbsp;
                                            <span><button onclick="decre('<%=pj.getId()%>')" style="width:25px;height: 25px;;background-color: #00dcff">-</button></span>
                                        </td>
                                

                                <td><%=deci.format(num)%></td>
                                <td><span id="eka<%=pj.getId()%>"><%=deci.format(subtot)%></span></td>

                                <td><a onclick="removecartiem('<%=add.getCart_product_id()%>')"><img src="images/remove.png" /></a></td>



                                </tr>

                                <%                                    }

                                %>

                                
                                <script>
                                    function incre(pid) {
                                        $.post(
                                                "cart_qty_incre",
                                                {id: pid},
                                        function (res) {
                                            var resu = res.toString();
                                            var r = resu.split(",");
                                            var tot = r[1];
                                            var sub = r[3];
                                            var qun = r[5];

                                           
                                           
                                            $('#deka'+pid).html('');
                                            $('#deka'+pid).html(qun);
                                            $('#eka'+pid).html('');
                                            $('#eka'+pid).html(tot);
                                            $('#thuna').html('');
                                            $('#thuna').html(sub);

                                        }
                                        );
                                    }
                                </script>
                                
                                <script>
                                    function decre(pid) {
                                        $.post(
                                                "cart_qty_decre",
                                                {id: pid},
                                        function (res) {

                                            var resu = res.toString();
                                            var r = resu.split(",");
                                            var tot = r[1];
                                            var sub = r[3];
                                            var qun = r[5];

                                            $('#deka'+pid).html('');
                                            $('#deka'+pid).html(qun);
                                            $('#eka'+pid).html('');
                                            $('#eka'+pid).html(tot);
                                            $('#thuna').html('');
                                            $('#thuna').html(sub);
                                        }
                                        );
                                    }
                                </script>
                                <%                                    }

                                %>

                                <%                                        DecimalFormat d = new DecimalFormat("0.00");
                                %>

                                <tr>
                                    <td style="background-color: white;   border-color: white;"></td>
                                    <td style="background-color: white;   border-color: white;"></td>
                                    <td style="background-color: white;   border-color: white;"></td>
                                    <td colspan="2" style="background-color: white;   border-color: white;">

                                        <h4 style="color: black">All Total &nbsp; Rs:&nbsp;<span  id="thuna"><%=d.format(subtotal)%></span></h4>

                                    </td>
                                </tr>
                                </tbody>

                            </table>

                            <script>
                                function removecartiem(cv) {
                                    // alert("awaaaa");
                                    $.post(
                                            "remove_from_cart",
                                            {id: cv},
                                    function (result) {
                                        var m = result.toString();

                                        window.location.replace("my_cart.jsp");

                                    }
                                    );

                                }
                            </script>

                            <%

                                String mad = null;
                                List<POJO.Products> lm = null;

                                Session ads = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                Keep_cart kp = (Keep_cart) request.getSession().getAttribute("shopping_cart");

                                POJO.DeleteStatus pojopr = (POJO.DeleteStatus) ads.load(POJO.DeleteStatus.class, Integer.parseInt("1"));

                                if (kp != null) {
                                    List<Added_cart_items> ad = kp.returnArray();
                                    forlp:
                                    for (Added_cart_items adm : ad) {

                                        Criteria mycrit1 = ads.createCriteria(POJO.Products.class);
                                        mycrit1.add(Restrictions.ne("deleteStatus", pojopr));
                                        mycrit1.add(Restrictions.eq("id", adm.getCart_product_id()));

                                        lm = mycrit1.list();

                                        if (lm.isEmpty()) {
                                            // out.print("ac");
                                            mad = "ac";

                                        } else {
                                            //  out.print("in");
                                            mad = "in";
                                            break forlp;
                                        }

                                    }

//                                    
                                }
                            %>



                            <%
                                if (mad == "ac") {
                            %>


                            <br>
                            <div  class="contact-submit">
                                <a   onclick="klo()" class="New-lassana-btn">Go To Next<img style="vertical-align: middle" src="images/index.png"></a>
                                <br>
                                <br>
                                <a id="conshopping-btn" href="index.jsp"  class="New-lassana-btn"><img style="vertical-align: middle" src="images/index2.png">Continue shopping</a><br>

                            </div>
                            <br>

                            <%      }

                            %>



                            <script>

                                function klo() {

                                <% //
                                    if (request.getSession().getAttribute("login") == null) {
                                %>

                                    window.location.replace("User_login.jsp");
                                <%
                                } else {
                                %>
                                    window.location.replace("my_delivery.jsp");
                                <%
                                    }
                                %>
                                }
                            </script>


                        </fieldset>

                        <%
                            ads.close();
                            adsession.close();
                        %>


                    </div> 
                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>
    </body>
</html>
