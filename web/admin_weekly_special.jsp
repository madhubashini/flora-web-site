<%-- 
    Document   : admin_weekly_special
    Created on : Mar 30, 2016, 3:12:07 PM
    Author     : HP
--%>
<%
    String num = null;
    if (request.getSession().getAttribute("login") != null) {
        POJO.UserReg logu = (POJO.UserReg) request.getSession().getAttribute("login");
        num = logu.getUserType().getId() + "";
        System.out.print("if eke");

        if (num.equals("2")) {


%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/fullcalendar.css' rel='stylesheet'>
        <link href='admin_css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link href='admin_css/uploadify.css' rel='stylesheet'>
        <link rel="shortcut icon" href="img/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />

        <script src="js/new2.js"></script>
        <script src="js/new_jquery.min.js"></script>


    </head>
    <body>
        <!-- topbar start -->
        <%@include file="admin_header.jsp" %>
        <!-- topbar ends -->
        <div class="container-fluid">

            <div class="row-fluid">
                <!-- left menu starts -->
                <%@include file="admin_leftBar.jsp" %>
                <!-- left menu ends -->
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <p style="font-size: 30px;margin-left: 35%;margin-top: 5%">Weekly Special Product</p>
                    </div>
                    <div class="row-fluid sortable">
                        <div  style="width: 1000px">
                            <div class="box-content">
                                <br>
                                <button style="float: right;margin-right: " type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal5">Add New Item</button>
                                <br>
                                <br>
                                <br>
                                <table class="table  table-bordered bootstrap-datatable datatable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Product Id</th>
                                            <th>Added Date</th>
                                            <th>End Date</th>
                                            <th>Unit Price  Rs.</th>
                                            <th>Discount Percentage  %</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <tr>
                                            <%                                                int index = 0;
                                                int records = 0;
                                                int recordsPerPage = 5;
                                                int proCount = 0;

                                                String realPageIndex = request.getParameter("pageIndx");

                                                if (realPageIndex == null) {
                                                    index = 1;
                                                } else {
                                                    index = Integer.parseInt(realPageIndex);
                                                }
                                                int result = (index * recordsPerPage) - recordsPerPage;

                                                Session se = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                                Criteria mycr = se.createCriteria(POJO.Weekly.class);

                                                mycr.setFirstResult(result);
                                                mycr.setMaxResults(recordsPerPage);

                                                POJO.DeleteStatus del = (POJO.DeleteStatus) se.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                                                mycr.add(Restrictions.eq("deleteStatus", del));

                                                List<POJO.Weekly> we = mycr.list();
                                                for (POJO.Weekly w : we) {

                                                    int idpro = w.getProducts().getId();
                                                    Criteria crt = se.createCriteria(POJO.Products.class);
                                                    crt.add(Restrictions.eq("id", idpro));
                                                    POJO.Products prom = (POJO.Products) crt.uniqueResult();

                                            %>
                                            <td class="center"><%=w.getId()%></td>
                                            <td class="center"><img src='<%=prom.getImg()%>' width="50px" height="50px"></td>
                                            <td class="center"><%=prom.getName()%></td>
                                            <td class="center"><%=prom.getId()%></td>
                                            <td class="center"><%=w.getAddedDate()%></td>
                                            <td class="center"><%=w.getEndDate()%></td>
                                            <td class="center"><%=prom.getUnitPrice()%></td>
                                            <td class="center"><%=prom.getPromotionn().getDiscountPersentage()%></td>
                                            <td class="center">
                                                <a class="btn btn-danger" onclick="del('<%=w.getId()%>')">
                                                    Delete
                                                </a>
                                                <script>
                                                    function del(para) {
                                                        $.post(
                                                                "delete_weekly",
                                                                {id: para},
                                                        function (result) {
                                                            window.location.replace("admin_weekly_special.jsp");
                                                        }
                                                        );
                                                    }
                                                </script>
                                            </td>


                                        </tr>
                                        <%                                            }

                                            Criteria cri77 = se.createCriteria(POJO.Weekly.class);
                                            POJO.DeleteStatus de = (POJO.DeleteStatus) se.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                                            cri77.add(Restrictions.eq("deleteStatus", de));

                                            List<POJO.Weekly> li = cri77.list();

                                            for (POJO.Weekly prm : li) {
                                                proCount++;
                                                // out.print(prm.getId());
                                            }

                                            int pages = proCount / 5;
                                            if (proCount > (pages * 5)) {
                                                pages = pages + 1;
                                            }

                                        %>
                                    </tbody>
                                </table>
                                <div class="pagination pagination-centered">
                                    <ul>
                                        <%                                            String url = null;
                                            for (int ii = 1; ii <= pages; ii++) {
                                                url = "http://localhost:8080/Madhu_Flora_New/admin_weekly_special.jsp?pageIndx=" + ii;

                                        %>



                                        <li class="active">
                                            <a href='<%=url%>'><%=ii%></a>
                                        </li>



                                        <%    }

                                        %>
                                    </ul>
                                </div

                            </div>

                        </div>
                    </div>

                    <div class="modal fade" id="myModal5" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" style="color: black;margin-left: 30%">Add new Item</h4>
                                </div>
                                <div class="modal-body" style="color: black">
                                    <form class="form-horizontal" method="post" action="">
                                        <fieldset>
                                            <br>
                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Product Id</label>
                                                <div class="controls">
                                                    <input class="input-xlarge focused" id="pid" type="number" name="">
                                                </div>
                                            </div>
                                            <p id="as" class="hide" style="color:red;margin-left:70px">Should be number</p>
                                            <p style="color:red;margin-left:70px" class="hide" id="msgeka">Invalid Item Id</p>

                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Added date</label>
                                                <div class="controls">
                                                    <input type="date" class="input-xlarge datepicker hasDatepicker" id="ad_date" value="">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">End date</label>
                                                <div class="controls">
                                                    <input type="date" class="input-xlarge datepicker hasDatepicker" id="en_date" value="">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" onclick="pro()">Add New</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        function pro() {

                            var pidk = document.getElementById("pid").value;
                            var ad = document.getElementById("ad_date").value;
                            var en = document.getElementById("en_date").value;

                           // alert(pidk + "" + ad + "" + en);
                           
                            if (pidk == "") {
                                $('#pide').show();
                                p = false;
                            } else {
                                p = true;
                            }
                            if (ad == "") {
                                pi = false;
                            } else {
                                pi = true;
                            }
                            if (en == "") {
                                pm = false;
                            }else{
                                pm=true;
                            }

                            if (p == true && pi == true && pm==true) {
                                //alert("awa");
                                $.post(
                                        "weekly_special",
                                        {p: pidk, a: ad, e: en},
                                function (result) {
                                    var a = result.toString();
                                    if (a == 1) {
                                        alert("Successfully Saved");
                                        window.location.replace("admin_weekly_special.jsp");
                                    }
                                    if (a == 2) {
                                        $('#msgeka').show();
                                    }
                                }

                                );
                            }
                        }
                    </script>
                </div>

            </div>
            <footer>
                <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>
            </footer>
        </div>

    </body>
</html>
<%        }

    } else {
        response.sendRedirect("index.jsp");
    }

%>