<%-- 
    Document   : my_userPro_deac
    Created on : Mar 20, 2016, 11:04:35 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/loginpage.css">

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>      
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>        
        <script type="text/javascript" src="js/my_javascripts.js"></script> 
    </head>
    <body>



        <div id="templatemo_wrapper_sp">

            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->
            </div>
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">

                    <%                                String ur = request.getParameter("code");
                    %>

                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <br>
                        <br>
                        <div class="form">
                            <div class="tab-content">

                                <h1>Deactivate Account!</h1>

                                <form>

                                    <p style="color: white;font-size: 16px">If you deactivate your account , you will not receive our notification e-mails 
                                        and you can re-activate your account by login !</p>
                                    <br>
                                    <div class="field-wrap">
                                        <input type="text" id="una"  placeholder="User Name" autofocus>
                                    </div>
                                    <p id="un_ep" class="hidden" style="color: white">Username Can't Be Empty</p>
                                    <div class="field-wrap">
                                        <input type="password" id="pss"  placeholder="Password">
                                    </div>
                                    <p id="ps_e" class="hideitems" style="color: white">Password Can't be empty</p>
                                    <p id="ps_le" class="hideitems" style="color: white">Length must be more than 4</p>
                                    <p id="mat" class="hideitems" style="color: white"> Username and Password Doesn't Match</p>
                                    <p id="suc" class="hideitems" style="color: white">Deactivation Success !</p>

                                    <ul class="tab-group">
                                        <li><a onclick="deac()" class="sign-up-button">Deactive</a></li>
                                    </ul>

                                </form>
                            </div>
                        </div>

                        <script type="text/javascript">
                            function deac() {
                                // alert("has");
                                var cod = '<%=ur%>';
                                var nam = document.getElementById("una").value;
                                var ps = document.getElementById("pss").value;

                                if (nam == "") {
                                    $('#un_ep').show();
                                    namfil = false;
                                } else {
                                    namfil = true;
                                }

                                if (ps == "") {
                                    $('#ps_e').show();
                                    psfll = false;
                                } else {
                                    psfll = true;

                                    if (ps.length <= 4) {
                                        $('#ps_le').show();
                                        leng = false;
                                    } else {
                                        leng = true;
                                    }
                                }

                                if (namfil == true && psfll == true && leng == true) {
                                    $.post(
                                            "account_deact",
                                            {c: cod, n: nam, p: ps},
                                    function (rec) {
                                        var resl = rec.toString();
                                        if (resl == 1) {
                                            // $('#suc').show();
                                            window.location.replace("index.jsp");
                                        }
                                        if (resl == 2) {
                                            $('#mat').show();
                                        }
                                    }
                                    );
                                }
                            }


                        </script>
                    </div> 
                    <div class="cleaner h20"></div>                       
                    <div class="cleaner h20"></div>
                </div>

                <div class="cleaner"></div>

            </div> <!-- END of main wrapper -->
            <%@include file="my_footer.jsp" %>
        </div>

    </div>
</body>
</html>
