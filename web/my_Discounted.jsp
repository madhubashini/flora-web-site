<%-- 
    Document   : my_Discounted
    Created on : Mar 1, 2016, 1:21:25 PM
    Author     : HP
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Products</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" type="text/css" href="css/pagination.css" />

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>        
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="js/my_javascripts.js"></script> 

    </head>
    <body>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#">Madhu Flora</a></div>

                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->
            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">

                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <h2>Products</h2>
                        <p></p>
                        <%                            int s = 0;
                            int p = 0;
                            int c = 1;
                            boolean b;

                            String stat = null;
                            int procount = 0;
                            int pgcoun = 0;

                            int indexess = 0;
                            int recordsPerpg = 8;
                            String pagein = request.getParameter("PageIndex");

                            String getI = request.getParameter("idf");
                            int get = Integer.parseInt(getI);
//                            out.print(getI);

                            if (pagein == null) {
                                indexess = 1;
                            }
                            if (pagein != null) {
                                indexess = Integer.parseInt(pagein);
                            }
                            int re = (indexess * recordsPerpg) - recordsPerpg;

                            Session d = Connections.NewHibernateUtil.getSessionFactory().openSession();
                            //
                            Criteria crf = d.createCriteria(POJO.SubCatogory.class);
                            POJO.Catogory cate = (POJO.Catogory) d.load(POJO.Catogory.class, get);
                            crf.add(Restrictions.eq("catogory", cate));
                            List<POJO.SubCatogory> sublist = crf.list();
                            //
                            POJO.Promotionn prom = (POJO.Promotionn) d.load(POJO.Promotionn.class, Integer.parseInt("1"));

                            Criteria cr = d.createCriteria(POJO.Products.class);
                            POJO.DeleteStatus pj = (POJO.DeleteStatus) d.load(POJO.DeleteStatus.class, Integer.parseInt("2"));
                            POJO.DeleteStatus pjo = (POJO.DeleteStatus) d.load(POJO.DeleteStatus.class, Integer.parseInt("3"));
                            cr.add(Restrictions.ne("deleteStatus", pj));
                            cr.add(Restrictions.in("subCatogory", sublist));
                            cr.add(Restrictions.ne("promotionn", prom));

                            cr.setFirstResult(re);
                            cr.setMaxResults(recordsPerpg);

                            List<POJO.Products> prolsit = cr.list();

//                           
                            for (POJO.Products pro : prolsit) {

                                double prv = Double.parseDouble(pro.getUnitPrice());
                                if (pro.getDeleteStatus().equals(pjo)) {
                                    stat = "Out Of Stock";
                                } else {
                                    stat = "In Stock";
                                }

                                DecimalFormat ds = new DecimalFormat("0.00");

                        %>

                        <div class="product_box">
                            <a href="my_product_details.jsp"><img src='<%=pro.getImg()%>' alt="floral set 1" width="165" height="165" /></a>
                            <h3><%=pro.getName()%></h3>
                            <p class="product_price">Rs:<%=ds.format(prv)%></p>
                            <p class="product_price"><%=stat%></p>
                            <%
                                if (!(pro.getDeleteStatus().equals(pjo))) {
                            %>
                            <p class="add_to_cart">
                                <a onclick="scv('<%=pro.getId()%>')">Add to Cart</a>
                                <script type="text/javascript">
                                    function scv(ii) {
                                        //alert("aaa");
                                        var url = "my_product_details.jsp?id=" + ii;
                                        window.location.replace(url);
                                    }
                                </script>

                                <%

                                    }


                                %>
                            </p>
                        </div>   
                        <%                            }

                            Criteria cr2 = d.createCriteria(POJO.Products.class);

                            cr2.add(Restrictions.ne("deleteStatus", pj));
                            cr2.add(Restrictions.in("subCatogory", sublist));
                            cr2.add(Restrictions.ne("promotionn", prom));
                            List<POJO.Products> prolsit2 = cr2.list();

                            for (POJO.Products pr : prolsit2) {
                                //out.print( pr.getName());
                                ++procount;
                            }
                            //out.print(procount);

                            pgcoun = procount / 8;

                            if (procount > (pgcoun * 8)) {
                                pgcoun = pgcoun + 1;

                            }


                        %>

                        <div class="pagination">
                            <ul>

                                <%                    //
                                    String newu = "";

                                    for (int ii = 1; ii <= pgcoun; ii++) {
                                        String url = "http://localhost:8080/Madhu_Flora_New/my_Discounted.jsp";

                                        if (getI != null) {
                                            newu = url + "?PageIndex=" + ii + "&idf=" + getI;
                                            // newu = url + "?PageIndex=" + ii;
                                        }
                                %>


                                <li><a href="<%=newu%>"><%=ii%></a>

                                    <%
                                        }

                                    %>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="cleaner"></div>

            </div> <!-- END of main -->
        </div> <!-- END of main wrapper -->

        <%@include file="my_footer.jsp" %>
    </div>
</body>
</html>
