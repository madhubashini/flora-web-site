<%-- 
    Document   : admin_search_viewpro
    Created on : Jul 7, 2016, 11:24:24 AM
    Author     : HP
--%>

<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link rel="shortcut icon" href="img/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    </head>
    <body>

        <%
            if (request.getParameter("pid") != null) {

                Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
                POJO.Products pr = (POJO.Products) ses.load(POJO.Products.class, Integer.parseInt(request.getParameter("pid")));
                

        %>

        <form class="form-horizontal" method="post" action="" style="margin-right: 50px">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="focusedInput"></label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='<%=pr.getImg()%>' width="120px" height="120px">                                      
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Item Name</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="text" name="name" value='<%=pr.getName()%>'>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Items</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="text" name="item" value=''>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Weight</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="text" name="weight" value='<%=pr.getWeight()%>'>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Unit Price</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="text" name="untp" value='<%=pr.getUnitPrice()%>'>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Discount</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="text" name="discount" value='<%=pr.getPromotionn().getDiscountPersentage()%>'>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Sub Category</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="text" name="" value='<%=pr.getSubCatogory().getSubCatogory()%>' >
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="textarea2">Description</label>
                    <div class="controls">
                        <textarea class="cleditor" id="" rows="6" name="desc" style="width:100%" scrolling="no"><%=pr.getDescription()%></textarea>
                    </div>
                </div>
            </fieldset>
        </form>
        <%                    } else {
                out.print("loading error ekak");
            }
        %>

    </body>
</html>

