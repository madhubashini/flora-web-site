<%-- 
    Document   : admin_deleted_view
    Created on : Jul 7, 2016, 2:25:39 PM
    Author     : HP
--%>

<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link rel="shortcut icon" href="img/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    </head>
    <body>
        <%
            String vform = request.getParameter("pid");
            Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
            if (vform != null) {
                POJO.UserReg ur = (POJO.UserReg) ses.load(POJO.UserReg.class, Integer.parseInt(vform));


        %>

        <form class="form-horizontal" method="post" action="" style="margin-right: 50px">
            <br>
            <br>
            <br>
            <fieldset>

                <div class="control-group">
                    <label class="control-label">Id</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getId()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"> First Name</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getFstName()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"> Last Name</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getLastName()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">E-Mail</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getEMail()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Added Date</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getAddedDate()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">User Type</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getUserType().getType()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Status</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getUserStatus().getStatus()%></span>
                    </div>
                </div>
            </fieldset>
        </form>

        <%    }
        %>
    </body>
</html>
