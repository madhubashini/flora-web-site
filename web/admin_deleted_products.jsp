<%-- 
    Document   : admin_deleted_products
    Created on : Jul 4, 2016, 2:22:58 PM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link rel="shortcut icon" href="img/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />

        <script src="js/new2.js"></script>
        <script src="js/new_jquery.min.js"></script>
    </head>
    <body>
        <!-- topbar start -->
        <%@include file="admin_header.jsp" %>
        <!-- topbar ends -->
        <div class="container-fluid">

            <div class="row-fluid">
                <!-- left menu starts -->
                <%@include file="admin_leftBar.jsp" %>
                <!-- left menu ends -->
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <p style="font-size: 30px;margin-left: 50%;margin-top: 5%">Inactive Products</p>
                    </div>
                    <div class="row-fluid sortable">
                        <div  style="width: 1000px">

                            <div class="box-content" >
                                <br>
                                <table class="table  table-bordered bootstrap-datatable datatable"   >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Unit Price</th>
                                            <th>Category</th>
                                            <th>SubCategory</th>
                                            <th>Status</th>
                                            <th>view</th>
                                            <th>Re-Active</th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <%

                                            Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                            int index = 0;
                                            int records = 0;
                                            int recordsPerPage = 10;
                                            int proCount = 0;
                                            String realPageIndex = request.getParameter("pageIndx");

                                            if (realPageIndex == null) {
                                                index = 1;
                                            } else {
                                                index = Integer.parseInt(realPageIndex);
                                            }
                                            int result = (index * recordsPerPage) - recordsPerPage;

                                            Criteria crt = ses.createCriteria(POJO.Products.class);
                                            POJO.DeleteStatus del = (POJO.DeleteStatus) ses.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                                            crt.add(Restrictions.ne("deleteStatus", del));

                                            crt.setFirstResult(result);
                                            crt.setMaxResults(recordsPerPage);

                                            List<POJO.Products> pro = crt.list();
                                            for (POJO.Products p : pro) {


                                        %>
                                        <tr>
                                            <td><%=p.getId()%></td>
                                            <td class="center"><img src='<%=p.getImg()%>' width="50px" height="50px"></td>
                                            <td class="center"><%=p.getName()%></td>
                                            <td class="center"><%=p.getUnitPrice()%></td>
                                            <td class="center"><%=p.getSubCatogory().getCatogory().getCatogory()%></td>
                                            <td class="center"><%=p.getSubCatogory().getSubCatogory()%></td>
                                            <td class="center"><%=p.getDeleteStatus().getStatus()%></td>

                                            <td class="center">
                                                <button type="button" onclick="opendetails(<%=p.getId()%>)" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Product Detail</button>
                                            </td>
                                            <td class="center">
                                                <a class="btn btn-success" onclick="reac('<%=p.getId()%>')">
                                                    Active                                            
                                                </a>

                                            </td>
                                        </tr>
                                    <script>
                                        function reac(par) {
                                            $.post("active_product", {id: par},
                                            function (res) {
                                                var u = window.location.href;
                                                window.location.replace(u);
                                            }
                                            );
                                        }
                                    </script>
                                    <script>

                                    </script>
                                    <%

                                        }
                                    %>

                                    </tbody>
                                </table> 
                                <%
                                    Criteria cr = ses.createCriteria(POJO.Products.class);
                                    POJO.DeleteStatus de = (POJO.DeleteStatus) ses.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                                    cr.add(Restrictions.ne("deleteStatus", de));

                                    List<POJO.Products> l = cr.list();

                                    for (POJO.Products prm : l) {
                                        proCount++;
                                        // out.print(prm.getId());
                                    }

                                    int pages = proCount / 10;
                                    if (proCount > (pages * 10)) {
                                        pages = pages + 1;
                                    }
                                %>
                            </div>


                            <div class="pagination pagination-centered">
                                <ul>
                                    <%
                                        String url = null;
                                        for (int ii = 1; ii <= pages; ii++) {
                                            url = "http://localhost:8080/Madhu_Flora_New/admin_deleted_products.jsp?pageIndx=" + ii;
                                    %>
                                    <li class="active">
                                        <a href='<%=url%>'><%=ii%></a>
                                    </li>


                                    <%

                                        }

                                    %>
                                </ul>
                            </div>
                        </div>

                        <style>
                            .modal.fade.in {
                                top: 38%;
                            }
                        </style>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" style="color: black;margin-left: 30%">View Product Details</h4>
                                    </div>
                                    <div class="modal-body" style="padding: 0px;color: black;height: 550px; overflow: hidden; max-height: 549px">
                                        <iframe id="ifram" class="pro-edit-iframe" id="prodetail-if" width="100%" height="100%" scrolling="no" border="0" src="admin_search_viewpro.jsp" frameborder="no">Your browser not support this feature</iframe>
                                    </div>
                                    <div class="modal-footer" style="height: 24px; padding: 10px 15px;">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            function opendetails(id) {
                                window.parent.parent.scrollTo(0, 0);
                                var $iframe = $('#ifram');
                                $iframe.attr('src', "admin_search_viewpro.jsp?pid=" + id + "");
                            }

                        </script>

                    </div>
                </div>

            </div>
            <footer>
                <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>
            </footer>
        </div>

    </body>
</html>
