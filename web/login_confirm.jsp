<%-- 
    Document   : index
    Created on : Dec 29, 2015, 11:55:53 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/loginpage.css">


        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/my_javascripts.js"></script> 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>  
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>   
        <script type="text/javascript" src="js/jquery.min.js"></script>

    </head>
    <body>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <%                            String un = request.getParameter("code");
                            // out.print(un);
                        %>
                        <br>
                        <br>
                        <div class="form">

                            <div class="tab-content">

                                <h1>Confirm User!</h1>

                                <form>
                                    <div class="field-wrap">
                                        <input  id="a" name="un" type="text"  placeholder="User Name" autofocus/>
                                    </div>
                                    <p id="us1" class="hideitems" style="color:white"> User Name can't be empty</p>
                                    <p id="us2" class="hideitems" style="color: white">Invalid User Name</p>
                                    <div class="field-wrap">
                                        <input id="b" name="code" type="text" placeholder="Code"/>
                                    </div>
                                    <p id="emp1" class="hideitems" style="color: white">Field Can't Be Empty</p>
                                    <p id="emp2" class="hideitems" style="color: white">Incorrect Code.Please Try Again</p>

                                    <ul class="tab-group">
                                        <li><a onclick="on()"  class="tab">Confirm</a></li>  
                                    </ul>
                                    <br><br>

                                </form> 

                            </div>
                        </div>

                    </div>



                    <script type="text/javascript">

                        function on() {

                            var named = document.getElementById("a").value;
                            var code = document.getElementById("b").value;
                            var uns = '<%=un%>';
                            if (named == "") {
                                $('#us1').show();
                                namefil = false;
                            } else {
                                namefil = true;
                            }
                            if (code == "") {
                                $('#emp1').show();
                                codefil = false;
                            } else {
                                codefil = true;
                                if (uns == code) {
                                    if (namefil == true) {
                                        $.post(
                                                "gencode",
                                                {un: named, code: code},
                                        function (result) {
                                            var out = result.toString();
                                            if (out == 1) {
                                                window.location.replace("index.jsp");
                                            }
                                            if (out == 2) {
                                                $('#us2').show();
                                            }
                                        }
                                        );
                                    }
                                } else {
                                    $('#emp2').show();
                                }
                            }
                        }

                    </script>

                    <div class="cleaner h20"></div>                      
                    <div class="cleaner h20"></div>

                </div>

                <div class="cleaner"></div>
            </div> <!-- END of main -->
        </div> <!-- END of main wrapper -->

        <%@include file="my_footer.jsp" %>
    </div>                             

    <!-- END of main wrapper -->

</body>
</html>
