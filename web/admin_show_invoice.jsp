<%-- 
    Document   : admin_show_invoice
    Created on : Jun 22, 2016, 12:36:37 PM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/fullcalendar.css' rel='stylesheet'>
        <link href='admin_css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link href='admin_css/uploadify.css' rel='stylesheet'>
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="img/favicon.ico">
    </head>
    <body>
        <!-- topbar start -->
        <%@include file="admin_header.jsp" %>
        <!-- topbar ends -->
        <div class="container-fluid">

            <div class="row-fluid">
                <!-- left menu starts -->
                <%@include file="admin_leftBar.jsp" %>
                <!-- left menu ends -->
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <p style="font-size: 30px;margin-left: 60%;margin-top: 5%">Deliver Order</p>
                    </div>
                    <div class="row-fluid sortable">
                        <div class="box span12" style="width: 100%;margin-left: 148px">


                            <div class="box-content">
                                <%
                                    Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                    String id = request.getParameter("ID");

                                    Criteria crt = ses.createCriteria(POJO.Invoice.class);
                                    crt.add(Restrictions.eq("id", Integer.parseInt(id)));
                                    POJO.Invoice invo = (POJO.Invoice) crt.uniqueResult();

                                    if (id != null) {

                                %>


                                <table style="margin-left: 10px">
                                    <tr>
                                        <td><label>Invoice Id</label></td>
                                        <td style="width: 10%"></td>
                                        <td>-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=id%></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>User Id</label></td>
                                        <td style="width: 10%"></td>
                                        <td>-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=invo.getUserReg().getId()%></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>Delivery Date</label></td>
                                        <td style="width: 10%"></td>
                                        <td>-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=invo.getDeliveryInfo().getDate()%></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>Message</label></td>
                                        <td style="width: 10%"></td>
                                        <td>-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=invo.getDeliveryInfo().getMessage()%></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>Instructions</label></td>
                                        <td style="width: 10%"></td>
                                        <td>-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=invo.getDeliveryInfo().getInstruction()%></label></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: super"><label>Shipping Details</label></td>
                                        <td style="width: 10%"></td>
                                        <td style="vertical-align: super">-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=invo.getDeliveryInfo().getRecipientInfo().getTitle()%> &nbsp;<%=invo.getDeliveryInfo().getRecipientInfo().getName()%></label>
                                            <label><%= invo.getDeliveryInfo().getRecipientInfo().getAddress()%></label>
                                            <label><%= invo.getDeliveryInfo().getRecipientInfo().getAddress2()%></label>
                                            <label><%=invo.getDeliveryInfo().getRecipientInfo().getDeliveryCity().getCity()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label>Location Type</label></td>
                                        <td style="width: 10%"></td>
                                        <td>-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=invo.getDeliveryInfo().getRecipientInfo().getLocationType()%></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>Receiver's Contact Number </label></td>
                                        <td style="width: 10%"></td>
                                        <td>-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=invo.getDeliveryInfo().getRecipientInfo().getPhoneNum()%></label></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: super"><label>Billing Details</label></td>
                                        <td style="width: 10%"></td>
                                        <td style="vertical-align: super">-</td>
                                        <td style="width: 10%"></td>
                                        <td>
                                            <label><%=invo.getDeliveryInfo().getSendersInfo().getTitle()%>&nbsp;<%=invo.getDeliveryInfo().getSendersInfo().getName()%></label>
                                            <label><%=invo.getDeliveryInfo().getSendersInfo().getEmail()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label>Sender's Contact Number</label></td>
                                        <td style="width: 10%"></td>
                                        <td>-</td>
                                        <td style="width: 10%"></td>
                                        <td><label><%=invo.getDeliveryInfo().getSendersInfo().getPhoneNum()%></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>Ordered Items</label></td>
                                        <td style="width: 10%"></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Item Name</th>
                                            <th>Item ID</th>
                                            <th>Quantity</th>
                                            <th>Status</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <%
                                            Criteria cr1 = ses.createCriteria(POJO.PayedItems.class);
                                            cr1.add(Restrictions.eq("invoice", invo));
                                            List<POJO.PayedItems> payli = cr1.list();

                                            int checkbocou = 1;
                                            for (POJO.PayedItems p : payli) {

                                        %>

                                        <tr>
                                            <td><label><%=p.getProducts().getName()%></label></td>
                                            <td><%=p.getProducts().getId()%></td>
                                            <td><label><%=p.getQty()%></label></td>
                                            <td>
                                                <label>Ready &nbsp;
                                                    <input id="check<%=checkbocou%>" type="checkbox" value="checked" ></label> 
                                            </td>

                                        </tr>

                                        <%
                                                ++checkbocou;
                                            }
                                        %>
                                    <script>
                                        var productcou =<%=payli.size()%>;
                                    </script>
                                    </tbody>
                                </table>
                                <br>
                                <br>
                                <div style="float: right">
                                    <a  class="btn btn-primary" id="cancelorder">Cancel Order</a>                                            
                                    <a   class="btn btn-primary" onclick="shp(<%=id%>)">Ship Order</a>                                            
                                </div>
                                <br>
                                <br>
                                <br>

                                <%                                    }
                                %>
                            </div>
                        </div>

                        <div class="box span12 " style="width: 100%;margin-left: 148px" id="ide">

                            <div class="box-content">
                                <div  id="myModal" style="display: block;">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"></button>
                                        <h3>Mention the reason to cancel order</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="controls">
                                            <textarea class="cleditor autogrow" id="area" rows="5" name="desc" style="width: 98%"></textarea>
                                        </div> 
                                    </div>
                                    <div style="float: right;margin-right: 15px">
                                        <a onclick="canc('<%=id%>')" class="btn btn-primary">Cancel</a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                </div>

                            </div>
                        </div>     


                    </div>

                </div>
            </div>
            <script>

                function shp(par) {

                    var flagcheckbo = false;
                    var maxinputs = 0;

                    for (var i = 1; i <= productcou; ++i) {

                        if ($('#check' + i).is(':checked')) {
                            ++maxinputs;
                        } else {
                            alert(i + 'checkbox not checkd');
                        }
                    }
                    if (maxinputs == productcou) {
                        // alert("allchecked");
                        $.post(
                                "shipped_invo",
                                {inv_id: par},
                        function (res) {
                            alert("Change Status Shipped");
                            window.location.replace("admin_order_mng.jsp");
                        }
                        );
                    }

                }
            </script>
            <script>
                function canc(para) {
                    //alert("aaa");
                    var txt = document.getElementById("area").value;
                    $.post(
                            "cancel_invo",
                            {inv: para, msg: txt},
                    function (result) {
                        alert("Successfuly Canceled")
                        window.location.replace("admin_order_mng.jsp");
                    }
                    );

                }
                $(document).ready(function () {
//alert("a");?
                    $('#ide').hide();
//alert("ab");

                });
                $('#cancelorder').click(function () {
                    $('#ide').show();
                });
            </script>


        </div>
        <footer>
            <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>
        </footer>
    </div>

</body>
</html>
