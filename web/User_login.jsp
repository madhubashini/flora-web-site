<%-- 
    Document   : User_login
    Created on : Jun 13, 2016, 6:33:38 PM
    Author     : HP
--%>

<%
    HttpSession hts=request.getSession();
    if(hts.getAttribute("login")!=null) {
        response.sendRedirect("index.jsp");
    } 
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/loginpage.css">


        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/my_javascripts.js"></script> 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>  
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>   
        <script type="text/javascript" src="js/jquery.min.js"></script>

        <style>
            #templatemo_menu1{
                font-size: 14px;
                clear: both;
                padding: 10px;
                width: 960px;
                background: url(images/templatemo_menu.png);
                height: 46px;
            }
            #templatemo_menu{
                clear: both;
                padding: 10px;
                width: 960px;
                height: 51px;
                background: url(images/templatemo_menu.png);

            }
        </style>


    </head>

    <body>
        <%

            HttpSession htse = request.getSession();
        %>


        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#">Floral Shop</a></div>
                    <%@include file="my_header.jsp" %>


                </div> <!-- END of header -->
            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">

                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right" >
                        <br>
                        <h4 style="color: white">
                            If you have an account with us  , Please Login . You don't have an account , Then Sign Up .
                        </h4>
                        <br>
                        <br>
                        <div class="form">

                            <ul class="tab-group">
                                <li class="tab" id="signuptab"><a onclick="opensignup()">Sign Up</a></li>
                                <li  class="tab active" id="signintab"><a  onclick="openlogin()">Log In</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="loginss">   
                                    <h1>Welcome Back!</h1>

                                    <form action="" method="">
                                        <div class="field-wrap">
                                            <input id="logun" name="username" value="" type="text" placeholder="User name">
                                        </div>
                                        <P style="color: red" id="username_empty" class="hidden">UserName Can't Be Empty </p>
                                        <P style="color: red" id="psw_empty" class="hidden">Password Can't Be Empty </p>
                                        <P style="color: red" id="password_length" class="hidden">Password Length Must Be More Than 4 Characters</p>
                                        <P style="color: red" id="wrong" class="hidden">Wrong Username or Password</p>
                                        <P style="color: red" id="pend" class="hidden">Please confirm your E-mail</p>
                                        <P style="color: red" id="admin_cont" class="hidden">Please contact your Administrator</p>
                                        <div class="field-wrap">
                                            <input id="logpass" name="password" value="" type="password" placeholder="Password">
                                        </div


                                        <p class="forgot"><a href="enter_email.jsp">Forgot Password?</a></p>

                                        <ul class="tab-group">
                                            <li> <a  id="login_button" class="tab" onclick="login()">Log In</a></li>
                                        </ul>

                                        <br>


                                    </form>

                                </div>

                                <!--Login eka iwarae-->


                                <!-- Register eka -->


                                <div id="signupss">   
                                    <h1>Sign Up for Free</h1>

                                    <form action="" method="">

                                        <div class="top-row">
                                            <div class="field-wrap">
                                                <input id="name" name="name" value="" type="text" placeholder="First Name" >
                                            </div>
                                            <div class="field-wrap">
                                                <input id="lastname" name="last_name" value="" type="text" autocomplete="on" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <P style="color: white" id="fst_empty" class="hidden"> First Name Can't Be Empty </p>
                                        <P style="color:  white" id="last_empty" class="hidden"> Last Name Can't Be Empty </p>
                                        <div class="field-wrap">
                                            <input id="emailad" name="email" value="" type="text" placeholder="E-mail">
                                        </div>
                                        <P style="color: white" id="ma_l" class="hidden">Email Can't Be Empty </p>
                                        <P style="color:  white" id="invalid_mail" class="hidden">Invalid E-mail</p>
                                        <P style="color:  white" id="other" class="hidden">Try another E-mail</p>
                                        <P style="color:  white" id="alr" class="hidden">Already you have an account</p>
                                        <div class="field-wrap">
                                            <input id="regboxuser" name="username" value="" type="text" autocomplete="on" placeholder="User Name">
                                        </div>
                                        <P style="color:  white" id="usn_empty" class="hidden">Username can't be empty</p>
                                        <P style="color:  white" id="usn" class="hidden">Try another username</p>
                                        <div class="field-wrap">
                                            <input id="pwrd1" name="password" value="" type="password" placeholder="Password">
                                        </div>
                                        <div class="field-wrap">
                                            <input id="pwrd2" name="con_password" value="" type="password" placeholder="Re-type Password">
                                        </div>
                                        <P style="color:  white" id="pswf_empty" class="hidden">Password fields Can't Be Empty </p>
                                        <P style="color:  white" id="notmatch" class="hidden">Password doesn't match </p>
                                        <P style="color:  white" id="plen" class="hidden">Password length doesn't match </p>
                                        <P style="color:  white" id="sen" class="hidden">Please Check your mails</p>

                                        <ul class="tab-group">
                                            <li><a id="but" class="tab" type="button" onclick="regis()">Get Started</a></li>
                                        </ul>

                                        <!-- <button type="" class="button button-block"/>Get Started</button>-->
                                    </form>
                                </div>
                                <!-- Login eka eka iwarae-->


                            </div><!-- tab-content -->

                        </div> <!-- /form -->
                    </div>
                    <!-- Login  eke script eka-->
                    <script>

                        function login() {

                            var logname = document.getElementById("logun").value;
                            var passwrd = document.getElementById("logpass").value;
                            if (logname == "") {
                                $("#username_empty").show();
                                user = false;
                            } else {
                                user = true;
                            }
                            if (passwrd == "") {
                                $('#psw_empty').show();
                                passw = false;
                            }
                            else if (passwrd.length <= 4) {
                                $("#password_length").show();
                                passw = false;
                            } else {
                                passw = true;
                            }
                            if (user == true && passw == true) {
                                $.post(
                                        "UserLogin",
                                        {lognames: logname, logpasss: passwrd},
                                function (result) {
                                    var men = result.toString();

                                    if (men == 1) {
                        <%          //
                            String camefrom = request.getHeader("referer");
                            if (camefrom != null) {
                        %>
                                        window.location.replace("<%=camefrom%>");
                        <%
                        } else {
                        %>
                                        window.location.replace("index.jsp");
                        <%
                            }
                        %>
                                    }
                                    if (men == 99) {
                                        window.location.replace("admin_home.jsp");
                                    }
                                    if (men == 2) {
                                        $('#admin_cont').show();
                                    }
                                    if (men == 3) {
                                        $("#pend").show();
                                    }
                                    if (men == 4) {
                                        window.location.replace("index.jsp");
                                    }
                                    if (men == 00) {
                                        $('#wrong').show();
                                    }
                                   

                                });
                            }
                        }
                    </script>

                    <!-- register eke script eka-->
                    <script>

                        function regis() {

                            // alert("aaaa");
                            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                            var fn = document.getElementById("name").value;
                            var ln = document.getElementById("lastname").value;
                            var email = document.getElementById("emailad").value;
                            var un = document.getElementById("regboxuser").value;
                            var p1 = document.getElementById("pwrd1").value;
                            var p2 = document.getElementById("pwrd2").value;

                            if (fn == "") {
                                $('#fst_empty').show();
                                fstname = false;
                            } else {
                                fstname = true;
                            }
//
                            if (ln == "") {
                                $('#last_empty').show();
                                lname = false;
                            } else {
                                lname = true;
                            }
                            if (un == "") {
                                $('#usn_empty').show();
                                unempty = false;
                            } else {
                                unempty = true;
                            }


                            if (email == "") {
                                $('#ma_l').show();
                                emailfil = false;
                            } else {
                                emailfil = true;

                                if (!(filter.test(email))) {
                                    $('#invalid_mail').show();
                                    emailch = false;
                                } else {
                                    emailch = true;
                                }

                            }


                            if (p1 == "" || p2 == "") {
                                $('#pswf_empty').show();
                                pswfill = false;
                            }
                            else {
                                pswfill = true;
                                if (!(p1 == p2)) {
                                    $('#notmatch').show();
                                    pswmath = false;
                                }
                                else {
                                    pswmath = true;
                                    if ((p1.length <= 4) || (p2.length <= 4)) {
                                        $('#plen').show();
                                        pwrdle = false;
                                    }
                                    else {
                                        pwrdle = true;
                                    }
                                }

                            }

                            if (fstname == true && lname == true && unempty == true && emailfil == true && emailch == true
                                    && pswfill == true && pswmath == true && pwrdle == true) {
                                //alert("alerteke");
                                servlet_post();
                            }else{
                               
                               
                            }

                        }

                        function servlet_post() {

                            var fname = document.getElementById("name").value;
                            var lname = document.getElementById("lastname").value;
                            var unames = document.getElementById("regboxuser").value;
                            var passw = $("#pwrd1").val();
                            var mails = $("#emailad").val();
                            $.post(
                                    "Register",
                                    {passw: passw, mail: mails, fnames: fname, lnames: lname, un: unames},
                            function (result) {
                                // alert("resulteka athule");
                                var res = result.toString();

                                if (res == 2) {
                                    $('#sen').show();
                                    
                                }
                                if (res == 1) {
                                    $('#usn').show();
                                }
                                if (res == 3) {
                                    $('#other').show();
                                }
                                if (res == 4) {
                                    $('#alr').show();
                                }


                            }


                            );
                        }
                    </script>

                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>
        <script>
            function openlogin() {
                $('#signupss').attr('style', "display:none");
                $('#loginss').attr('style', "display:block");
                $('#signuptab').removeClass('active');
                $('#signintab').addClass('active');
            }
            function opensignup() {
                $('#signupss').attr('style', "display:block");
                $('#loginss').attr('style', "display:none");
                $('#signuptab').addClass('active');
                $('#signintab').removeClass('active');
            }

            ddsmoothmenu.init({
                mainmenuid: "templatemo_menu", //menu DIV id
                orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
                classname: 'ddsmoothmenu', //class added to menu's outer DIV
                //customtheme: ["#1c5a80", "#18374a"],
                contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
            });
        </script>
    </body>
</html>
