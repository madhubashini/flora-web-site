<%-- 
    Document   : sales_report
    Created on : Oct 13, 2016, 10:36:38 PM
    Author     : HP
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" src="js/jquery.min.js"></script>
    </head>
    <body>

        <style>
            td,th{
                border: solid black 1px;    
            }
        </style>
<script>
            $(document).ready(function () {
                PrintElem('#a');
              
               
            });
            function PrintElem(elem)
            {
                //                Popup($('<div/>').append($(elem).clone()).html());
                Popup($(elem).html());
            }

            function Popup(data)
            {

                var headstr = "<html><head><title>Booking Details</title></head><body>";
                var footstr = "</body></html>";
                var newstr = document.getElementById('a').innerHTML;
                var oldstr = document.body.innerHTML;
                document.body.innerHTML = headstr + newstr + footstr;
                window.print();
                document.body.innerHTML = oldstr;
                return false;
            }
        </script>
        <div id="a">
        <table style="width: 90%;" >

            <th>Invoice Id</th>
            <th>User Id</th>
            <th>Date</th>
            <th>Total</th>

            <%

                Date d = new Date();
                SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yy");
                String ss = sd.format(d);
                //out.print(ss);

                Session s = Connections.NewHibernateUtil.getSessionFactory().openSession();
                Criteria c = s.createCriteria(POJO.Invoice.class);
                c.add(Restrictions.eq("date", ss));
                List<POJO.Invoice> l = c.list();
                double tot = 0;
                DecimalFormat df = new DecimalFormat("0.00");

                for (POJO.Invoice i : l) {


            %>

            <tr>
                <td><%=i.getId()%></td>
                <td><%=i.getUserReg().getId()%></td>
                <td><%=i.getDeliveryInfo().getDate()%></td>
                <td><%=i.getSubtot()%></td>
            </tr>

            <%
                    tot = tot + Double.parseDouble(i.getSubtot());
                }
            %>



        </table>
        <div style="float: right;margin-right: 10%">
            Today Collection = Rs  <%=df.format(tot)%>
        </div>
</div>
    </body>
</html>
