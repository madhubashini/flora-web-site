<%-- 
    Document   : show_one_user
    Created on : Oct 13, 2016, 5:09:21 PM
    Author     : HP
--%>

<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" src="js/jquery.min.js"></script>
    </head>



    <script>
        $(document).ready(function () {
            PrintElem('#a');


        });
        function PrintElem(elem)
        {
            //                Popup($('<div/>').append($(elem).clone()).html());
            Popup($(elem).html());
        }

        function Popup(data)
        {

            var headstr = "<html><head><title>Booking Details</title></head><body>";
            var footstr = "</body></html>";
            var newstr = document.getElementById('a').innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
    </script>
    <body id="a">
        <%
            String uid = request.getParameter("uid");

            Session s = Connections.NewHibernateUtil.getSessionFactory().openSession();
            Criteria cr = s.createCriteria(POJO.UserReg.class);
            cr.add(Restrictions.eq("id", Integer.parseInt(uid)));
            POJO.UserReg ur = (POJO.UserReg) cr.uniqueResult();


        %>

        <h2 style="margin-left: 40%">User Details</h2>
        <div style="margin-left:20%">
            User Id : <%=ur.getId()%>  <br>
            First Name :<%=ur.getFstName()%><br>
            E-mail : <%=ur.getEMail()%><br>
            Added Date :<%=ur.getAddedDate()%><br>
            User Type :<%= ur.getUserType().getType()%><br>
            User Status :<%=ur.getUserStatus().getStatus()%><br>

        </div>


    </body>
</html>
