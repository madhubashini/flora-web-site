<%-- 
    Document   : admin_deleted_users
    Created on : Jun 21, 2016, 11:44:18 AM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link rel="shortcut icon" href="admin_image/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />

        <script src="js/new2.js"></script>
        <script src="js/new_jquery.min.js"></script>
    </head>
    <body>

        <!-- topbar start -->
        <%@include file="admin_header.jsp" %>
        <!-- topbar ends -->
        <div class="container-fluid">

            <div class="row-fluid">
                <!-- left menu starts -->
                <%@include file="admin_leftBar.jsp" %>
                <!-- left menu ends -->
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <p style="font-size: 30px;margin-left: 50%;margin-top: 5%">Deleted Users</p>
                    </div>
                    <br>
                    <%
                        Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();

                        String RequestID = request.getParameter("ReqID");
                        String RequestTy = request.getParameter("ReqTyp");

                        String vform = request.getParameter("viewId");
                        String shwform = request.getParameter("showId");


                    %>

                    <div class="row-fluid sortable">
                        <div  style="width: 1000px">

                            <div class="box-content">

                                <div class="dataTables_filter" id="DataTables_Table_0_filter"><label>
                                        <div style="margin-left: 50%">
                                            Search By Id &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="aa" type="number" aria-controls="DataTables_Table_0">&nbsp;&nbsp;&nbsp;
                                        <button onclick="idekas()" class="btn btn-primary">Search</button>
                                    </div>
                                    </label><br>
                                </div>
                                <p id="men" class="hidden" style="color: red">You Entered Invalid ID</p>

                                <script>
                                    function idekas() {
                                        var proId = document.getElementById("aa").value;
                                        window.location.replace(" admin_deleted_users.jsp?ReqID=" + proId);

                                    }
                                </script>

                                <div class="dataTables_filter" id="DataTables_Table_0_filter"><label>
                                        <div style="margin-left: 50%">
                                        Search By Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id="cat6" name="" aria-controls="DataTables_Table_0" style="width: 150px">
                                            <option selected="see">Select</option>
                                            <%                                                Criteria cr1 = ses.createCriteria(POJO.UserType.class);
                                                List<POJO.UserType> user = cr1.list();
                                                for (POJO.UserType us1 : user) {

                                            %>

                                            <option value="4"><%=us1.getType()%></option> 


                                            <%   }


                                            %>


                                        </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button onclick="ustypes()" class="btn btn-primary">Search</button>
                                    </div>
                                    </label>
                                </div>
                                <script>
                                    function ustypes() {
                                        var utype = $('#cat6').find('option:selected').text();
                                        window.location.replace(" admin_deleted_users.jsp?ReqTyp=" + utype);

                                    }
                                </script>
                                <br>
                                <hr>
                                <br>
                                <table class="table  table-bordered bootstrap-datatable datatable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>E mail</th>
                                            <th>Added Date</th>
                                            <th>User Type</th>
                                            <th>User Activity</th>
                                            <th>User View </th>
                                            <th>Active</th>

                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <%                                            int index = 0;
                                            int records = 0;
                                            int recordsPerPage = 10;
                                            int proCount = 0;

                                            String realPageIndex = request.getParameter("pageIndx");

                                            if (realPageIndex == null) {
                                                index = 1;
                                            } else {
                                                index = Integer.parseInt(realPageIndex);
                                            }
                                            int result = (index * recordsPerPage) - recordsPerPage;

                                            Criteria criuser = ses.createCriteria(POJO.UserReg.class);
                                            POJO.UserReg user_regs = null;
                                            POJO.UserStatus u = (POJO.UserStatus) ses.load(POJO.UserStatus.class, Integer.parseInt("2"));

                                            if (RequestID != null) {
                                                criuser.add(Restrictions.eq("id", Integer.parseInt(RequestID)));
                                            }

                                            if (RequestTy != null) {

                                                Criteria c1 = ses.createCriteria(POJO.UserType.class);
                                                c1.add(Restrictions.eq("type", RequestTy));
                                                POJO.UserType usty = (POJO.UserType) c1.uniqueResult();

                                                criuser.add(Restrictions.eq("userType", usty));
                                            }
                                            criuser.add(Restrictions.eq("userStatus", u));

                                            criuser.setFirstResult(result);
                                            criuser.setMaxResults(recordsPerPage);

                                            List<POJO.UserReg> user_reg_list = criuser.list();

                                            if (!user_reg_list.isEmpty()) {

                                                for (POJO.UserReg loopuser : user_reg_list) {


                                        %>

                                        <tr>
                                            <td class="center"><%=loopuser.getId()%></td>
                                            <td class="center"><%=loopuser.getFstName()%></td>
                                            <td class="center"><%=loopuser.getLastName()%></td>
                                            <td class="center"><%=loopuser.getEMail()%></td>
                                            <td class="center"><%=loopuser.getAddedDate()%></td>
                                            <td class="center"><%=loopuser.getUserType().getType()%></td>
                                            <td class="center">
                                                <button type="button" onclick="opendet('<%=loopuser.getId()%>')" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal5">View Activity</button> 
                                            </td>
                                            <td class="center">
                                                <button type="button" onclick="opend('<%=loopuser.getId()%>')" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">View User</button>
                                            </td>
                                            <td class="center">
                                                <a class="btn btn-danger" onclick="active('<%=loopuser.getId()%>')">
                                                    Active
                                                </a>
                                            </td>
                                    <script>
                                        function active(par) {
                                            $.post(
                                                    "user_active",
                                                    {user: par},
                                            function (result) {
                                                var url = window.location.href;
                                                window.location.replace(url);
                                            }
                                            );
                                        }
                                    </script>

                                    </tr>

                                    <%
                                            }
                                        }

                                    %>

                                    </tbody>
                                </table>
                                <%                                    Criteria crmtt = ses.createCriteria(POJO.UserReg.class);
                                    POJO.UserStatus uu = (POJO.UserStatus) ses.load(POJO.UserStatus.class, Integer.parseInt("2"));

                                    if (RequestID != null) {
                                        crmtt.add(Restrictions.eq("id", Integer.parseInt(RequestID)));
                                    }

                                    if (RequestTy != null) {

                                        Criteria c2 = ses.createCriteria(POJO.UserType.class);
                                        c2.add(Restrictions.eq("type", RequestTy));
                                        POJO.UserType usty = (POJO.UserType) c2.uniqueResult();
                                        crmtt.add(Restrictions.eq("userType", usty));
                                    }

                                    crmtt.add(Restrictions.eq("userStatus", uu));

                                    List<POJO.UserReg> li = crmtt.list();

                                    for (POJO.UserReg prm : li) {
                                        proCount++;
                                        // out.print(prm.getId());
                                    }

                                    int pages = proCount / 10;
                                    if (proCount > (pages * 10)) {
                                        pages = pages + 1;
                                    }


                                %>

                                <div class="pagination pagination-centered">
                                    <ul>
                                        <%                                            String url = null;
                                            for (int ii = 1; ii <= pages; ii++) {

                                                if (RequestID != null) {
                                                    url = "http://localhost:8080/Madhu_Flora_New/admin_deleted_users.jsp?pageIndx=" + ii + "&ReqID=" + RequestID;
                                                }

                                                if (RequestTy != null) {
                                                    url = "http://localhost:8080/Madhu_Flora_New/admin_deleted_users.jsp?pageIndx=" + ii + "&ReqTyp=" + RequestTy;
                                                }
                                                if (RequestID == null && RequestTy == null) {
                                                    url = "http://localhost:8080/Madhu_Flora_New/admin_deleted_users.jsp?pageIndx=" + ii;
                                                }
                                        %>



                                        <li class="active">
                                            <a href='<%=url%>'><%=ii%></a>
                                        </li>



                                        <%    }

                                        %>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <style>
                            .modal.fade.in {
                                top: 38%;
                            }
                        </style>
                        <div class="modal fade" id="myModal5" style="overflow: hidden; " role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" style="color: black;margin-left: 30%">View User Details</h4>
                                    </div>
                                    <div class="modal-body" style="padding: 0px;color: black;height: 550px; overflow: hidden; max-height: 549px">
                                        <iframe id="ifram1" class="pro-edit-iframe" id="prodetail-if" width="100%" height="100%" scrolling="no" border="0" src="admin_deleted_activity.jsp" frameborder="no">Your browser not support this feature</iframe>
                                    </div>
                                    <div class="modal-footer" style="height: 24px; padding: 10px 15px;">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            function opendet(id) {
                                window.parent.parent.scrollTo(0, 0);
                                var $iframe = $('#ifram1');
                                $iframe.attr('src', "admin_deleted_activity.jsp?pid=" + id + "");
                            }

                        </script>



                        <div class="modal fade" id="myModal" style="overflow: hidden; " role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" style="color: black;margin-left: 30%">View User Details</h4>
                                    </div>
                                    <div class="modal-body" style="padding: 0px;color: black;height: 550px; overflow: hidden; max-height: 549px">
                                        <iframe id="ifram2" class="pro-edit-iframe" id="prodetail-if" width="100%" height="100%" scrolling="no" border="0" src="admin_deleted_view.jsp" frameborder="no">Your browser not support this feature</iframe>
                                    </div>
                                    <div class="modal-footer" style="height: 24px; padding: 10px 15px;">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            function opend(id) {
                                window.parent.parent.scrollTo(0, 0);
                                var $iframe = $('#ifram2');
                                $iframe.attr('src', "admin_deleted_view.jsp?pid=" + id + "");
                            }

                        </script>
                    </div>
                </div>
            </div>
            <footer>
                <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>
            </footer>
        </div>


    </body>
</html>
