<%-- 
    Document   : my_delivery
    Created on : Dec 23, 2015, 11:14:05 AM
    Author     : HP
--%>

<%
    Session ses_crt = Connections.NewHibernateUtil.getSessionFactory().openSession();

    HttpSession sesht = request.getSession();
    if ((POJO.UserReg) sesht.getAttribute("login") != null) {
        POJO.UserReg u = (POJO.UserReg) sesht.getAttribute("login");
        POJO.UserReg ur = (POJO.UserReg) ses_crt.load(POJO.UserReg.class, u.getId());
        int i = 0;
        if (ur != null) {
            i = ur.getId();
        }
        out.print(ur.getUserStatus().getId());
        if (ur.getUserStatus().getId() == 1) {
%>


<%@page import="java.util.ArrayList"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="java.awt.Window"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Shipping Details</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/jquery-ui.css">

        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/jquery-ui.js"></script>
        <link rel="stylesheet" href="css/css_1.css" />
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>       
    </head>


    <body>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>

                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">


                <%                    if (request.getSession().getAttribute("deliveryD") != null) {

                        ArrayList a = (ArrayList) request.getSession().getAttribute("deliveryD");

                        String userId = a.get(0).toString();
                        String r_name = a.get(1).toString();
                        String r_contact = a.get(2).toString();
                        String r_ad1 = a.get(3).toString();
                        String r_ad2 = a.get(4).toString();
                        String city = a.get(5).toString();
                        String date = a.get(6).toString();
                        String time = a.get(7).toString();
                        String message = a.get(8).toString();
                        String istruction = a.get(9).toString();
                        String s_name = a.get(10).toString();
                        String s_email = a.get(11).toString();
                        String s_contact = a.get(12).toString();
                        String send_title = a.get(13).toString();
                        String rec_title = a.get(14).toString();
                        String location = a.get(15).toString();


                %>

                <div id="templatemo_main">

                    <div>
                        <div style=" float: left">
                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Sender's Information</h3></p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="send_title">
                                                <option  value="1" selected="se"><%=send_title%></option>
                                                <option value="1">Mr</option>
                                                <option value="1">Ms</option>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input  id="send_name" type="text" name="name" placeholder="First Name" value='<%=s_name%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="s_name">Name can't be empty</p>
                                    <p class="contact-input">
                                        <input id="send_email" type="text" name="name" placeholder="E mail" value='<%=s_email%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="ma_ll">E-mail can't be empty</p>
                                    <p class="hidden" style="color: black" id="ma_ll_i"> Invalid E-mail</p>
                                    <p class="contact-input">
                                        <input id="send_phone" type="text" name="name" placeholder="Phone Number" value='<%=s_contact%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="phone1">Phone number can't be empty</p>
                                    <p class="hidden" style="color: black" id="phone2">Invalid Phone number</p>
                                    <p class="contact-input">
                                        <textarea name="message" id="instru" placeholder="Instructions:-"><%=istruction%></textarea>
                                    </p>
                                </fieldset>
                            </form>
                        </div>

                        <div style="float: left; margin: 1px">
                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Receiver's Information</h3></p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="rec_title">
                                                <option value="1" selected="se"><%=rec_title%></option>
                                                <option value="1">Mr</option>
                                                <option value="1">Ms</option>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input  id="rec_name" type="text" name="name" placeholder="First Name" value='<%=r_name%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="r_names">Name can't be empty</p>
                                    <p class="contact-input">
                                        <input id="rec_address1" type="text" name="name" placeholder="Address 1" value='<%=r_ad1%>'>
                                    </p>
                                    <p class="contact-input">
                                        <input id="rec_address2" type="text" name="name" placeholder="Address 2" value='<%=r_ad2%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="addr_1">Fill at least one address field</p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="city">
                                                <option value="1" selected="se"><%=city%></option>
                                                <%
                                                    Criteria citycr = ses_crt.createCriteria(POJO.DeliveryCity.class);
                                                    List<POJO.DeliveryCity> dli = citycr.list();
                                                    for (POJO.DeliveryCity d : dli) {
                                                %>
                                                <option value="1"><%=d.getCity()%></option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </label>
                                    </p>

                                    <p class="contact-input">
                                        <input id="rec_phone" type="text" name="name" placeholder="Phone Number" value='<%=r_contact%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="phone3">Phone number can't be empty</p>
                                    <p class="hidden" style="color: black" id="phone4">Invalid phone number</p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="location">
                                                <option  value="1" selected="se"><%=location%></option>
                                                <option value="1">Home</option>
                                                <option value="1">Flat</option>
                                                <option value="1">Office</option>
                                                <option value="1">Hospital</option>
                                                <option value="1">Other</option>
                                            </select>
                                        </label>
                                    </p>
                                </fieldset>
                            </form>
                        </div>

                        <div style="float: left">
                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Delivery Information</h3></p>
                                    <p class="contact-input">
                                        <textarea name="message" id="message" placeholder="Message:- Ex.Wish you a happy BirthDay"><%=message%></textarea>
                                    </p>

                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="doption">
                                                <option  selected="se" value="1"><%=time%></option>
                                                <option value="1">Day</option>
                                                <option value="1">Early Morning</option>
                                                <option value="1">Morning</option>
                                                <option value="1">Afternoon</option>
                                                <option value="1">Late evening</option>
                                                <option value="1">Mid Night</option>
                                            </select>
                                        </label>
                                    </p>


                                    <p class="contact-input">Pick a Delivery Date: <input type="text" id="datepicker" name="date" value='<%=date%>'/></p>
                                    <p class="hidden" style="color: black" id="dates">Date can't be empty</p>

                                    <script>

                                        $(document).ready(
                                                function () {                                                     
                                                    var date = new Date();
                                                    var d=new Date(date.setTime( date.getTime() + 2 * 86400000 ));
                                                    $("#datepicker").datepicker({
                                                        minDate: d,
                                                        changeMonth: true, //this option for allowing user to select month
                                                        changeYear: false //this option for allowing user to select from year range
                                                    });
                                                }

                                        );
                                                                    
                                    </script>


                                    <br><br><br><br><br>
                                    <p class="contact-submit">
                                        <a onclick="c()" class="New-lassana-btn">Go To Next &nbsp;<img style="vertical-align: middle" src="images/index.png"></a>
                                        <br>
                                        <br>
                                        <a id="conshopping-btn"  href="my_cart.jsp" class="New-lassana-btn"><img style="vertical-align: middle" src="images/index2.png">Back To Cart</a>
                                    </p>
                                </fieldset>

                            </form>
                        </div>

                    </div>
                    <div class="cleaner"></div>
                </div> <!-- END of main -->


                <%                } else {
                %>





                <div id="templatemo_main">


                    <div>

                        <div style=" float: left">
                            <%
                                POJO.UserReg us = (POJO.UserReg) ses_crt.load(POJO.UserReg.class, ur.getId());

                                Criteria my_cr = ses_crt.createCriteria(POJO.DefaultBilling.class);
                                my_cr.add(Restrictions.eq("userReg", us));
                                List<POJO.DefaultBilling> deft_my = my_cr.list();

                                Criteria senderinf = ses_crt.createCriteria(POJO.SendersInfo.class);
                                senderinf.add(Restrictions.eq("userReg", us));
                                senderinf.addOrder(Order.desc("id"));
                                List<POJO.SendersInfo> sendlist = senderinf.list();

                                if (sendlist.isEmpty() && deft_my.isEmpty()) {


                            %>
                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Sender's Information</h3></p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="send_title">
                                                <option value="1">Mr</option>
                                                <option value="1">Ms</option>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input  id="send_name" type="text" name="name" placeholder="First Name">
                                    </p>
                                    <p class="hidden" style="color: black" id="s_name">Name can't be empty</p>
                                    <p class="contact-input">
                                        <input id="send_email" type="text" name="name" placeholder="E mail">
                                    </p>
                                    <p class="hidden" style="color: black" id="ma_ll">E-mail can't be empty</p>
                                    <p class="hidden" style="color: black" id="ma_ll_i"> Invalid E-mail</p>
                                    <p class="contact-input">
                                        <input id="send_phone" type="text" name="name" placeholder="Phone Number">
                                    </p>
                                    <p class="hidden" style="color: black" id="phone1">Phone number can't be empty</p>
                                    <p class="hidden" style="color: black" id="phone2">Invalid Phone number</p>
                                    <p class="contact-input">
                                        <textarea name="message" id="instru" placeholder="Instructions:-"></textarea>
                                    </p>
                                </fieldset>
                            </form>
                            <%                                }

                            %>

                            <!-- -->

                            <%                                //Object obj = null;
                                POJO.SendersInfo obj = null;
                                POJO.DefaultBilling bilng = null;

                                if ((!sendlist.isEmpty()) && deft_my.isEmpty()) {
                                    lopbr:
                                    for (POJO.SendersInfo s : sendlist) {
                                        obj = (POJO.SendersInfo) ses_crt.load(POJO.SendersInfo.class, s.getId());
                                        break lopbr;
                                    }
                                }

                                if ((!sendlist.isEmpty()) && (!deft_my.isEmpty()) || (sendlist.isEmpty()) && (!deft_my.isEmpty())) {
                                    lp2:
                                    for (POJO.DefaultBilling l1 : deft_my) {
                                        bilng = (POJO.DefaultBilling) ses_crt.load(POJO.DefaultBilling.class, l1.getId());
                                        break lp2;
                                    }

                                    // 
                                }
                                if (obj != null) {

                            %>
                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Sender's Information</h3></p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="send_title">
                                                <option value="1" selected="se"><%=obj.getTitle()%></option>
                                                <option value="1">Mr</option>
                                                <option value="1">Ms</option>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input  id="send_name" type="text" name="name" placeholder="First Name" value='<%=obj.getName()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="s_name">Name can't be empty</p>
                                    <p class="contact-input">
                                        <input id="send_email" type="text" name="name" placeholder="E mail" value='<%=obj.getEmail()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="ma_ll">E-mail can't be empty</p>
                                    <p class="hidden" style="color: black" id="ma_ll_i"> Invalid E-mail</p>
                                    <p class="contact-input">
                                        <input id="send_phone" type="tel" name="name" placeholder="Phone Number" value='<%=obj.getPhoneNum()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="phone1">Phone number can't be empty</p>
                                    <p class="hidden" style="color: black" id="phone2">Invalid Phone number</p>
                                    <p class="contact-input">
                                        <textarea name="message" id="instru" placeholder="Instructions:-"></textarea>
                                    </p>
                                </fieldset>
                            </form>


                            <%

                                }

                                if (bilng != null) {
                            %>

                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Sender's Information</h3></p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="send_title">
                                                <option value="1" selected="se"><%=bilng.getTitle()%></option>
                                                <option value="1">Mr</option>
                                                <option value="1">Ms</option>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input  id="send_name" type="text" name="name" placeholder="First Name" value='<%=bilng.getName()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="s_name">Name can't be empty</p>
                                    <p class="contact-input">
                                        <input id="send_email" type="text" name="name" placeholder="E mail" value='<%=bilng.getEmail()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="ma_ll">E-mail can't be empty</p>
                                    <p class="hidden" style="color: black" id="ma_ll_i"> Invalid E-mail</p>
                                    <p class="contact-input">
                                        <input id="send_phone" type="text" name="name" placeholder="Phone Number" value='<%=bilng.getPhoneNum()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="phone1">Phone number can't be empty</p>
                                    <p class="hidden" style="color: black" id="phone2">Invalid Phone number</p>
                                    <p class="contact-input">
                                        <textarea name="message" id="instru" placeholder="Instructions:-"></textarea>
                                    </p>
                                </fieldset>
                            </form>
                            <%
                                }
                            %>
                            <!--  -->
                        </div>





                        <div style="float: left; margin: 1px">

                            <%
                                Criteria mycri_cri2 = ses_crt.createCriteria(POJO.DefaultShipping.class);
                                mycri_cri2.add(Restrictions.eq("userReg", us));
                                List<POJO.DefaultShipping> my_list_2 = mycri_cri2.list();

                                Criteria mycri_3 = ses_crt.createCriteria(POJO.RecipientInfo.class);
                                mycri_3.add(Restrictions.eq("userReg", us));
                                mycri_3.addOrder(Order.desc("id"));
                                List<POJO.RecipientInfo> my_lst_3 = mycri_3.list();

                                if (my_list_2.isEmpty() && my_lst_3.isEmpty()) {
                            %>


                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Receiver's Information</h3></p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="rec_title">
                                                <option value="1">Mr</option>
                                                <option value="1">Ms</option>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input  id="rec_name" type="text" name="name" placeholder="First Name">
                                    </p>
                                    <p class="hidden" style="color: black" id="r_names">Name can't be empty</p>
                                    <p class="contact-input">
                                        <input id="rec_address1" type="text" name="name" placeholder="Address 1">
                                    </p>
                                    <p class="contact-input">
                                        <input id="rec_address2" type="text" name="name" placeholder="Address 2">
                                    </p>
                                    <p class="hidden" style="color: black" id="addr_1">Fill at least one address field</p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="city">

                                                <%
                                                    Criteria cr = ses_crt.createCriteria(POJO.DeliveryCity.class);
                                                    List<POJO.DeliveryCity> lil = cr.list();
                                                    for (POJO.DeliveryCity cit : lil) {


                                                %>

                                                <option value="1"><%=cit.getCity()%></option>

                                                <%
                                                    }
                                                %>
                                            </select>
                                        </label>
                                    </p>

                                    <p class="contact-input">
                                        <input id="rec_phone" type="text" name="name" placeholder="Phone Number">
                                    </p>
                                    <p class="hidden" style="color: black" id="phone3">Phone number can't be empty</p>
                                    <p class="hidden" style="color: black" id="phone4">Invalid phone number</p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select  name="subject" id="location">
                                                <option value="1">Home</option>
                                                <option value="1">Flat</option>
                                                <option value="1">Office</option>
                                                <option value="1">Hospital</option>
                                                <option value="1">Other</option>
                                            </select>
                                        </label>
                                    </p>


                                </fieldset>
                            </form>

                            <%
                                }

                                POJO.DefaultShipping def_spng = null;
                                POJO.RecipientInfo rec_inf = null;

                                if ((!my_lst_3.isEmpty()) && my_list_2.isEmpty()) {
                                    lopbr:
                                    for (POJO.RecipientInfo r : my_lst_3) {
                                        rec_inf = (POJO.RecipientInfo) ses_crt.load(POJO.RecipientInfo.class, r.getId());
                                        break lopbr;
                                    }
                                }

                                if ((!my_lst_3.isEmpty()) && (!my_list_2.isEmpty()) || (my_lst_3.isEmpty()) && (!my_list_2.isEmpty())) {
                                    lp2:
                                    for (POJO.DefaultShipping l : my_list_2) {
                                        def_spng = (POJO.DefaultShipping) ses_crt.load(POJO.DefaultShipping.class, l.getId());
                                        break lp2;
                                    }

                                    // 
                                }

                                if (rec_inf != null) {
                            %>
                            <!--  -->



                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Receiver's Information</h3></p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="rec_title">
                                                <option value="1" selected="se"><%=rec_inf.getTitle()%></option>
                                                <option value="1">Mr</option>
                                                <option value="1">Ms</option>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input  id="rec_name" type="text" name="name" placeholder="First Name" value='<%=rec_inf.getName()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="r_names">Name can't be empty</p>
                                    <p class="contact-input">
                                        <input id="rec_address1" type="text" name="name" placeholder="Address 1" value='<%=rec_inf.getAddress()%>'>
                                    </p>
                                    <p class="contact-input">
                                        <input id="rec_address2" type="text" name="name" placeholder="Address 2" value='<%=rec_inf.getAddress2()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="addr_1">Fill at least one address field</p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="city">
                                                <option value="1" selected="se"><%=rec_inf.getDeliveryCity().getCity()%></option>
                                                <%

                                                    Criteria cr1 = ses_crt.createCriteria(POJO.DeliveryCity.class);
                                                    List<POJO.DeliveryCity> lil = cr1.list();
                                                    for (POJO.DeliveryCity ci : lil) {


                                                %>

                                                <option value="1"><%=ci.getCity()%></option>

                                                <%
                                                    }
                                                %>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input id="rec_phone" type="text" name="name" placeholder="Phone Number" value='<%=rec_inf.getPhoneNum()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="phone3">Phone number can't be empty</p>
                                    <p class="hidden" style="color: black" id="phone4">Invalid phone number</p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="location">
                                                <option  value="1" selected="se"><%=rec_inf.getLocationType()%></option>
                                                <option value="1">Home</option>
                                                <option value="1">Flat</option>
                                                <option value="1">Office</option>
                                                <option value="1">Hospital</option>
                                                <option value="1">Other</option>
                                            </select>
                                        </label>
                                    </p>
                                </fieldset>
                            </form>


                            <%
                                }
                                if (def_spng != null) {
                            %>


                            <!--  -->

                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Receiver's Information</h3></p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="rec_title">
                                                <option value="1" selected="se"><%=def_spng.getTitle()%></option>
                                                <option value="1">Mr</option>
                                                <option value="1">Ms</option>
                                            </select>
                                        </label>
                                    </p>
                                    <p class="contact-input">
                                        <input  id="rec_name" type="text" name="name" placeholder="First Name" value='<%=def_spng.getName()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="r_names">Name can't be empty</p>
                                    <p class="contact-input">
                                        <input id="rec_address1" type="text" name="name" placeholder="Address 1" value='<%=def_spng.getAddress()%>'>
                                    </p>
                                    <p class="contact-input">
                                        <input id="rec_address2" type="text" name="name" placeholder="Address 2" value='<%=def_spng.getAddress2()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="addr_1">Fill at least one address field</p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="city">
                                                <option value="1" selected="se"><%=def_spng.getDeliveryCity().getCity()%></option>


                                                <%

                                                    Criteria cr2 = ses_crt.createCriteria(POJO.DeliveryCity.class);
                                                    List<POJO.DeliveryCity> lil = cr2.list();
                                                    for (POJO.DeliveryCity c : lil) {


                                                %>

                                                <option value="1"><%=c.getCity()%></option>

                                                <%
                                                    }
                                                %>
                                            </select>
                                        </label>
                                    </p>

                                    <p class="contact-input">
                                        <input id="rec_phone" type="text" name="name" placeholder="Phone Number" value='<%=def_spng.getPhoneNum()%>'>
                                    </p>
                                    <p class="hidden" style="color: black" id="phone3">Phone number can't be empty</p>
                                    <p class="hidden" style="color: black" id="phone4">Invalid phone number</p>
                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="location">
                                                <option value="1"  selected="se"><%=def_spng.getLocationType()%></option>
                                                <option value="1">Home</option>
                                                <option value="1">Flat</option>
                                                <option value="1">Office</option>
                                                <option value="1">Hospital</option>
                                                <option value="1">Other</option>
                                            </select>
                                        </label>
                                    </p>
                                </fieldset>
                            </form>
                            <%
                                }
                            %>
                        </div>
                        <div style="float: left">
                            <form action="" class="contact">
                                <fieldset class="contact-inner">
                                    <p><h3 style="color: black;margin-left: 35px">Delivery Information</h3></p>
                                    <p class="contact-input">
                                        <textarea name="message" id="message" placeholder="Message:- Ex.Wish you a happy BirthDay"></textarea>
                                    </p>

                                    <p class="contact-input">
                                        <label for="select" class="select">
                                            <select name="subject" id="doption">
                                                <option value="1">Day</option>
                                                <option value="1">Early Morning</option>
                                                <option value="1">Morning</option>
                                                <option value="1">Afternoon</option>
                                                <option value="1">Late evening</option>
                                                <option value="1">Mid Night</option>
                                            </select>
                                        </label>
                                    </p>


                                    <p class="contact-input">Pick a Delivery Date: <input type="text" id="datepicker" name="date"/></p>
                                    <p class="hidden" style="color: black" id="dates">Date can't be empty</p>

                                    <script>

                                        $(document).ready(
                                                function () {
//                                                     
                                                    var date = new Date();
                                                    var d=new Date(date.setTime( date.getTime() + 2 * 86400000 ));
                                                    $("#datepicker").datepicker({
                                                        minDate: d,
                                                        changeMonth: true, //this option for allowing user to select month
                                                        changeYear: false //this option for allowing user to select from year range
                                                    });
                                                }

                                        );
                                                                    
                                    </script>


                                    <br><br><br>
                                    <p class="contact-submit">
                                        <a onclick="c()" class="New-lassana-btn">Go To Next &nbsp;<img style="vertical-align: middle" src="images/index.png"></a>
                                        <br>
                                        <br>
                                        <a id="conshopping-btn"  href="my_cart.jsp" class="New-lassana-btn"><img style="vertical-align: middle" src="images/index2.png">Back To Cart</a>
                                    </p>


                                </fieldset>
                                <%
                                    ses_crt.close();


                                %>


                            </form>
                        </div>

                    </div>

                    <div class="cleaner"></div>
                </div> <!-- END of main -->

                <%                    }
                %>



                <script type="text/javascript">

                    function c() {

                        // alert("aaa");

                        var user = '<%=i%>';
                        var r_name = document.getElementById("rec_name").value;
                        var r_cont = document.getElementById("rec_phone").value;
                        var r_ad1 = document.getElementById("rec_address1").value;
                        var r_ad2 = document.getElementById("rec_address2").value;
                        var r_city = $('#city').find('option:selected').text();
                        var date = document.getElementById("datepicker").value;
                        var time = $('#doption').find('option:selected').text();
                        var msg = $('#message').val();
                        var instr = $('#instru').val();
                        var s_name = document.getElementById("send_name").value;
                        var s_email = document.getElementById("send_email").value;
                        var s_contact = document.getElementById("send_phone").value;
                        var location = $('#location').find('option:selected').text();
                        var s_ti = $('#send_title').find('option:selected').text();
                        var r_ti = $('#rec_title').find('option:selected').text();


                        if (s_name == "") {
                            $('#s_name').show();
                            s_names = false;
                        } else {
                            s_names = true;
                        }

                        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        if (s_email == "") {
                            $('#ma_ll').show();
                            emailfil = false;
                        } else {
                            emailfil = true;
                            if (!(filter.test(s_email))) {
                                $('#ma_ll_i').show();
                                emailch = false;
                            } else {
                                emailch = true;
                            }
                        }

                        var phonefil = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/;
                        if (s_contact == "") {
                            $('#phone1').show();
                            s_phn = false;
                        } else {
                            s_phn = true;
                            if (!(phonefil.test(s_contact))) {
                                $('#phone2').show();
                                s_phn_val = false;
                            } else {
                                s_phn_val = true;
                            }
                        }

                        if (r_name == "") {
                            $('#r_names').show();
                            rname = false;
                        } else {
                            rname = true;
                        }
                        if (r_ad1 == "" && r_ad2 == "") {
                            $('#addr_1').show();
                            addr = false;
                        } else {
                            addr = true;
                        }

                        if (r_cont == "") {
                            $('#phone3').show();
                            r_phn = false;
                        } else {
                            r_phn = true;
                            if (!(phonefil.test(r_cont))) {
                                $('#phone4').show();
                                r_phn_val = false;
                            } else {
                                r_phn_val = true;
                            }
                        }

// $('#dates').prop("style","border: 1px solid red;");
                        if (date == "") {
                            $('#dates').show();
                            dat = false;
                        } else {
                            dat = true;
                        }

                        if (s_names == true && emailch == true && s_phn_val == true && rname == true && addr == true && r_phn_val == true && dat == true) {
                            //alert("awaa");
                            $.post(
                                    "delivery_details",
                                    {rec_name: r_name,
                                        rec_phone: r_cont,
                                        rec_address1: r_ad1,
                                        rec_address2: r_ad2,
                                        city: r_city,
                                        date: date,
                                        time: time,
                                        message: msg,
                                        instru: instr,
                                        send_name: s_name,
                                        send_email: s_email,
                                        send_phone: s_contact,
                                        location: location,
                                        send_title: s_ti,
                                        rec_title: r_ti,
                                        user: user
                                    },
                            function (res) {

                                window.location.replace("checkout_page.jsp");


                            }
                            );
                        }

                    }

                </script>
            </div> <!-- END of main wrapper -->


            <%@include file="my_footer.jsp" %>
        </div>                

    </body>

</html>

<%//
        } else {
            request.getSession().invalidate();
            response.sendRedirect("index.jsp");
        }
    } else {
        response.sendRedirect("User_login.jsp");
    }
%>