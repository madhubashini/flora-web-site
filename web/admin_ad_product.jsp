<%-- 
    Document   : admin_ad_product
    Created on : Mar 23, 2016, 5:28:09 PM
    Author     : HP
--%>

<%
    String num = null;
    if (request.getSession().getAttribute("login") != null) {
        POJO.UserReg logu = (POJO.UserReg) request.getSession().getAttribute("login");
        num = logu.getUserType().getId() + "";
        System.out.print("if eke");

        if (num.equals("2")) {


%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/fullcalendar.css' rel='stylesheet'>
        <link href='admin_css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link href='admin_css/uploadify.css' rel='stylesheet'>
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="admin_image/favicon.ico">

    </head>
    <body>
        <!-- topbar start -->

        <%@include file="admin_header.jsp" %>

        <!-- topbar ends -->
        <div class="container-fluid">

            <div class="row-fluid">
                <!-- left menu starts -->

                <%@include file="admin_leftBar.jsp" %>
                <!-- left menu ends -->
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <p style="font-size: 30px;margin-left: 50%;margin-top: 5%">Add New Product</p>
                    </div>
                    <div class="row-fluid sortable">

                        <div class="box span12" style="width:100% ;margin-left: 21%">
                            <div class="box-content">
                                <form id="form1" class="form-horizontal" method="post" action="add_new_product" enctype="multipart/form-data">
                                    <br>
                                    <fieldset>
                                        <div id="imageload2" class="control-group">
                                            <label class="control-label" for="focusedInput"></label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="loadedimage2" src='' width="150px" height="150px">
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="fileInput">Image</label>
                                            <div class="controls">
                                                <div class="" id="uniform-fileInput">
                                                    <input type="file" id="files2" name="files[]" value=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="hide" id="im" style="color:red">Image can't be empty</label>
                                        <div class="control-group">
                                            <label class="control-label" for="focusedInput">Item Name</label>
                                            <div class="controls">
                                                <input class="input-xlarge focused" id="name" type="text" name="name">
                                            </div>
                                            <label class="hide" id="na" style="color:red">Name can't be empty</label>
                                            <label class="hide" id="na_le" style="color:red">Name can't be more than 20</label>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="focusedInput">Items</label>
                                            <div class="controls">
                                                <input class="input-xlarge focused" id="item" type="number" name="item">
                                            </div>
                                        </div>
                                        <label class="hide" id="it" style="color:red">Item can't be empty</label>
                                        <label class="hide" id="itm" style="color:red">Item should be numeric</label>
                                        <div class="control-group">
                                            <label class="control-label" for="focusedInput">Weight</label>
                                            <div class="controls">
                                                <input class="input-xlarge focused" id="weit" type="text" name="weight">&nbsp;Kg
                                            </div>
                                        </div>
                                        <label class="hide" id="weig" style="color:red">Weight can't be empty</label>
                                        <div class="control-group">
                                            <label class="control-label" for="focusedInput">Unit Price</label>
                                            <div class="controls">
                                                <input class="input-xlarge focused" id="untp" type="text" name="untp">&nbsp;Rs.
                                            </div>
                                        </div>
                                        <label class="hide" id="pr_emp" style="color:red">Price can't be empty</label>
                                        <label class="hide" id="pr_deci" style="color:red">Price Should be in decimal format EX= 0.00</label>

                                        <div class="control-group">
                                            <label class="control-label" for="focusedInput">Promotion</label>
                                            <div class="controls">
                                                <select id="prom" data-rel="chosen" name="promo">
                                                    <option value="" selected="selected">Select</option>
                                                    <%                                                        Session session_3 = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                                        POJO.DeleteStatus de = (POJO.DeleteStatus) session_3.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                                                        Criteria cri_3 = session_3.createCriteria(POJO.Promotionn.class);
                                                        cri_3.add(Restrictions.eq("deleteStatus", de));
                                                        List<POJO.Promotionn> list_1_1 = cri_3.list();
                                                        for (POJO.Promotionn prms : list_1_1) {
                                                            if (prms.getDiscountPersentage().equals("0")) {
                                                    %>

                                                    <option value="<%=prms.getId()%>">No Promotion</option>
                                                    <%
                                                    } else {
                                                    %>
                                                    <option value="<%=prms.getId()%>"><%=prms.getDiscountPersentage()%> %</option>

                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="hide" id="prom_p" style="color:red">Promotion can't be empty</label>
                                        <div class="control-group">
                                            <label class="control-label" for="focusedInput">Sub Category</label>
                                            <div class="controls">
                                                <select id="sub" data-rel="chosen" name="catg">
                                                    <option value="" selected="selected">Select</option>
                                                    <%
                                                        Criteria cri_4 = session_3.createCriteria(POJO.SubCatogory.class);
                                                        POJO.DeleteStatus d = (POJO.DeleteStatus) session_3.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                                                        cri_4.add(Restrictions.eq("deleteStatus", d));
                                                        List<POJO.SubCatogory> lists_2 = cri_4.list();
                                                        for (POJO.SubCatogory prm : lists_2) {
                                                            if (prm.getCatogory().getDeleteStatus().equals(d)) {

                                                    %>
                                                    <option value='<%=prm.getId()%>'><%=prm.getSubCatogory()%> &nbsp;-&nbsp;<%=prm.getCatogory().getCatogory()%></option>

                                                    <%
                                                            }
                                                        }
                                                    %>

                                                </select>
                                            </div>
                                        </div>
                                        <label class="hide" id="su_ca" style="color:red">Category can't be empty</label>
                                        <div class="control-group">
                                            <label class="control-label" for="textarea2">Description</label>
                                            <div class="controls">
                                                <textarea class="cleditor" id="des" rows="5" name="desc" style="width: 53%"></textarea>
                                            </div>
                                        </div>
                                        <label class="hide" id="d" style="color:red">Description can't be empty</label>
                                        <label class="hide" id="d_le" style="color:red">length can't be more than 100</label>
                                        <div style="float: right;margin-right: 15px">
                                            <button type="button" class="btn btn-primary" onclick="submtf()">Add New</button>
                                            <button class="btn"><a href="admin_ad_product.jsp" style="color: black">Cancel</a></button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <script>
                            function submtf() {

                                var img = document.getElementById("files2").value;
                                var name = document.getElementById("name").value;
                                var ite = document.getElementById("item").value;
                                var wei = document.getElementById("weit").value;
                                var unt = document.getElementById("untp").value;
                                var pro = $('#prom').find('option:selected').val();
                                var sub = $('#sub').find('option:selected').val();
                                var des = document.getElementById("des").value;
                                var fil = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/;

                                if (img == "") {
                                    $('#im').show();
                                    im = false;
                                } else {
                                    im = true;
                                }

                                if (name == "") {
                                    $('#na').show();
                                    pname = false;
                                } else {
                                    pname = true;
                                    if (name.length > 20) {
                                        $('#na_le').show();
                                        ple = false;
                                    } else {
                                        ple = true;
                                    }
                                }
                                if (ite == "") {
                                    $('#it').show();
                                    pitem = false;
                                } else {
                                    pitem = true;
                                }
                                if (wei == "") {
                                    $('#weig').show();
                                    pwei = false;
                                } else {
                                    pwei = true;
                                }
                                if (unt == "") {
                                    $('#pr_emp').show();
                                    punt = false;
                                } else {
                                    punt = true;
                                    var numasd = parseFloat(unt);
                                    if ((!Number.isNaN(numasd) && numasd.toFixed(2).toString() === unt) == false) {
                                        $('#pr_deci').show();
                                        prc = false;
                                    } else {
                                        prc = true;
                                    }
                                }
                                if (sub == "") {
                                    $('#su_ca').show();
                                    psu = false;
                                } else {
                                    psu = true;
                                }
                                if (des == "") {
                                    $('#d').show();
                                    psu = false;
                                } else {
                                    psu = true;
                                    if (des.length > 600) {
                                        $('#d_le').show();
                                        de = false;
                                    } else {
                                        de = true;
                                    }
                                }
                                if (pro == "") {
                                    $('#prom_p').show();
                                    pmr = false;
                                } else {
                                    pmr = true;
                                }
                                if (pname == true && ple == true && pitem == true && pwei == true && punt == true &&
                                        psu == true && de == true && prc == true && pmr == true) {
                                    $('#form1').submit();
                                    alert("Successfully Saved");

                                }

                            }
                        </script>
                        <script>

                            function handleFileSelect2(evt) {
                                $('#loadedimage2').remove();
                                var files = evt.target.files; // FileList object

                                // Loop through the FileList and render image files as thumbnails.
                                for (var i = 0, f; f = files[i]; i++) {

                                    // Only process image files.
                                    if (!f.type.match('image.*')) {
                                        continue;
                                    }

                                    var reader = new FileReader();
                                    // Closure to capture the file information.
                                    reader.onload = (function (theFile) {
                                        return function (e) {
                                            // Render thumbnail.
                                            var span = document.createElement('span');
                                            span.innerHTML = ['<img id="loadedimage2" class="thumb" src="', e.target.result,
                                                '" title="', escape(theFile.name), '"  width="150px" height="150px"/>'].join('');
                                            document.getElementById('imageload2').insertBefore(span, null);
                                        };
                                    })(f);

                                    // Read in the image file as a data URL.
                                    reader.readAsDataURL(f);
                                }
                            }

                            document.getElementById('files2').addEventListener('change', handleFileSelect2, false);
                        </script>


                    </div>
                </div>
            </div>
            <footer>
                <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>
            </footer>
        </div>
    </body>
</html>

<%
        }

    } else {
        response.sendRedirect("index.jsp");
    }

%>