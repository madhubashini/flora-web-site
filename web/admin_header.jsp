<%-- 
    Document   : admin_header
    Created on : Mar 14, 2016, 4:47:47 PM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <!-- topbar starts -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="index.html"> <img alt="Charisma Logo" src="images/flower_b.png" /></a>

                <!-- theme selector starts -->
                <div class="btn-group pull-right" style="padding: 20px 5px 1px">
                    <a class="btn dropdown-toggle" onclick="logout()" >Logout</a>
                </div>
                <script>
                    function logout(){
                    $.post(
                            "logout_admin",
                            function (rs){
                              window.location.replace("index.jsp");  
                            }
                            );
                }
                </script>
                    
                <!-- theme selector ends -->

                <!-- user dropdown starts -->

                <div class="btn-group pull-right theme-container" style="padding: 20px 5px 1px">
<!--                    <a class="btn dropdown-toggle"  href="#">Admin Profile</a>-->
                    <a class="btn dropdown-toggle"  href="index.jsp">Visit Site >></a>
                </div>
                <!-- user dropdown ends -->

                <div class="top-nav nav-collapse">
                    <div class="btn-group pull-left" >
                    
                </div>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
</html>
