<%-- 
    Document   : enter_email
    Created on : Jun 17, 2016, 3:47:55 PM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" href="css/check.css">
        <link rel="stylesheet" href="css/loginpage.css">


        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>                 
        <script type="text/JavaScript" src="js/slimbox2.js"></script>           
        <script type="text/javascript" src="js/my_javascripts.js"></script> 
        <script type="text/javascript" src="js/jquery-1-4-2.min.js"></script>  
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>   
        <script type="text/javascript" src="js/jquery.min.js"></script>

    </head>
    <body>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#">Floral Shop</a></div>
                    <%@include file="my_header.jsp" %>


                </div> <!-- END of header -->
            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>
                    <div id="content" class="right" >
                        <br>
                        <br>
                        <div class="form">
                            <div id="signupss">   
                                <h1>Reset Password</h1>

                                <form action="" method="">
                                    <p style="color: white;font-size: 16px">
                                        You can recover your password by send an E-mail .
                                        If you have an account with us that you have entered E-mail , 
                                        you will receive a link . Click the link and enter your new password
                                        then your new password will be replace .
                                    </p>
                                    <br>
                                    <div class="field-wrap">
                                        <input id="emails"  value="" type="text" autocomplete="on" placeholder="Enter your Account E-mail">
                                    </div>
                                    <P style="color: red" id="invalid" class="hidden">Invalid email.Please Try Again</p>
                                    <P style="color: red" id="invalid2" class="hidden">You Don't Have Account With This Email</p>
                                    <p style="color: red" id="pendi" class="hidden">Confirm your E-mail first</p>
                                    <p style="color: red" id="inct" class="hidden">Please contact your Administrator</p>
                                    <p style="color: red" id="deact" class="hidden">Re-activate your account first</p>
                                    <p style="color: red" id="sent" class="hidden">Please Check Your Mails</p>

                                    <ul class="tab-group">
                                        <li><a id="fgt_psw_submit" class="tab">Send E-mail</a></li>  
                                    </ul>

                                    <br>
                                    <br>

                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="cleaner"></div>

                </div>
            </div>
            <script>

                $(document).ready(function () {
                    $('#fgt_psw_submit').click(function () {

                        var frgtmail = document.getElementById("emails").value;
                        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;


                        if (!filter.test(frgtmail)) {
                            $("#invalid").show();

                        } else {

                            $.post(
                                    "ent_email",
                                    {email: frgtmail},
                            function (result) {
                                var methodpara = result.toString();

                                if (methodpara == 1) {
                                    $('#sent').show();
                                }
                                if (methodpara == 2) {
                                    $('#inct').show();
                                }
                                if (methodpara == 3) {
                                    $('#pendi').show();
                                }
                                if (methodpara == 4) {
                                    $('#deact').show();
                                }
                                if (methodpara == 5) {
                                    $('#invalid2').show();
                                }
                            });
                        }

                    });
                });

            </script>
            <%@include file="my_footer.jsp" %>
        </div>
    </body>
</html>
