<%-- 
    Document   : checkout_page
    Created on : Feb 14, 2016, 5:28:26 PM
    Author     : HP
--%>
<%
    Session adsession = Connections.NewHibernateUtil.getSessionFactory().openSession();

    POJO.UserReg u = (POJO.UserReg) request.getSession().getAttribute("login");

    if (u != null) {
        POJO.UserReg ur = (POJO.UserReg) adsession.load(POJO.UserReg.class, u.getId());
                if (ur.getUserStatus().getId() == 1) {

%>


<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="servlets.Added_cart_items"%>
<%@page import="servlets.Keep_cart"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Checkout</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" href="css/check.css">

        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>       
        <script type="text/javascript" src="js/code.jquery.com_jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="js/my_javascripts.js"></script> 
    </head>
    <body>

        <div id="templatemo_wrapper_sp">
            <div id="templatemo_header_wsp">
                <div id="templatemo_header" class="header_subpage">
                    <div id="site_title"><a href="#"> Madhu Flora</a></div>


                    <%@include file="my_header.jsp" %>
                </div> <!-- END of header -->

            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">
                    <%@include file="my_sidebar.jsp" %>




                    <%                        ArrayList a = (ArrayList) request.getSession().getAttribute("deliveryD");

                        out.print(a.get(0));

                        String userId = a.get(0).toString();
                        String r_name = a.get(1).toString();
                        String r_contact = a.get(2).toString();
                        String r_ad1 = a.get(3).toString();
                        String r_ad2 = a.get(4).toString();
                        String city = a.get(5).toString();
                        String date = a.get(6).toString();
                        String time = a.get(7).toString();
                        String message = a.get(8).toString();
                        String istruction = a.get(9).toString();
                        String s_name = a.get(10).toString();
                        String s_email = a.get(11).toString();
                        String s_contact = a.get(12).toString();
                        String send_title = a.get(13).toString();
                        String rec_title = a.get(14).toString();
                        String location = a.get(15).toString();

                        DecimalFormat pas = new DecimalFormat("0.00");

                        //out.print(s);
                    %>



                    <div id="content" class="right">    

                        <form action="" class="contact" style="width: 685px; ">

                            <fieldset class="contact-inner" style="width: 630px; height: auto">
                                <p>
                                <h1 style="color: black; margin-left: 155px">Order Summery </h1>
                                </p>
                                <br>

                                <table>
                                    <tr>
                                        <td><h4 style="color: black;margin-left: 5px">Ordered Items</h4></td>
                                        <td><h4 style="color: black;">:</h4></td>
                                    </tr>
                                </table>

                                <table class="table1" style="width: 75%">
                                    <thead>
                                        <tr>
                                            <th><h4 style="color: black;">Item Name</h4></th>
                                    <th><h4 style="color: black;">Quantity</h4></th>
                                    <th><h4 style="color: black;">Status</h4></th>
                                    </tr>
                                    </thead>



                                    <%                                            double tot = 0.0;
                                        double total = 0.00;
                                        double discout;
                                        double subtot;
                                        double subtotal = 0.0;

                                        Keep_cart kpc = (Keep_cart) request.getSession().getAttribute("shopping_cart");
                                        POJO.DeleteStatus del = (POJO.DeleteStatus) adsession.load(POJO.DeleteStatus.class, Integer.parseInt("1"));
                                        if (kpc != null) {
                                            List<Added_cart_items> adc = kpc.returnArray();
                                    %>

                                    <%
                                        for (Added_cart_items add : adc) {

                                            Criteria crtmt = adsession.createCriteria(POJO.Products.class);
                                            //add.getCart_product_id()
                                            crtmt.add(Restrictions.eq("id", add.getCart_product_id()));
                                            POJO.Products pj = (POJO.Products) crtmt.uniqueResult();

                                            double num = Double.parseDouble(pj.getUnitPrice());

                                            total = (double) add.getCart_product_qty() * Double.parseDouble(pj.getUnitPrice());

                                            String ds = pj.getPromotionn().getDeleteStatus().getStatus();

                                            if (ds.equals("Active")) {
                                                // out.print("if");
                                                discout = Double.parseDouble(pj.getPromotionn().getDiscountPersentage()) * total / 100;
                                                //out.print(discout+"discntEka");
                                                subtot = total - discout;
                                            } else {
                                                //out.print("else");
                                                subtot = total;
                                            }

                                            subtotal += subtot;
                                    %>
                                    <tbody>
                                        <tr>
                                            <td><h4 style="color: black"><%=pj.getName()%></h4></td>
                                            <td><h4 style="color: black"><%=add.getCart_product_qty()%></h4></td>

                                            <%
                                                if (pj.getDeleteStatus().equals(del)) {
                                            %>
                                            <td><h4 style="color: black">Done</h4></td>

                                            <%
                                            } else {

                                            %>
                                            <td><h4 style="color: red"><a onclick="removecart('<%=add.getCart_product_id()%>')"><img src="images/remove.png" /></a></h4></td>
                                                        <%
                                                            }
                                                        %>
                                        </tr>

                                    </tbody>

                                    <%}

                                    %>


                                    <%}%>

                                </table>
                                <script>
                                    function removecart(cv) {
                                        $.post(
                                                "remove_from_cart",
                                                {id: cv},
                                        function (result) {
                                            // var m = result.toString();
                                            window.location.replace("checkout_page.jsp");

                                        }
                                        );

                                    }
                                </script>

                                <br>

                                <table style="margin-left: 10px">
                                    <tr>
                                        <td><h4 style="color: black">Recipient's Name</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=r_name%></h4></td>

                                    </tr>


                                    <tr>
                                        <td><h4 style="color: black">Recipient's Contact Number</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=r_contact%></h4></td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black">Recipient's Address</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=r_ad1%>&nbsp;<%=r_ad2%>&nbsp;<%=city%></h4></td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black">Delivery Date & Time</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=date%>&nbsp;&nbsp;<%=time%></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4 style="color: black">Instruction</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=istruction%></h4></td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black"> Message</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=message%></h4></td>
                                    </tr>

                                    <tr>
                                        <td> <h4 style="color: black">Sender's Name & Email</h4></td>
                                        <td> <h4 style="color: black">:</h4></td>
                                        <td> <h4 style="color: black"><%=s_name%>&nbsp;(<%=s_email%>)</h4></td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black">Sender's Contact Number</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black"><%=s_contact%></h4></td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black">Price Of Gift Items</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black">Rs.&nbsp;<%=pas.format(subtotal)%></h4></td>
                                    </tr>


                                    <%

                                        //Session news = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                        Criteria css = adsession.createCriteria(POJO.DeliveryCity.class);
                                        css.add(Restrictions.eq("city", city));
                                        POJO.DeliveryCity dc = (POJO.DeliveryCity) css.uniqueResult();

                                        tot = Double.parseDouble(dc.getCharge()) + subtotal;

                                        double del_char = Double.parseDouble(dc.getCharge());

                                    %>
                                    <tr>
                                        <td><h4 style="color: black">Delivery Chargers</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black">Rs.&nbsp;<%=pas.format(del_char)%></h4></td>
                                    </tr>

                                    <tr>
                                        <td><h4 style="color: black">Total</h4></td>
                                        <td><h4 style="color: black">:</h4></td>
                                        <td><h4 style="color: black">Rs.&nbsp;<%=pas.format(tot)%></h4></td>
                                    </tr>

                                </table>
                                <br>
                                <p id="ms" class="hidden" style="color:red; margin-left:80px;font-size: 20px">Please Remove Above Items</p>

                                <br><br>

                                <%

                                    String mad = null;
                                    List<POJO.Products> lm = null;

                                    Session ads = Connections.NewHibernateUtil.getSessionFactory().openSession();
                                    Keep_cart kp = (Keep_cart) request.getSession().getAttribute("shopping_cart");

                                    POJO.DeleteStatus pojopr = (POJO.DeleteStatus) ads.load(POJO.DeleteStatus.class, Integer.parseInt("1"));

                                    if (kp != null) {
                                        List<Added_cart_items> ad = kp.returnArray();
                                        forlp:
                                        for (Added_cart_items adm : ad) {

                                            Criteria mycrit1 = ads.createCriteria(POJO.Products.class);
                                            mycrit1.add(Restrictions.ne("deleteStatus", pojopr));
                                            mycrit1.add(Restrictions.eq("id", adm.getCart_product_id()));

                                            lm = mycrit1.list();

                                            if (lm.isEmpty()) {
                                                // out.print("ac");
                                                mad = "ac";

                                            } else {
                                                //  out.print("in");
                                                mad = "in";
                                                break forlp;
                                            }

                                        }

//                                    
                                    }
                                %>

                                <%
                                    if (mad == "ac") {
                                %>

                                <div  class="contact-submit">
                                    <a   onclick=" toDB()" class="New-lassana-btn">Payment<img style="vertical-align: middle" src="images/index.png"></a>
                                    <br>
                                    <br>
                                    <a  href="my_delivery.jsp" id="conshopping-btn"  onclick="" class="New-lassana-btn"><img style="vertical-align: middle" src="images/index2.png">Back To Delivery</a>
                                </div>

                                <%
                                } else {
                                %>
                                <script>
                                    $('#ms').show();
                                </script>
                                <%
                                    }
                                %>
                                <script type="text/javascript">

                                    function toDB() {
                                        var total = '<%=tot%>';
                                        $.post(
                                                "save_invoice",
                                                {tot: total},
                                        function (result) {
                                            var id = result.toString();
                                            window.open("Invoice_page.jsp", "Invoice", "status=1,width=860,height=700");
                                            window.location.replace("index.jsp");
                                        }
                                        );
                                    }
                                </script>
                            </fieldset>

                        </form>


                    </div>

                    <%
                        adsession.close();
                    %>
                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>
        </div>                
    </body>

</html>

<%//
        }else{
            request.getSession().invalidate();
            response.sendRedirect("index.jsp");
        }
    } else {
        response.sendRedirect("User_login.jsp");
    }
%>
