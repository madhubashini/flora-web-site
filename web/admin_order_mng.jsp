<%-- 
    Document   : admin_order_mng
    Created on : Apr 8, 2016, 6:31:05 PM
    Author     : HP
--%>
<%
    String num = null;
    if (request.getSession().getAttribute("login") != null) {
        POJO.UserReg logu = (POJO.UserReg) request.getSession().getAttribute("login");
        num = logu.getUserType().getId() + "";
        System.out.print("if eke");

        if (num.equals("2")) {


%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link rel="shortcut icon" href="img/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <link href="styles_main.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <!-- topbar start -->
        <%@include file="admin_header.jsp" %>
        <!-- topbar ends -->
        <div class="container-fluid">

            <div class="row-fluid">
                <!-- left menu starts -->
                <%@include file="admin_leftBar.jsp" %>
                <!-- left menu ends -->
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <p style="font-size: 30px;margin-left: 50%;margin-top: 5%">Invoice Management</p>
                    </div>
                    <br>
                    <div class="row-fluid sortable">
                        <div style="width: 1000px">


                            <%                                Session sesion = Connections.NewHibernateUtil.getSessionFactory().openSession();

                                String reqID = request.getParameter("ID");

                            %>

                            <div class="box-content">
                                <div style="margin-left: 50%">
                                    <div class="dataTables_filter" id="DataTables_Table_0_filter"><label>
                                            Search By  Invoice ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="number" id="t" aria-controls="DataTables_Table_0">&nbsp;&nbsp;&nbsp;
                                            <button type="button"  class="btn btn-primary" onclick="ser1()">Search</button><br>
                                        </label>
                                    </div>
                                </div>
                                <div style="float:left">
                                    <div class="dataTables_filter" id="DataTables_Table_0_filter" style="float: right"><label>
                                            <button type="button"  class="btn btn-primary" onclick="invshp()">Shipped Invoices&nbsp;</button><br><br>
                                            <button type="button"  class="btn btn-primary" onclick="invcan()">Canceled Invoices</button><br><br>
                                            <button type="button"  class="btn btn-primary" onclick="news()">&nbsp;&nbsp;&nbsp;&nbsp;Sales Report&nbsp;&nbsp;&nbsp;&nbsp;</button><br><br>
                                        </label>
                                    </div>
                                </div>
                                
                                <p id="men" class="hide" style="color: red">You Entered Invalid ID</p>
                                <br>

                                <script>

                                    function ser1() {

                                        var id = document.getElementById("t").value;
                                        //alert(id);
                                        window.location.replace("admin_order_mng.jsp?ID=" + id);

                                    }
                                    
                                    function news(){
                                        window.open("sales_report.jsp","Invoice", "status=1,width=860,height=700");
                                    }
                                </script>
                                <script>
                                    function invshp() {
                                        var i = 1;
                                        window.location.replace("admin_shipped_invo.jsp");
                                    }
                                </script>
                                <script>
                                    function invcan() {
                                        window.location.replace("admin_canceled_invo.jsp");
                                    }
                                </script>

                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <hr>
                                <br>
                                <table class="table  table-bordered bootstrap-datatable datatable">
                                    <thead>
                                        <tr>
                                            <th>Invoice ID</th>
                                            <th>Date</th>
                                            <th>User ID</th>
                                            <th>Sub Total Rs.</th>
                                            <th>Delivery Date</th>
                                            <th>Shipping Option</th>
                                            <th></th>
                                        </tr>
                                    </thead> 

                                    <tbody>
                                        <%                                int index = 0;
                                            int records = 0;
                                            int recordsPerPage = 10;
                                            int proCount = 0;

                                            String realPageIndex = request.getParameter("pageIndx");

                                            if (realPageIndex == null) {
                                                index = 1;
                                            } else {
                                                index = Integer.parseInt(realPageIndex);
                                            }
                                            int result = (index * recordsPerPage) - recordsPerPage;

                                            Criteria cr1 = sesion.createCriteria(POJO.Invoice.class);

                                            if (reqID != null) {
                                                cr1.add(Restrictions.eq("id", Integer.parseInt(reqID)));
                                            }
                                            POJO.DeliveryStatus de = (POJO.DeliveryStatus) sesion.load(POJO.DeliveryStatus.class, Integer.parseInt("1"));
                                            cr1.add(Restrictions.eq("deliveryStatus", de));
                                            cr1.addOrder(Order.desc("id"));

                                            cr1.setFirstResult(result);
                                            cr1.setMaxResults(recordsPerPage);

                                            List<POJO.Invoice> in = cr1.list();

                                        %> 

                                        <%    if (!in.isEmpty()) {
                                                for (POJO.Invoice invoice : in) {
                                        %>
                                        <tr>
                                            <td><%=invoice.getId()%></td>
                                            <td class="center"><%=invoice.getDate()%></td>
                                            <td class="center"><%=invoice.getUserReg().getId()%></td>
                                            <td class="center"><%=invoice.getSubtot()%></td>
                                            <td class="center"><%=invoice.getDeliveryInfo().getDate()%></td>


                                            <td class="center">

                                                <a class="btn btn-info" onclick="inb1('<%=invoice.getId()%>')">
                                                    <i></i>  
                                                    Ship OR Cancel                                           
                                                </a>


                                            </td>

                                            <td>
                                                <a  onclick="inpri('<%=invoice.getId()%>')" style="text-decoration: underline;color: white">
                                                    <i></i>  
                                                    Print                                           
                                                </a>
                                            </td>

                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>
                                <script>
                                    function inb1(res) {
                                        window.location.replace("admin_show_invoice.jsp?ID=" + res);
                                    }

                                    function inpri(id) {
                                        window.open("show_invoice.jsp?idd=" + id, "Invoice", "status=1,width=860,height=700");
                                    }
                                </script>

                                <%
                                    Criteria cr2 = sesion.createCriteria(POJO.Invoice.class);
                                    if (reqID != null) {
                                        cr2.add(Restrictions.eq("id", Integer.parseInt(reqID)));
                                    }
                                    POJO.DeliveryStatus d = (POJO.DeliveryStatus) sesion.load(POJO.DeliveryStatus.class, Integer.parseInt("1"));
                                    cr2.add(Restrictions.eq("deliveryStatus", d));
                                    cr2.addOrder(Order.desc("id"));

                                    List<POJO.Invoice> l = cr2.list();

                                    for (POJO.Invoice pr : l) {
                                        proCount++;
                                        //out.print(prm.getId());
                                    }
                                    int pages = proCount / 10;
                                    if (proCount > (pages * 10)) {
                                        pages = pages + 1;
                                    }

                                %>

                                <div class="pagination pagination-centered">
                                    <ul>
                                        <%                                            String url = null;
                                            for (int ii = 1; ii <= pages; ii++) {
                                                if (reqID != null) {
                                                    url = "http://localhost:8080/Madhu_Flora_New/admin_order_mng.jsp?pageIndx=" + ii + "&ID=" + reqID;
                                                } else {
                                                    url = "http://localhost:8080/Madhu_Flora_New/admin_order_mng.jsp?pageIndx=" + ii;
                                                }
                                        %>



                                        <li class="active">
                                            <a href='<%=url%>'><%=ii%></a>
                                        </li>



                                        <%
                                            }
                                        %>
                                    </ul>
                                </div>

                                <%                                } else {
                                %>

                                <!--                                <script>
                                                                    $('#men').show();
                                                                </script>-->


                                <%
                                    }
                                %>

                            </div>


                        </div>

                    </div>
                </div>

            </div>
            <footer>
                <p class="pull-left">&copy; <a href="" target="_blank">Madhu Flora</a> 2012</p>
            </footer>
        </div>

    </body>
</html>
<%
        }

    } else {
        response.sendRedirect("index.jsp");
    }

%>