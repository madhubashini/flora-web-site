<%-- 
    Document   : admin_show_userActy
    Created on : Jul 7, 2016, 12:38:29 PM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link id="bs-css" href="admin_css/bootstrap-classic.min.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="admin_css/bootstrap-responsive.css" rel="stylesheet">
        <link href="admin_css/charisma-app.css" rel="stylesheet">
        <link href="admin_css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='admin_css/chosen.css' rel='stylesheet'>
        <link href='admin_css/uniform.default.css' rel='stylesheet'>
        <link href='admin_css/colorbox.css' rel='stylesheet'>
        <link href='admin_css/jquery.cleditor.css' rel='stylesheet'>
        <link href='admin_css/jquery.noty.css' rel='stylesheet'>
        <link href='admin_css/noty_theme_default.css' rel='stylesheet'>
        <link href='admin_css/elfinder.min.css' rel='stylesheet'>
        <link href='admin_css/elfinder.theme.css' rel='stylesheet'>
        <link href='admin_css/opa-icons.css' rel='stylesheet'>
        <link rel="shortcut icon" href="img/favicon.ico">
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>

    </head>
    <body>

        <%
            POJO.UserReg ur = null;
            String vform = request.getParameter("ide");
            Session ses = Connections.NewHibernateUtil.getSessionFactory().openSession();
            if (vform != null) {
                ur = (POJO.UserReg) ses.load(POJO.UserReg.class, Integer.parseInt(vform));
                System.out.print(ur.getId());


        %>
        <form class="form-horizontal" method="post" action="" style="margin-right: 50px;color: black" >
            <fieldset>
                <br>
                <div class="control-group">
                    <label class="control-label">Id</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getId()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"> First Name</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getFstName()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"> Last Name</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getLastName()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">E-Mail</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getEMail()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Added Date</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><%=ur.getAddedDate()%></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">User Type</label>
                    <div class="controls">
                        <select id="utyp" data-rel="chosen" name="catg">
                            <option value="1" selected="selected"><%=ur.getUserType().getType()%></option>
                            <%    Criteria crm = ses.createCriteria(POJO.UserType.class);
                                List<POJO.UserType> ty = crm.list();
                                for (POJO.UserType p : ty) {
                            %>
                            <option value="<%=p.getId()%>"><%=p.getType()%></option>
                            <%
                                }
                            %>
                        </select>

                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Status</label>
                    <div class="controls">
                        <select id="stats" data-rel="chosen" name="catg">
                            <option value="1" selected="selected"><%=ur.getUserStatus().getStatus()%></option>
                            <%    Criteria cr = ses.createCriteria(POJO.UserStatus.class);
                                List<POJO.UserStatus> t = cr.list();
                                for (POJO.UserStatus s : t) {
                            %>
                            <option value="<%=s.getId()%>"><%=s.getStatus()%></option>
                            <%
                                }
                            %>
                        </select>

                    </div>
                </div>
                <div style="height: 24px; padding: 10px 15px;float: right">
                    <button onclick="jsgo(<%=ur.getId()%>)" class="btn btn-primary">Save Changes</button>
                </div>
            </fieldset>
        </form>
        <%
            }
        %>
        <script>
            function  jsgo(para) {

                var typ = $('#utyp').find('option:selected').val();
                var stat = $('#stats').find('option:selected').val();

                $.post(
                        "edit_user",
                        {type: typ, status: stat, uid: para},
                function (res) {
                    alert("aaa");
                    var a = res.toString();
                    if (a == "1") {
                        window.location.replace("admin_user_add.jsp");
                    } else {
                        alert("something wrong");
                    }
                }
                );
            }

        </script>
    </body>
</html>
<%
    ses.flush();
%>