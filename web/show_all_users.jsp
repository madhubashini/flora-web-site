<%-- 
    Document   : show_all_users
    Created on : Oct 13, 2016, 3:35:37 PM
    Author     : HP
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body id="a"> 
        <style>
            td,th{
                border: solid black 1px;    
            }
        </style>

        <script>
            $(document).ready(function () {
                PrintElem('#a');


            });
            function PrintElem(elem)
            {
                // Popup($('<div/>').append($(elem).clone()).html());
                Popup($(elem).html());
            }

            function Popup(data)
            {

                var headstr = "<html><head><title>Booking Details</title></head><body>";
                var footstr = "</body></html>";
                var newstr = document.getElementById('a').innerHTML;
                var oldstr = document.body.innerHTML;
                document.body.innerHTML = headstr + newstr + footstr;
                window.print();
                document.body.innerHTML = oldstr;
                return false;
            }
        </script>

        <h2 style="margin-left: 25%">All Users</h2>
        <table style="width: 90%;">
            <th>User ID</th>
            <th>First Name</th>
            <th>E-mail</th>
            <th>Added Date</th>
            <th>User Type</th>
            <th>User Status</th>

            <%

                Session s = Connections.NewHibernateUtil.getSessionFactory().openSession();
                Criteria c = s.createCriteria(POJO.UserReg.class);
                List<POJO.UserReg> u = c.list();
                for (POJO.UserReg ur : u) {
            %>

            <tr>
                <td>
                    <%=ur.getId()%>
                </td>
                <td>
                    <%= ur.getFstName()%>
                </td>
                <td>
                    <%=ur.getEMail()%>
                </td>
                <td>
                    <%=ur.getAddedDate()%>
                </td>
                <td>
                    <%=ur.getUserType().getType()%>
                </td>
                <td>
                    <%=ur.getUserStatus().getStatus()%>
                </td>
            </tr>

            <%      }


            %>


        </table>
    </body>
</html>
