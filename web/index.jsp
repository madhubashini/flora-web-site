<%-- 
    Document   : index
    Created on : Feb 2, 2016, 7:18:08 PM
    Author     : HP
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Index</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link href="styles_main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/slider_theme.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/sub_menue.css" />
        <link rel="stylesheet" type="text/css" href="css/pagination.css" />

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>
        <script type="text/javascript" src="js/my_javascripts.js"></script>
        <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
        <script type="text/JavaScript" src="js/slimbox2.js"></script>                
        <script  type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
        <link href="css/typicons.min.css" rel="stylesheet">
    </head>

    <body>



        <div id="templatemo_wrapper_h">
            <div id="templatemo_header_wh">
                <div id="templatemo_header" class="header_home">
                    <div id="site_title"><a href="#"></a>             
                        <div id="hotline" >
                            <!--<a><img src="images/flower_b.png"></a>-->
                            <p style="color: red"><strong>Hotline (+940112184518)</strong></p>
                        </div>  
                    </div>
                    <%@include file="my_header.jsp" %>
                    <%@include file="my_slider.jsp" %>

                </div> <!-- END of header -->
            </div> <!-- END of header wrapper -->
            <div id="templatemo_main_wrapper">
                <div id="templatemo_main">

                    <%@include file="my_sidebar.jsp" %>

                    <div id="content" class="right">
                        <h2>Welcome to Madhu Flora</h2>

                        <div class="madhu_flora">
                            <p>Madhu Flora is the most confidence way to deliver your gift items to your benefactor.We provide free delivery servise to colombo ,kandy & kurunegala 
                                nuygergni rgierbgjenig hguiehrgijneog ijhiguheinr iehiuhgie ibuygihbe buhebrhbg ebubuebuvhuvhbveb</p> 
                        </div>
                        <%                            DecimalFormat d = new DecimalFormat("0.00");

                        %>

                        <%                            String stats = null;

                            Session k = Connections.NewHibernateUtil.getSessionFactory().openSession();
                            POJO.SubCatogory suc = (POJO.SubCatogory) k.load(POJO.SubCatogory.class, Integer.parseInt("2"));
                            POJO.DeleteStatus pojp = (POJO.DeleteStatus) k.load(POJO.DeleteStatus.class, Integer.parseInt("2"));
                            POJO.DeleteStatus poj = (POJO.DeleteStatus) k.load(POJO.DeleteStatus.class, Integer.parseInt("3"));

                            Criteria cris = k.createCriteria(POJO.Products.class);
                            cris.add(Restrictions.ne("deleteStatus", pojp));
                            cris.add(Restrictions.eq("subCatogory", suc));
                            cris.addOrder(Order.desc("id"));
                            cris.setMaxResults(3);
                            List<POJO.Products> list = cris.list();

                            one:
                            for (POJO.Products prod : list) {
                                double prv = Double.parseDouble(prod.getUnitPrice());
                                if (prod.getDeleteStatus().equals(poj)) {
                                    stats = "Out Of Stock";
                                } else {
                                    stats = "In Stock";
                                }


                        %>

                        <div class="product_box">

                            <img src=<%=prod.getImg()%> alt="floralset1" width="165" height="165" />
                            <h3><%=prod.getName()%></h3>
                            <p class="product_price">Rs:<%=d.format(prv)%></p>
                            <p  id="plm" class="product_price"><%=stats%></p>
                            <%
                                if (!(prod.getDeleteStatus().equals(poj))) {
                            %>
                            <p  id="hde" class="add_to_cart">
                                <a onclick="sendrequest('<%=prod.getId()%>')">Add to Cart</a>
                                <script type="text/javascript">

                                    function sendrequest(id) {
                                        var i = id;
                                        var url = "my_product_details.jsp?id=" + i;
                                        window.location.replace(url);

                                    }
                                </script>

                            </p>
                            <%
                                }
                            %>
                        </div>

                        <%
                            }


                        %>


                        <%                            String stats2 = null;
                            Criteria cris2 = k.createCriteria(POJO.Products.class);
                            POJO.SubCatogory suc2 = (POJO.SubCatogory) k.load(POJO.SubCatogory.class, Integer.parseInt("15"));
                            cris2.add(Restrictions.ne("deleteStatus", pojp));
                            cris2.add(Restrictions.eq("subCatogory", suc2));
                            cris2.addOrder(Order.desc("id"));
                            cris2.setMaxResults(3);
                            List<POJO.Products> list2 = cris2.list();

                            one:
                            for (POJO.Products prodd : list2) {
                                double pn = Double.parseDouble(prodd.getUnitPrice());
                                if (prodd.getDeleteStatus().equals(poj)) {
                                    stats2 = "Out Of Stock";
                                } else {
                                    stats2 = "In Stock";
                                }

                        %>

                        <div class="product_box">

                            <img src=<%=prodd.getImg()%> alt="floralset1" width="165" height="165" />
                            <h3><%=prodd.getName()%></h3>
                            <p class="product_price">Rs:<%=d.format(pn)%></p>
                            <p  id="plm" class="product_price"><%=stats2%></p>
                            <%
                                if (!(prodd.getDeleteStatus().equals(poj))) {
                            %>
                            <p  id="hde" class="add_to_cart">
                                <a onclick="sendrequestt('<%=prodd.getId()%>')">Add to Cart</a>
                                <script type="text/javascript">
                                    function sendrequestt(id) {
                                        var i = id;
                                        var url = "my_product_details.jsp?id=" + i;
                                        window.location.replace(url);
                                    }
                                </script>

                            </p>
                            <%
                                }
                            %>
                        </div>

                        <%
                            }


                        %>

                        <%                            String stats3 = null;
                            Criteria cris3 = k.createCriteria(POJO.Products.class);
                            POJO.SubCatogory suc3 = (POJO.SubCatogory) k.load(POJO.SubCatogory.class, Integer.parseInt("10"));
                            cris3.add(Restrictions.ne("deleteStatus", pojp));
                            cris3.add(Restrictions.eq("subCatogory", suc3));
                            cris3.addOrder(Order.desc("id"));
                            cris3.setMaxResults(2);
                            List<POJO.Products> list3 = cris3.list();

                            one:
                            for (POJO.Products prods : list3) {
                                double pr = Double.parseDouble(prods.getUnitPrice());
                                if (prods.getDeleteStatus().equals(poj)) {
                                    stats3 = "Out Of Stock";
                                } else {
                                    stats3 = "In Stock";
                                }

                        %>

                        <div class="product_box">

                            <img src=<%=prods.getImg()%> alt="floralset1" width="165" height="165" />
                            <h3><%=prods.getName()%></h3>
                            <p class="product_price">Rs:<%=d.format(pr)%></p>
                            <p  id="plm" class="product_price"><%=stats3%></p>
                            <%
                                if (!(prods.getDeleteStatus().equals(poj))) {
                            %>
                            <p  id="hde" class="add_to_cart">
                                <a onclick="sendrequestn('<%=prods.getId()%>')">Add to Cart</a>
                                <script type="text/javascript">

                                    function sendrequestn(id) {
                                        var i = id;
                                        var url = "my_product_details.jsp?id=" + i;
                                        window.location.replace(url);
                                    }
                                </script>

                            </p>
                            <%
                                }
                            %>
                        </div>

                        <%
                            }


                        %>


                    </div>
                    <%                        k.close();
                    %>
                    <div class="cleaner"></div>
                </div> <!-- END of main -->
            </div> <!-- END of main wrapper -->

            <%@include file="my_footer.jsp" %>


        </div>

    </body>
</html>
